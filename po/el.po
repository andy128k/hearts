# Greek, Modern (1453-) translation for hearts
# Copyright (c) (c) 2006 Canonical Ltd, and Rosetta Contributors 2006
# This file is distributed under the same license as the hearts package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2006.
#
msgid ""
msgstr ""
"Project-Id-Version: hearts\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-06-05 07:45+0200\n"
"PO-Revision-Date: 2007-10-31 15:30+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Greek, Modern (1453-) <el@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2008-02-13 22:44+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. set the score window title
#: ../src/hearts.c:89
msgid "Scores"
msgstr "Βαθμολογία"

#: ../src/hearts.c:141 ../src/hearts.c:183
#, fuzzy
msgid "Select three cards that you wish to pass on."
msgstr "Διάλεξε τρία χαρτιά που θέλεις να μοιράσεις στους άλλους παίκτες"

#. redraw the screen
#: ../src/hearts.c:146 ../src/hearts.c:188 ../src/events.c:205
#: ../src/events.c:233
msgid "Click on the card you wish to play."
msgstr "Διαλέξε το χαρτί που θα παίξεις"

#: ../src/hearts.c:299
msgid "Game over - You have won!"
msgstr "Τέλος Παιχνιδιού - Νίκησες!"

#: ../src/hearts.c:301
msgid "Game over - You have lost"
msgstr "Τέλος Παιχνιδιού - Έχασες"

#: ../src/hearts.c:303
msgid "Click somewhere to start a new game."
msgstr "Κάντε κλικ για να ξεκινήσετε ένα νέο παιχνίδι."

#. redraw the screen
#: ../src/hearts.c:307 ../src/hearts.c:482 ../src/events.c:181
#: ../src/events.c:270
msgid "Click somewhere to continue the game."
msgstr "Κάντε κλικ για να συνεχίσετε το παιχνίδι."

#. Hint: %s is the name of the card the player should play
#: ../src/hearts.c:486
#, c-format
msgid "Play %s."
msgstr "Παίξε %s."

#: ../src/hearts.c:491 ../src/events.c:192
#, c-format
msgid "Click on the hand of %s to pass the cards."
msgstr ""

#. Hint: %s is the name of the card the player should pass on
#: ../src/hearts.c:503
#, c-format
msgid "Pass %s."
msgstr "Μοίρασε %s"

#: ../src/hearts.c:507
msgid "Sorry, I can't help you."
msgstr "Συγγνώμη, δε μπορώ να σε βοηθήσω"

#: ../src/cards.c:39
msgid "the ace of clubs"
msgstr "άσσος σπαθί"

#: ../src/cards.c:40
msgid "the two of clubs"
msgstr "δύο σπαθί"

#: ../src/cards.c:41
msgid "the three of clubs"
msgstr "τρία σπαθί"

#: ../src/cards.c:42
msgid "the four of clubs"
msgstr "τέσσερα σπαθί"

#: ../src/cards.c:43
msgid "the five of clubs"
msgstr "πέντε σπαθί"

#: ../src/cards.c:44
msgid "the six of clubs"
msgstr "έξι σπαθί"

#: ../src/cards.c:45
msgid "the seven of clubs"
msgstr "επτά σπαθί"

#: ../src/cards.c:46
msgid "the eight of clubs"
msgstr "οκτώ σπαθί"

#: ../src/cards.c:47
msgid "the nine of clubs"
msgstr "εννέα σπαθί"

#: ../src/cards.c:48
msgid "the ten of clubs"
msgstr "δέκα σπαθί"

#: ../src/cards.c:49
msgid "the jack of clubs"
msgstr "βαλές σπαθί"

#: ../src/cards.c:50
msgid "the queen of clubs"
msgstr "ντάμα σπαθί"

#: ../src/cards.c:51
msgid "the king of clubs"
msgstr "παπάς σπαθί"

#: ../src/cards.c:58
msgid "the ace of diamonds"
msgstr "άσσος καρώ"

#: ../src/cards.c:59
msgid "the two of diamonds"
msgstr "δύο καρώ"

#: ../src/cards.c:60
msgid "the three of diamonds"
msgstr "τρία καρώ"

#: ../src/cards.c:61
msgid "the four of diamonds"
msgstr "τέσσερα καρώ"

#: ../src/cards.c:62
msgid "the five of diamonds"
msgstr "πέντε καρώ"

#: ../src/cards.c:63
msgid "the six of diamonds"
msgstr "έξι καρώ"

#: ../src/cards.c:64
msgid "the seven of diamonds"
msgstr "επτά καρώ"

#: ../src/cards.c:65
msgid "the eight of diamonds"
msgstr "οκτώ καρώ"

#: ../src/cards.c:66
msgid "the nine of diamonds"
msgstr "εννέα καρώ"

#: ../src/cards.c:67
msgid "the ten of diamonds"
msgstr "δέκα καρώ"

#: ../src/cards.c:68
msgid "the jack of diamonds"
msgstr "βαλές καρώ"

#: ../src/cards.c:69
msgid "the queen of diamonds"
msgstr "ντάμα καρώ"

#: ../src/cards.c:70
msgid "the king of diamonds"
msgstr "παπάς καρώ"

#. Motörhead! :-)
#: ../src/cards.c:77
msgid "the ace of spades"
msgstr "άσσος μπαστούνι"

#: ../src/cards.c:78
msgid "the two of spades"
msgstr "δύο μπαστούνι"

#: ../src/cards.c:79
msgid "the three of spades"
msgstr "τρία μπαστούνι"

#: ../src/cards.c:80
msgid "the four of spades"
msgstr "τέσσερα μπαστούνι"

#: ../src/cards.c:81
msgid "the five of spades"
msgstr "πέντε μπαστούνι"

#: ../src/cards.c:82
msgid "the six of spades"
msgstr "έξι μπαστούνι"

#: ../src/cards.c:83
msgid "the seven of spades"
msgstr "επτά μπαστούνι"

#: ../src/cards.c:84
msgid "the eight of spades"
msgstr "οκτώ μπαστούνι"

#: ../src/cards.c:85
msgid "the nine of spades"
msgstr "εννέα μπαστούνι"

#: ../src/cards.c:86
msgid "the ten of spades"
msgstr "δέκα μπαστούνι"

#: ../src/cards.c:87
msgid "the jack of spades"
msgstr "βαλές μπαστούνι"

#: ../src/cards.c:88
msgid "the queen of spades"
msgstr "ντάμα μπαστούνι"

#: ../src/cards.c:89
msgid "the king of spades"
msgstr "παπάς μπαστούνι"

#: ../src/cards.c:96
msgid "the ace of hearts"
msgstr "άσσος κούπα"

#: ../src/cards.c:97
msgid "the two of hearts"
msgstr "δύο κούπα"

#: ../src/cards.c:98
msgid "the three of hearts"
msgstr "τρία κούπα"

#: ../src/cards.c:99
msgid "the four of hearts"
msgstr "τέσσερα κούπα"

#: ../src/cards.c:100
msgid "the five of hearts"
msgstr "πέντε κούπα"

#: ../src/cards.c:101
msgid "the six of hearts"
msgstr "έξι κούπα"

#: ../src/cards.c:102
msgid "the seven of hearts"
msgstr "επτά κούπα"

#: ../src/cards.c:103
msgid "the eight of hearts"
msgstr "οκτώ κούπα"

#: ../src/cards.c:104
msgid "the nine of hearts"
msgstr "εννέα κούπα"

#: ../src/cards.c:105
msgid "the ten of hearts"
msgstr "δέκα κούπα"

#: ../src/cards.c:106
msgid "the jack of hearts"
msgstr "βαλές κούπα"

#: ../src/cards.c:107
msgid "the queen of hearts"
msgstr "ντάμα κούπα"

#: ../src/cards.c:108
msgid "the king of hearts"
msgstr "παπάς κούπα"

#: ../src/ui.c:60
msgid "Images"
msgstr "Εικόνες"

#: ../src/gnome-hearts.glade.h:1
msgid "A hearts game for GNOME by Sander Marechal"
msgstr "Κούπες για GNOME από τον Sander Marechal"

#: ../src/gnome-hearts.glade.h:2
msgid "Car_d style:"
msgstr "Στυλ χαρτιών"

#: ../src/gnome-hearts.glade.h:3
msgid "Choose a _background image:"
msgstr "Διάλεξε φόντο"

#: ../src/gnome-hearts.glade.h:4
#, fuzzy
msgid "Choose a ruleset for the game and set some gentleman's rules"
msgstr ""
"Διάλεξε τους κανόνες του παιχνιδιού και δημιούργησε τους δικούς σου κανόνες "
"κυριών"

#: ../src/gnome-hearts.glade.h:5
msgid "Choose the style of the cards you wish to play with"
msgstr "Διάλεξε το στυλ των χαρτιών με τα οποία επιθυμείς να παίξεις"

#: ../src/gnome-hearts.glade.h:6
msgid "Copyright 2006-2007, Stichting Lone Wolves"
msgstr "Πνευματικά δικαιώματα 2006-2007 Lone Wolves"

#: ../src/gnome-hearts.glade.h:7
msgid "Game"
msgstr "Παιχνίδι"

#. The name of the game, not the type of card
#: ../src/gnome-hearts.glade.h:9
msgid "Hearts"
msgstr "Κούπες"

#: ../src/gnome-hearts.glade.h:10
msgid "Hearts must be \"_broken\" before leading the trick"
msgstr ""

#: ../src/gnome-hearts.glade.h:11
msgid "Hint"
msgstr "Κόλπο"

#: ../src/gnome-hearts.glade.h:12
msgid ""
"Omnibus\n"
"Omnibus Alternative\n"
"Spot Hearts\n"
"Standard"
msgstr ""

#: ../src/gnome-hearts.glade.h:16
msgid "Players"
msgstr "Παίκτες"

#: ../src/gnome-hearts.glade.h:17
msgid "Preferences"
msgstr "Προτιμήσεις..."

#: ../src/gnome-hearts.glade.h:18
msgid "Restart"
msgstr "Επανεκκίνηση"

#: ../src/gnome-hearts.glade.h:19
msgid "Select a background image"
msgstr "Διάλεξε φόντο"

#: ../src/gnome-hearts.glade.h:20
msgid ""
"Select which players you wish to play against, or choose <random> to have "
"the computer pick one for you."
msgstr ""
"Διάλεξε τους παίκτες που επιθυμείς να παίξουν εναντίον σου ή διάλεξε "
"<τυχαία> για να διαλέξει ο υπολογιστής για σένα"

#: ../src/gnome-hearts.glade.h:21
msgid "Style"
msgstr "Στυλ"

#: ../src/gnome-hearts.glade.h:22
msgid "_Control"
msgstr "_Έλεγχος"

#: ../src/gnome-hearts.glade.h:23
msgid "_East:"
msgstr "_Ανατολικά"

#: ../src/gnome-hearts.glade.h:24
msgid "_Fullscreen"
msgstr "_Πλήρης οθόνη"

#: ../src/gnome-hearts.glade.h:25
msgid "_Game"
msgstr "_Παιχνίδι"

#: ../src/gnome-hearts.glade.h:26
msgid "_Help"
msgstr "_Βοήθεια"

#: ../src/gnome-hearts.glade.h:27
msgid "_Hint"
msgstr "_Κόλπο"

#: ../src/gnome-hearts.glade.h:28
msgid "_Leave fullscreen"
msgstr "_Έξοδος από την πλήρη οθόνη"

#: ../src/gnome-hearts.glade.h:29
msgid "_North:"
msgstr "_Βοράς"

#: ../src/gnome-hearts.glade.h:30
msgid "_Point cards may not be played on the first trick"
msgstr "_Μάρκαρε τα χαρτιά που δε μπορούν να παίξουν στην πρώτη γύρα"

#: ../src/gnome-hearts.glade.h:31
msgid "_Queen of Spades breaks hearts"
msgstr ""

#: ../src/gnome-hearts.glade.h:32
msgid "_Redo move"
msgstr "_Ακύρωση αναίρεσης κίνησης"

#: ../src/gnome-hearts.glade.h:33
msgid "_Restart"
msgstr "_Επανεκκίνηση"

#: ../src/gnome-hearts.glade.h:34
msgid "_Ruleset:"
msgstr "_Κανόνες"

#: ../src/gnome-hearts.glade.h:35
msgid "_Scores"
msgstr "_Βαθμολογίες"

#: ../src/gnome-hearts.glade.h:36
msgid "_Show the player scores next to their name"
msgstr ""

#: ../src/gnome-hearts.glade.h:37
msgid "_Statusbar"
msgstr "_Γραμμή κατάστασης"

#: ../src/gnome-hearts.glade.h:38
msgid "_Stretch image"
msgstr ""

#: ../src/gnome-hearts.glade.h:39
msgid "_Tile image"
msgstr ""

#: ../src/gnome-hearts.glade.h:40
msgid "_Toolbar"
msgstr "_Μπάρα εργαλείων"

#: ../src/gnome-hearts.glade.h:41
msgid "_Two of clubs leads the round"
msgstr "_Το δύο σπαθί ξεκινά το γύρο"

#: ../src/gnome-hearts.glade.h:42
msgid "_Undo move"
msgstr "_Αναίρεση κίνησης"

#: ../src/gnome-hearts.glade.h:43
msgid "_View"
msgstr "_Προβολή"

#: ../src/gnome-hearts.glade.h:44
msgid "_West:"
msgstr "_Δύση"

#. The game, not the type of card.
#: ../src/gnome-hearts.glade.h:46
#, fuzzy
msgid "gnome-hearts 0.3"
msgstr "gnome_Κούπες 0.3"

#. TRANSLATORS: Replace this string with your names, one name per line.
#: ../src/gnome-hearts.glade.h:48
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  Kostas https://launchpad.net/~kostkon\n"
"  Panos Mavrogiorgos https://launchpad.net/~pmav99\n"
"  Stelios Papadopoulos https://launchpad.net/~stilianos-papadopoulos"
