use gio::{Icon, Menu, MenuItem, MenuModel};
use glib::prelude::*;

fn item(label: &str, action: &str, accel: Option<&str>, icon: Option<&str>) -> MenuItem {
    let item = MenuItem::new(Some(label), Some(action));

    if let Some(accel) = accel {
        item.set_attribute_value("accel", Some(&accel.into()));
    }

    if let Some(icon_name) = icon {
        match Icon::new_for_string(icon_name) {
            Ok(icon) => item.set_icon(&icon),
            Err(e) => eprintln!("Cannot find icon {}. Reason: {}", icon_name, e),
        }
    }

    item
}

trait MenuBuilderExt {
    fn item(self, label: &str, action: &str) -> Self;
    fn item_with_accel(self, label: &str, action: &str, accel: &str) -> Self;
    fn submenu(self, label: &str, submenu: MenuModel) -> Self;
}

impl MenuBuilderExt for Menu {
    fn item(self, label: &str, action: &str) -> Self {
        self.append_item(&item(label, action, None, None));
        self
    }

    fn item_with_accel(self, label: &str, action: &str, accel: &str) -> Self {
        self.append_item(&item(label, action, Some(accel), None));
        self
    }

    fn submenu(self, label: &str, submenu: MenuModel) -> Self {
        self.append_submenu(Some(label), &submenu);
        self
    }
}

pub fn create_menu_bar() -> MenuModel {
    let menu = Menu::new();
    menu.append_submenu(Some("_Game"), &{
        let menu = Menu::new();
        menu.append_section(None, &{
            Menu::new()
                .item_with_accel("_New", "app.new", "<Primary>n")
                .item("_Restart", "app.restart")
                .item("_Scores", "app.scores")
                .item("_Preferences", "app.preferences")
        });
        menu.append_section(None, &{
            Menu::new().item_with_accel("_Quit", "app.quit", "<Primary>q")
        });
        menu
    });
    menu.append_submenu(Some("_View"), &{
        Menu::new()
            .item_with_accel("_Fullscreen", "app.fullscreen", "F11")
            .item("_Toolbar", "app.toolbar")
            .item("_Statusbar", "app.statusbar")
    });
    menu.append_submenu(Some("_Help"), &{
        let menu = Menu::new();
        menu.append_section(None, &{
            Menu::new()
                .item("_Hint", "app.hint")
                .item("_Help", "app.help")
        });
        menu.append_section(None, &{ Menu::new().item("_About", "app.about") });
        menu
    });
    menu.upcast()
}
