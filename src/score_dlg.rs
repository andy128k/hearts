use crate::score::*;
use gtk::prelude::*;
use gtk::{
    Dialog, DialogFlags, Grid, Justification, Label, Orientation, PositionType, ResponseType,
    Separator, Widget, Window,
};

pub fn show_scores(parent: &Window, title: &str, names: &[&str; 4], score_history: &[ScoreValues]) {
    let dlg = Dialog::with_buttons(
        Some(title),
        Some(parent),
        DialogFlags::MODAL,
        &[("Close", ResponseType::Reject)],
    );
    dlg.set_transient_for(Some(parent));
    dlg.set_resizable(false);
    dlg.set_icon_name(Some("gnome-hearts"));
    dlg.set_property_default_height(200);

    let grid = Grid::new();
    grid.set_border_width(10);
    grid.set_column_spacing(12);
    grid.set_row_spacing(6);
    grid.set_column_homogeneous(true);

    fn name_label(name: &str) -> Label {
        let label = Label::new(Some(name));
        label.set_justify(Justification::Center);
        label
    }

    {
        let north = name_label(&names[0]);
        grid.attach(&north, 0, 0, 1, 1);

        let east = name_label(&names[1]);
        grid.attach(&east, 1, 0, 1, 1);

        let south = name_label(&names[2]);
        grid.attach(&south, 2, 0, 1, 1);

        let west = name_label(&names[3]);
        grid.attach(&west, 3, 0, 1, 1);
    }

    grid.attach(&Separator::new(Orientation::Horizontal), 0, 1, 4, 1);

    fn score_label(score: i32, stroked: bool) -> Label {
        let text = if stroked {
            format!("<s>{}</s>", score)
        } else {
            format!("{}", score)
        };
        let label = Label::new(None);
        label.set_markup(&text);
        label.set_justify(Justification::Center);
        label
    }

    {
        let count = score_history.len();
        for (index, scores) in score_history.iter().enumerate() {
            let but_last = index + 1 < count;

            let north = score_label(scores.north, but_last);
            grid.attach_next_to(&north, None::<&Widget>, PositionType::Bottom, 1, 1);

            let east = score_label(scores.east, but_last);
            grid.attach_next_to(&east, Some(&north), PositionType::Right, 1, 1);

            let south = score_label(scores.south, but_last);
            grid.attach_next_to(&south, Some(&east), PositionType::Right, 1, 1);

            let west = score_label(scores.west, but_last);
            grid.attach_next_to(&west, Some(&south), PositionType::Right, 1, 1);
        }
    }

    dlg.get_content_area().add(&grid);

    dlg.show_all();
    dlg.run();
    dlg.close();
}
