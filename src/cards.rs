use crate::i18n::tr;
use num::FromPrimitive;
use num_derive::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::ops::Sub;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord, FromPrimitive)]
pub enum Rank {
    TWO = 2,
    THREE,
    FOUR,
    FIVE,
    SIX,
    SEVEN,
    EIGHT,
    NINE,
    TEN,
    JACK,
    QUEEN,
    KING,
    ACE,
}

impl Rank {
    pub const ALL: [Rank; 13] = [
        Rank::TWO,
        Rank::THREE,
        Rank::FOUR,
        Rank::FIVE,
        Rank::SIX,
        Rank::SEVEN,
        Rank::EIGHT,
        Rank::NINE,
        Rank::TEN,
        Rank::JACK,
        Rank::QUEEN,
        Rank::KING,
        Rank::ACE,
    ];

    pub fn succ(&self) -> Self {
        Self::from_i32(*self as i32 + 1).unwrap_or(Rank::ACE)
    }
    pub fn pred(&self) -> Self {
        Self::from_i32(*self as i32 - 1).unwrap_or(Rank::TWO)
    }
}

impl Sub for Rank {
    type Output = i32;
    fn sub(self, rhs: Self) -> Self::Output {
        (self as i32) - (rhs as i32)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Suit {
    CLUBS = 0,
    DIAMONDS,
    HEARTS,
    SPADES,
}

impl Suit {
    pub const ALL: [Suit; 4] = [Suit::CLUBS, Suit::DIAMONDS, Suit::HEARTS, Suit::SPADES];
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum Ruleset {
    Omnibus,
    OmnibusAlternative,
    SpotHearts,
    Standard,
}

impl Ruleset {
    pub const ALL: &'static [Ruleset] = &[
        Ruleset::Omnibus,
        Ruleset::OmnibusAlternative,
        Ruleset::SpotHearts,
        Ruleset::Standard,
    ];

    pub fn from_id(id: impl AsRef<str>) -> Option<Self> {
        match id.as_ref() {
            "omnibus" => Some(Ruleset::Omnibus),
            "omnibus-alternative" => Some(Ruleset::OmnibusAlternative),
            "spot-hearts" => Some(Ruleset::SpotHearts),
            "standard" => Some(Ruleset::Standard),
            _ => None,
        }
    }

    pub fn to_id(&self) -> &'static str {
        match self {
            Ruleset::Omnibus => "omnibus",
            Ruleset::OmnibusAlternative => "omnibus-alternative",
            Ruleset::SpotHearts => "spot-hearts",
            Ruleset::Standard => "standard",
        }
    }

    pub fn display(&self) -> &'static str {
        match self {
            Ruleset::Omnibus => "Omnibus",
            Ruleset::OmnibusAlternative => "Omnibus Alternative",
            Ruleset::SpotHearts => "Spot Hearts",
            Ruleset::Standard => "Standard",
        }
    }

    pub fn score_limit(&self) -> i32 {
        match self {
            Ruleset::SpotHearts => 500,
            _ => 100,
        }
    }

    /// Returns the point value of a card
    pub fn card_cost(&self, card: &Card) -> i32 {
        if *self == Ruleset::SpotHearts {
            if card == &Card::QUEEN_OF_SPADES {
                25
            } else if card.suit == Suit::HEARTS {
                card.rank as i32
            } else {
                0
            }
        } else {
            if card == &Card::QUEEN_OF_SPADES {
                13
            } else if card.suit == Suit::HEARTS {
                1
            } else {
                0
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Card {
    pub suit: Suit,
    pub rank: Rank,
}

impl Card {
    pub const DECK: [Card; 52] = [
        Card {
            suit: Suit::CLUBS,
            rank: Rank::TWO,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::THREE,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::FOUR,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::FIVE,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::SIX,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::SEVEN,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::EIGHT,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::NINE,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::TEN,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::JACK,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::QUEEN,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::KING,
        },
        Card {
            suit: Suit::CLUBS,
            rank: Rank::ACE,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::TWO,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::THREE,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::FOUR,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::FIVE,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::SIX,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::SEVEN,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::EIGHT,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::NINE,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::TEN,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::JACK,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::QUEEN,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::KING,
        },
        Card {
            suit: Suit::DIAMONDS,
            rank: Rank::ACE,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::TWO,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::THREE,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::FOUR,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::FIVE,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::SIX,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::SEVEN,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::EIGHT,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::NINE,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::TEN,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::JACK,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::QUEEN,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::KING,
        },
        Card {
            suit: Suit::SPADES,
            rank: Rank::ACE,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::TWO,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::THREE,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::FOUR,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::FIVE,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::SIX,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::SEVEN,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::EIGHT,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::NINE,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::TEN,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::JACK,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::QUEEN,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::KING,
        },
        Card {
            suit: Suit::HEARTS,
            rank: Rank::ACE,
        },
    ];

    pub const QUEEN_OF_SPADES: Card = Card {
        suit: Suit::SPADES,
        rank: Rank::QUEEN,
    };
    pub const KING_OF_SPADES: Card = Card {
        suit: Suit::SPADES,
        rank: Rank::KING,
    };
    pub const ACE_OF_SPADES: Card = Card {
        suit: Suit::SPADES,
        rank: Rank::ACE,
    };
    pub const TWO_OF_CLUBS: Card = Card {
        suit: Suit::CLUBS,
        rank: Rank::TWO,
    };

    pub fn new(suit: Suit, rank: Rank) -> Self {
        Self { suit, rank }
    }

    pub fn id(&self) -> i32 {
        (13 * (self.suit as i32)) + ((self.rank as i32 - 1) % 13)
    }

    /** Return the name of a card */
    pub fn get_name(&self) -> String {
        match (self.suit, self.rank) {
            (Suit::CLUBS, Rank::TWO) => tr("the two of clubs"),
            (Suit::CLUBS, Rank::THREE) => tr("the three of clubs"),
            (Suit::CLUBS, Rank::FOUR) => tr("the four of clubs"),
            (Suit::CLUBS, Rank::FIVE) => tr("the five of clubs"),
            (Suit::CLUBS, Rank::SIX) => tr("the six of clubs"),
            (Suit::CLUBS, Rank::SEVEN) => tr("the seven of clubs"),
            (Suit::CLUBS, Rank::EIGHT) => tr("the eight of clubs"),
            (Suit::CLUBS, Rank::NINE) => tr("the nine of clubs"),
            (Suit::CLUBS, Rank::TEN) => tr("the ten of clubs"),
            (Suit::CLUBS, Rank::JACK) => tr("the jack of clubs"),
            (Suit::CLUBS, Rank::QUEEN) => tr("the queen of clubs"),
            (Suit::CLUBS, Rank::KING) => tr("the king of clubs"),
            (Suit::CLUBS, Rank::ACE) => tr("the ace of clubs"),
            (Suit::DIAMONDS, Rank::TWO) => tr("the two of diamonds"),
            (Suit::DIAMONDS, Rank::THREE) => tr("the three of diamonds"),
            (Suit::DIAMONDS, Rank::FOUR) => tr("the four of diamonds"),
            (Suit::DIAMONDS, Rank::FIVE) => tr("the five of diamonds"),
            (Suit::DIAMONDS, Rank::SIX) => tr("the six of diamonds"),
            (Suit::DIAMONDS, Rank::SEVEN) => tr("the seven of diamonds"),
            (Suit::DIAMONDS, Rank::EIGHT) => tr("the eight of diamonds"),
            (Suit::DIAMONDS, Rank::NINE) => tr("the nine of diamonds"),
            (Suit::DIAMONDS, Rank::TEN) => tr("the ten of diamonds"),
            (Suit::DIAMONDS, Rank::JACK) => tr("the jack of diamonds"),
            (Suit::DIAMONDS, Rank::QUEEN) => tr("the queen of diamonds"),
            (Suit::DIAMONDS, Rank::KING) => tr("the king of diamonds"),
            (Suit::DIAMONDS, Rank::ACE) => tr("the ace of diamonds"),
            (Suit::SPADES, Rank::TWO) => tr("the two of spades"),
            (Suit::SPADES, Rank::THREE) => tr("the three of spades"),
            (Suit::SPADES, Rank::FOUR) => tr("the four of spades"),
            (Suit::SPADES, Rank::FIVE) => tr("the five of spades"),
            (Suit::SPADES, Rank::SIX) => tr("the six of spades"),
            (Suit::SPADES, Rank::SEVEN) => tr("the seven of spades"),
            (Suit::SPADES, Rank::EIGHT) => tr("the eight of spades"),
            (Suit::SPADES, Rank::NINE) => tr("the nine of spades"),
            (Suit::SPADES, Rank::TEN) => tr("the ten of spades"),
            (Suit::SPADES, Rank::JACK) => tr("the jack of spades"),
            (Suit::SPADES, Rank::QUEEN) => tr("the queen of spades"),
            (Suit::SPADES, Rank::KING) => tr("the king of spades"),
            (Suit::SPADES, Rank::ACE) => tr("the ace of spades"),
            (Suit::HEARTS, Rank::TWO) => tr("the two of hearts"),
            (Suit::HEARTS, Rank::THREE) => tr("the three of hearts"),
            (Suit::HEARTS, Rank::FOUR) => tr("the four of hearts"),
            (Suit::HEARTS, Rank::FIVE) => tr("the five of hearts"),
            (Suit::HEARTS, Rank::SIX) => tr("the six of hearts"),
            (Suit::HEARTS, Rank::SEVEN) => tr("the seven of hearts"),
            (Suit::HEARTS, Rank::EIGHT) => tr("the eight of hearts"),
            (Suit::HEARTS, Rank::NINE) => tr("the nine of hearts"),
            (Suit::HEARTS, Rank::TEN) => tr("the ten of hearts"),
            (Suit::HEARTS, Rank::JACK) => tr("the jack of hearts"),
            (Suit::HEARTS, Rank::QUEEN) => tr("the queen of hearts"),
            (Suit::HEARTS, Rank::KING) => tr("the king of hearts"),
            (Suit::HEARTS, Rank::ACE) => tr("the ace of hearts"),
        }
    }
}

fn best_by_key<F, O>(cards: &[Card], key: F) -> usize
where
    F: Fn(&Card) -> O,
    O: Ord,
{
    let (index, _) = cards
        .iter()
        .enumerate()
        .max_by_key(|(_index, card)| key(card))
        .unwrap();
    index
}

pub fn select_card(cards: &mut Vec<Card>, card: &Card) -> Option<Card> {
    let index = cards.iter().position(|c| c == card)?;
    Some(cards.remove(index))
}

pub fn select_card_best_by_key<F, O>(cards: &mut Vec<Card>, key: F) -> Card
where
    F: Fn(&Card) -> O,
    O: Ord,
{
    let index = best_by_key(cards, key);
    cards.remove(index)
}

pub fn have_suit(cards: &[Card], suit: Suit) -> bool {
    cards.iter().any(|c| c.suit == suit)
}

pub fn is_point_card(card: Card) -> bool {
    card.suit == Suit::HEARTS || (card.suit == Suit::SPADES && card.rank == Rank::QUEEN)
}

/* Get the point value of a list of cards */
pub fn cards_get_points(cards: &[Card], ruleset: Ruleset) -> i32 {
    let modifier = if cards.len() == 52 {
        -2 // Shooting the sun: substract double points for hearts and the Queen of spades
    } else if cards.iter().filter(|c| is_point_card(**c)).count() == 14 {
        -1 // Shooting the moon: substract points for hearts and the Queen of spades
    } else {
        1
    };

    cards
        .iter()
        .map(|card| match (card, ruleset) {
            (
                Card {
                    suit: Suit::HEARTS, ..
                },
                Ruleset::SpotHearts,
            ) => card.rank as i32 * modifier,
            (
                Card {
                    suit: Suit::HEARTS, ..
                },
                _,
            ) => modifier,
            (
                Card {
                    suit: Suit::SPADES,
                    rank: Rank::QUEEN,
                },
                Ruleset::SpotHearts,
            ) => 25 * modifier,
            (
                Card {
                    suit: Suit::SPADES,
                    rank: Rank::QUEEN,
                },
                _,
            ) => 13 * modifier,
            (
                Card {
                    suit: Suit::DIAMONDS,
                    rank: Rank::JACK,
                },
                Ruleset::Omnibus,
            ) => -10,
            (
                Card {
                    suit: Suit::DIAMONDS,
                    rank: Rank::TEN,
                },
                Ruleset::OmnibusAlternative,
            ) => -10,
            _ => 0,
        })
        .sum()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_points_calc() {
        assert_eq!(cards_get_points(&Card::DECK, Ruleset::Standard), -52);
        assert_eq!(cards_get_points(&Card::DECK, Ruleset::Omnibus), -62);
        assert_eq!(
            cards_get_points(&Card::DECK, Ruleset::OmnibusAlternative),
            -62
        );
        assert_eq!(cards_get_points(&Card::DECK, Ruleset::SpotHearts), -258);
        assert_eq!(cards_get_points(&[Card::TWO_OF_CLUBS], Ruleset::Omnibus), 0);
        assert_eq!(
            cards_get_points(
                &[Card::QUEEN_OF_SPADES, Card::TWO_OF_CLUBS],
                Ruleset::Omnibus
            ),
            13
        );
    }
}
