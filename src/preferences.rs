use crate::ai::Opponent;
use crate::cards::Ruleset;
use crate::config::*;
use crate::i18n::*;
use glib::clone;
use glib::prelude::*;
use gtk::prelude::*;
use gtk::{
    CheckButton, ComboBoxText, Dialog, FileChooserAction, FileChooserButton, FileFilter, Grid,
    Label, Notebook, RadioButton, Widget, Window,
};
use std::fs::read_dir;
use std::io;
use std::path::PathBuf;
use std::rc::Rc;

trait Form<T> {
    fn set_data(&self, data: &T);
    fn get_data(&self) -> Option<T>;
    fn on_change(&mut self, cb: impl Fn(T) + 'static);
    fn widget(&self) -> Widget;
}

struct GameOptionsForm {
    grid: Grid,
    ruleset: ComboBoxText,
    clubs_lead: CheckButton,
    hearts_break: CheckButton,
    no_blood: CheckButton,
    queen_breaks_hearts: CheckButton,
    changed: Option<Box<dyn Fn(GameOptions) + 'static>>,
}

impl GameOptionsForm {
    fn new() -> Rc<Self> {
        let grid = Grid::new();
        grid.set_border_width(12);
        grid.set_column_spacing(12);
        grid.set_row_spacing(12);

        let label = gtk::LabelBuilder::new()
            .label(&tr(
                "Choose a ruleset for the game and set some gentleman's rules",
            ))
            .build();

        grid.attach(&label, 0, 0, 2, 1);

        let ruleset = ComboBoxText::new();
        for r in Ruleset::ALL {
            ruleset.append(Some(r.to_id()), r.display());
        }
        ruleset.set_hexpand(true);
        grid.attach(&Label::new(Some(&tr("Ruleset"))), 0, 1, 1, 1);
        grid.attach(&ruleset, 1, 1, 1, 1);

        let clubs_lead = CheckButton::with_label(tr("Two of clubs leads the round").as_str());
        grid.attach(&clubs_lead, 0, 2, 2, 1);

        let no_blood = CheckButton::with_label(
            tr("Point cards may not be played on the first trick").as_str(),
        );
        grid.attach(&no_blood, 0, 3, 2, 1);

        let hearts_break = CheckButton::with_label(
            tr("Hearts must be \"broken\" before leading the trick").as_str(),
        );
        grid.attach(&hearts_break, 0, 4, 2, 1);

        let queen_breaks_hearts =
            CheckButton::with_label(tr("Queen of Spades breaks hearts").as_str());
        queen_breaks_hearts.set_margin_start(18);
        grid.attach(&queen_breaks_hearts, 0, 5, 2, 1);

        let form = Rc::new(Self {
            grid,
            ruleset,
            clubs_lead,
            hearts_break,
            no_blood,
            queen_breaks_hearts,
            changed: None,
        });

        form.ruleset
            .connect_changed(clone!(@weak form => move |_| form.changed()));
        form.clubs_lead
            .connect_toggled(clone!(@weak form => move |_| form.changed()));
        form.hearts_break
            .connect_toggled(clone!(@weak form => move |_| form.changed()));
        form.no_blood
            .connect_toggled(clone!(@weak form => move |_| form.changed()));
        form.queen_breaks_hearts
            .connect_toggled(clone!(@weak form => move |_| form.changed()));

        form
    }

    fn changed(&self) {
        if let Some(data) = self.get_data() {
            self.set_data(&data);
            if let Some(ref changed) = self.changed {
                (*changed)(data);
            }
        }
    }
}

impl Form<GameOptions> for GameOptionsForm {
    fn set_data(&self, data: &GameOptions) {
        self.ruleset.set_active_id(Some(data.ruleset.to_id()));

        self.clubs_lead.set_active(data.clubs_lead);
        self.no_blood.set_active(data.no_blood);

        self.hearts_break.set_active(data.hearts_break);
        if data.hearts_break {
            self.queen_breaks_hearts.set_sensitive(true);
            self.queen_breaks_hearts
                .set_active(data.queen_breaks_hearts);
        } else {
            self.queen_breaks_hearts.set_sensitive(false);
            self.queen_breaks_hearts.set_active(false);
        }
    }

    fn get_data(&self) -> Option<GameOptions> {
        Some(GameOptions {
            ruleset: Ruleset::from_id(self.ruleset.get_active_id()?)?,
            clubs_lead: self.clubs_lead.get_active(),
            no_blood: self.no_blood.get_active(),
            hearts_break: self.hearts_break.get_active(),
            queen_breaks_hearts: self.hearts_break.get_active()
                && self.queen_breaks_hearts.get_active(),
        })
    }

    fn on_change(&mut self, cb: impl Fn(GameOptions) + 'static) {
        self.changed = Some(Box::new(cb));
    }

    fn widget(&self) -> Widget {
        self.grid.clone().upcast()
    }
}

struct AIOptionsForm {
    grid: Grid,
    north_ai: ComboBoxText,
    east_ai: ComboBoxText,
    west_ai: ComboBoxText,
    changed: Option<Box<dyn Fn(AIOptions) + 'static>>,
}

impl AIOptionsForm {
    pub fn new() -> Rc<Self> {
        let grid = Grid::new();
        grid.set_border_width(12);
        grid.set_column_spacing(12);
        grid.set_row_spacing(12);

        grid.attach(
            &Label::new(Some(&tr("Select which players you wish to play against"))),
            0,
            0,
            2,
            1,
        );

        let north_ai = ComboBoxText::new();
        for s in &Opponent::ALL {
            north_ai.append(Some(s.to_str()), s.to_str());
        }
        north_ai.set_hexpand(true);
        grid.attach(&Label::new(Some(&tr("North"))), 0, 1, 1, 1);
        grid.attach(&north_ai, 1, 1, 1, 1);

        let east_ai = ComboBoxText::new();
        for s in &Opponent::ALL {
            east_ai.append(Some(s.to_str()), &s.to_str());
        }
        east_ai.set_hexpand(true);
        grid.attach(&Label::new(Some(&tr("East"))), 0, 2, 1, 1);
        grid.attach(&east_ai, 1, 2, 1, 1);

        let west_ai = ComboBoxText::new();
        for s in &Opponent::ALL {
            west_ai.append(Some(s.to_str()), &s.to_str());
        }
        west_ai.set_hexpand(true);
        grid.attach(&Label::new(Some(&tr("West"))), 0, 3, 1, 1);
        grid.attach(&west_ai, 1, 3, 1, 1);

        let form = Rc::new(Self {
            grid,
            north_ai,
            east_ai,
            west_ai,
            changed: None,
        });

        form.north_ai
            .connect_changed(clone!(@weak form => move |_| form.changed()));
        form.east_ai
            .connect_changed(clone!(@weak form => move |_| form.changed()));
        form.west_ai
            .connect_changed(clone!(@weak form => move |_| form.changed()));

        form
    }

    fn changed(&self) {
        if let Some(ref changed) = self.changed {
            if let Some(data) = self.get_data() {
                (*changed)(data);
            }
        }
    }
}

impl Form<AIOptions> for AIOptionsForm {
    fn set_data(&self, data: &AIOptions) {
        self.north_ai.set_active_id(Some(data.north_ai.to_str()));
        self.east_ai.set_active_id(Some(data.east_ai.to_str()));
        self.west_ai.set_active_id(Some(data.west_ai.to_str()));
    }

    fn get_data(&self) -> Option<AIOptions> {
        Some(AIOptions {
            north_ai: Opponent::from_str(&self.north_ai.get_active_id()?)?,
            east_ai: Opponent::from_str(&self.east_ai.get_active_id()?)?,
            west_ai: Opponent::from_str(&self.west_ai.get_active_id()?)?,
        })
    }

    fn on_change(&mut self, cb: impl Fn(AIOptions) + 'static) {
        self.changed = Some(Box::new(cb));
    }

    fn widget(&self) -> Widget {
        self.grid.clone().upcast()
    }
}

#[derive(Debug, Clone)]
pub struct CardStyle {
    pub name: String,
    pub path: PathBuf,
}

struct StyleOptionsForm {
    grid: Grid,
    background_image: FileChooserButton,
    background_stretched: RadioButton,
    card_style: ComboBoxText,
    show_scores: CheckButton,
    changed: Option<Box<dyn Fn(StyleOptions) + 'static>>,
    card_styles: Vec<CardStyle>,
}

impl StyleOptionsForm {
    pub fn new(card_styles: &[CardStyle]) -> Rc<Self> {
        let grid = Grid::new();
        grid.set_border_width(12);
        grid.set_column_spacing(12);
        grid.set_row_spacing(12);

        let background_image =
            FileChooserButton::new(&tr("Select a background image"), FileChooserAction::Open);
        let filter = FileFilter::new();
        filter.set_name(Some(&tr("Images")));
        filter.add_mime_type("image/png");
        filter.add_mime_type("image/svg+xml");
        filter.add_mime_type("image/jpeg");
        filter.add_mime_type("image/gif");
        background_image.set_filter(&filter);
        background_image.set_hexpand(true);
        grid.attach(&Label::new(Some(&tr("Background"))), 0, 0, 1, 1);
        grid.attach(&background_image, 1, 0, 1, 1);

        let background_stretched = {
            let radio_grid = Grid::new();
            radio_grid.set_column_spacing(12);

            let tile = RadioButton::with_label(tr("Tile image").as_str());
            radio_grid.attach(&tile, 0, 0, 1, 1);

            let stretch = RadioButton::with_label_from_widget(&tile, tr("Stretch image").as_str());
            radio_grid.attach(&stretch, 1, 0, 1, 1);

            grid.attach(&radio_grid, 1, 1, 1, 1);
            stretch
        };

        let card_style = ComboBoxText::new();
        card_style.append(Some("-1"), &tr("Default"));
        for (index, s) in card_styles.iter().enumerate() {
            card_style.append(Some(format!("{}", index).as_str()), &s.name);
        }
        card_style.set_hexpand(true);
        grid.attach(&Label::new(Some(&tr("Card style"))), 0, 2, 1, 1);
        grid.attach(&card_style, 1, 2, 1, 1);

        let show_scores =
            CheckButton::with_label(tr("Show the player scores next to their name").as_str());
        grid.attach(&show_scores, 0, 3, 2, 1);

        let form = Rc::new(Self {
            grid,
            background_image,
            background_stretched,
            card_style,
            show_scores,
            changed: None,
            card_styles: card_styles.to_vec(),
        });

        form.background_image
            .connect_selection_changed(clone!(@weak form => move |_| form.changed()));
        form.background_stretched
            .connect_toggled(clone!(@weak form => move |_| form.changed()));
        form.card_style
            .connect_changed(clone!(@weak form => move |_| form.changed()));
        form.show_scores
            .connect_toggled(clone!(@weak form => move |_| form.changed()));

        form
    }

    fn changed(&self) {
        if let Some(ref changed) = self.changed {
            if let Some(data) = self.get_data() {
                (*changed)(data);
            }
        }
    }
}

impl Form<StyleOptions> for StyleOptionsForm {
    fn set_data(&self, data: &StyleOptions) {
        if let Some(ref path) = data.background_image {
            self.background_image.set_filename(path);
        } else {
            self.background_image.unselect_all();
        }
        self.background_stretched
            .set_active(data.background_stretched);

        let style_index = self
            .card_styles
            .iter()
            .position(|s| Some(&s.path) == data.card_style.as_ref())
            .map(|p| p as i32)
            .unwrap_or(-1);
        self.card_style
            .set_active_id(Some(&format!("{}", style_index)));

        self.show_scores.set_active(data.show_scores);
    }

    fn get_data(&self) -> Option<StyleOptions> {
        Some(StyleOptions {
            background_image: self.background_image.get_filename(),
            background_stretched: self.background_stretched.get_active(),
            card_style: self
                .card_style
                .get_active_id()
                .and_then(|id| id.parse::<usize>().ok())
                .and_then(|index| self.card_styles.get(index))
                .map(|style| style.path.clone()),
            show_scores: self.show_scores.get_active(),
        })
    }

    fn on_change(&mut self, cb: impl Fn(StyleOptions) + 'static) {
        self.changed = Some(Box::new(cb));
    }

    fn widget(&self) -> Widget {
        self.grid.clone().upcast()
    }
}

fn load_card_styles() -> Result<Vec<CardStyle>, io::Error> {
    let mut card_styles = Vec::new();

    /* read cards from the gnome-games card dir */
    for entry in read_dir("/usr/share/gnome-games-common/cards/")? {
        let entry = entry?;
        if entry.file_type()?.is_file() {
            let path = entry.path();
            let extension = path.extension();
            if extension == Some("png".as_ref())
                || extension == Some("svg".as_ref())
                || extension == Some("svgz".as_ref())
            {
                let card_style = CardStyle {
                    name: path.file_stem().unwrap().to_str().unwrap().to_owned(),
                    path: path.clone(),
                };
                card_styles.push(card_style);
            }
        }
    }

    Ok(card_styles)
}

pub fn preferences(parent: &Window, config: &Config) -> Option<Config> {
    let card_styles = match load_card_styles() {
        Ok(styles) => styles,
        Err(err) => {
            eprintln!("Cannot load card styles: {}", err);
            Vec::new()
        }
    };

    let dlg = Dialog::new();
    dlg.set_title(&tr("Preferences"));
    dlg.set_transient_for(Some(parent));
    dlg.set_resizable(false);

    let notebook = Notebook::new();
    notebook.set_show_border(false);

    let game_options = GameOptionsForm::new();
    game_options.set_data(&config.game_options);
    let ai_options = AIOptionsForm::new();
    ai_options.set_data(&config.ai_options);
    let style_options = StyleOptionsForm::new(&card_styles);
    style_options.set_data(&config.style_options);

    notebook.append_page(&game_options.widget(), Some(&Label::new(Some(&tr("Game")))));
    notebook.append_page(
        &ai_options.widget(),
        Some(&Label::new(Some(&tr("Players")))),
    );
    notebook.append_page(
        &style_options.widget(),
        Some(&Label::new(Some(&tr("Style")))),
    );

    dlg.get_content_area().set_border_width(0);
    dlg.get_content_area().add(&notebook);

    dlg.show_all();
    dlg.run();

    let result = Config {
        game_options: game_options.get_data()?,
        ai_options: ai_options.get_data()?,
        style_options: style_options.get_data()?,
    };

    dlg.close();

    Some(result)
}
