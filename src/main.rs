mod ai;
mod card_list;
mod card_style;
mod cards;
mod config;
mod events;
mod game;
mod i18n;
mod menu;
mod pass;
mod player;
mod playing_area;
mod preferences;
mod score;
mod score_dlg;
mod side;
mod state;
mod toolbar;
mod trick;

use gio::prelude::*;
use gio::{resources_register, ApplicationFlags, Resource, SimpleAction};
use glib::{clone, variant::Variant, Bytes};
use gtk::prelude::*;
use gtk::{Application, ApplicationWindow, Grid, Statusbar, WindowPosition};
use std::cell::RefCell;
use std::rc::Rc;

use crate::config::Config;
use crate::events::*;
use crate::game::{Game, GameState};
use crate::i18n::*;
use crate::playing_area::PlayingArea;
use crate::state::State;

fn create_app() -> gtk::Application {
    let app = Application::new(Some("net.andy128k.hearts"), ApplicationFlags::HANDLES_OPEN)
        .expect("Initialization of application failed.");

    let config = Rc::new(RefCell::new(Config::default()));

    app.connect_startup(clone!(@strong config => move |_app| {
        load_pixmaps().unwrap();
        *config.borrow_mut() = Config::load();
    }));
    app.connect_activate(clone!(@strong config => move |app| {
        activate(app, &config.borrow());
    }));
    app.connect_shutdown(clone!(@strong config => move |_app| {
        if let Err(error) = config.borrow().save() {
            eprintln!("{}", error);
        }
    }));
    app
}

fn main() {
    // #ifdef ENABLE_NLS
    //     bindtextdomain(GETTEXT_PACKAGE, PACKAGE_LOCALE_DIR);
    //     textdomain(PACKAGE);
    //     setlocale(LC_ALL, "");
    // #endif

    let argv: Vec<String> = ::std::env::args().collect();
    let app = create_app();
    let code = app.run(&argv);
    std::process::exit(code);
}

fn load_pixmaps() -> Result<(), glib::Error> {
    const RESOURCE_BUNDLE: &[u8] = include_bytes!(concat!(env!("OUT_DIR"), "/pixmaps.gresource"));
    // https://github.com/gtk-rs/glib/issues/120
    let data = Bytes::from(&RESOURCE_BUNDLE[..]);
    let resource = Resource::from_data(&data)?;
    resources_register(&resource);
    Ok(())
}

fn activate(app: &Application, config: &Config) {
    let game = Rc::new(RefCell::new(Game::new(config)));

    app.set_menubar(Some(&menu::create_menu_bar()));

    let window = ApplicationWindow::new(app);
    window.set_size_request(750, 550);
    window.set_position(WindowPosition::Center);
    window.set_title("Hearts");
    window.set_icon_name(Some("gnome-hearts"));

    let grid = Grid::new();

    let tb = toolbar::create_tool_bar();
    tb.set_hexpand(true);
    grid.attach(&tb, 0, 0, 1, 1);

    let playingarea = PlayingArea::new(&config.style_options, &game);
    grid.attach(&playingarea.get_widget(), 0, 1, 1, 1);

    let statusbar = Statusbar::new();
    statusbar.set_hexpand(true);
    grid.attach(&statusbar, 0, 2, 1, 1);

    window.add(&grid);

    let state = Rc::new(RefCell::new(State {
        app: app.clone(),
        window: window.clone(),
        toolbar: tb,
        playingarea,
        statusbar: statusbar.clone(),
        config: config.clone(),
        game: game.clone(),
    }));

    {
        let state2 = state.clone();
        let action = SimpleAction::new("new", None);
        action.connect_activate(move |_, _| on_new_event(&state2.borrow()));
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new("restart", None);
        action.connect_activate(move |_, _| on_restart_event(&state2.borrow()));
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new("scores", None);
        action.connect_activate(move |_, _| on_scores_event(&state2.borrow()));
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new("preferences", None);
        action.connect_activate(move |_, _| on_preferences_event(&mut state2.borrow_mut()));
        app.add_action(&action);
    }
    {
        let app2 = app.clone();
        let action = SimpleAction::new("quit", None);
        action.connect_activate(move |_, _| app2.quit());
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new_stateful("fullscreen", None, &Variant::from(false));
        action.connect_change_state(move |action, value| {
            on_fullscreen_event(action, value, &state2.borrow())
        });
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new_stateful("toolbar", None, &Variant::from(true));
        action.connect_change_state(move |action, value| {
            on_toolbar_toggled(action, value, &state2.borrow())
        });
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new_stateful("statusbar", None, &Variant::from(true));
        action.connect_change_state(move |action, value| {
            on_statusbar_toggled(action, value, &state2.borrow())
        });
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new("help", None);
        action.connect_activate(move |_, _| on_help(&state2.borrow()));
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new("hint", None);
        action.connect_activate(move |_, _| on_hint_event(&state2.borrow()));
        app.add_action(&action);
    }
    {
        let state2 = state.clone();
        let action = SimpleAction::new("about", None);
        action.connect_activate(move |_, _| on_about_event(&state2.borrow()));
        app.add_action(&action);
    }

    {
        let state2 = state.clone();
        game.borrow_mut().connect_round_end(move || {
            let state3 = state2.clone();
            glib::idle_add_local(move || {
                on_scores_event(&state3.borrow());
                Continue(false)
            });
        });
    }
    {
        let state2 = state.clone();
        game.borrow_mut().connect_state_changed(move |_| {
            let state3 = state2.clone();
            glib::idle_add_local(move || {
                let state = state3.borrow();
                let status = get_status_text(&state.game.borrow());
                set_status(&state.statusbar, &status);
                Continue(false)
            });
        });
    }

    set_status(&statusbar, &get_status_text(&game.borrow()));

    window.show_all();
}

fn get_status_text(game: &Game) -> String {
    match game.state {
        GameState::SelectCards => {
            let pass_player = &game.players[(game.user + game.pass) as usize];
            format!(
                "Select three cards that you wish to pass to {}.",
                pass_player.name
            )
        }
        GameState::PassCards => {
            let pass_player = &game.players[(game.user + game.pass) as usize];
            format!(
                "Click on the hand of {} to pass the cards.",
                pass_player.name
            )
        }
        GameState::RecieveCards => tr("Click somewhere to continue the game."),
        GameState::Play => tr("Click on the card you wish to play."),
        GameState::RoundEnd => tr("Click somewhere to continue the game."),
        GameState::End { .. } => tr("Click somewhere to start a new game."),
    }
}

fn set_status(statusbar: &Statusbar, message: &str) {
    let context_id = statusbar.get_context_id("game status");
    statusbar.pop(context_id);
    statusbar.push(context_id, message);
}
