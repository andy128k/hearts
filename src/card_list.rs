use crate::cards::*;
use rand::{seq::SliceRandom, thread_rng};
use std::cmp::Ord;
use std::collections::HashSet;
use std::iter::Iterator;

#[derive(Clone)]
pub struct Cards(Vec<Card>);

impl Cards {
    pub fn new(card: Card) -> Cards {
        Cards(vec![card])
    }

    pub fn from_slice(cards: &[Card]) -> Option<Cards> {
        if !cards.is_empty() {
            Some(Cards(cards.to_vec()))
        } else {
            None
        }
    }

    pub fn from_set(cards: &HashSet<Card>) -> Option<Cards> {
        if !cards.is_empty() {
            let vec = cards.iter().cloned().collect();
            Some(Cards(vec))
        } else {
            None
        }
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn to_vec(&self) -> Vec<Card> {
        self.0.iter().cloned().collect()
    }

    pub fn iter(&self) -> impl Iterator<Item = &Card> {
        self.0.iter()
    }

    pub fn add(&mut self, card: Card) {
        self.0.push(card);
    }

    pub fn filter(&self, predicate: impl Fn(Card) -> bool) -> Option<Cards> {
        let filtered: Vec<Card> = self.0.iter().filter(|c| predicate(**c)).cloned().collect();
        if filtered.is_empty() {
            None
        } else {
            Some(Cards(filtered))
        }
    }

    pub fn contains(&self, card: Card) -> bool {
        self.0.contains(&card)
    }

    pub fn have_suit(&self, suit: Suit) -> bool {
        self.0.iter().any(|c| c.suit == suit)
    }

    pub fn of_suit(&self, suit: Suit) -> Option<Cards> {
        self.filter(|c| c.suit == suit)
    }

    /// returns card with lowest rank
    pub fn lowest(&self) -> Card {
        self.min_by_key(|c| c.rank)
    }

    /// returns card with highest rank
    pub fn highest(&self) -> Card {
        self.max_by_key(|c| c.rank)
    }

    pub fn only(&self) -> Option<Card> {
        if self.0.len() == 1 {
            Some(self.0[0])
        } else {
            None
        }
    }

    pub fn min_by_key<K: Fn(Card) -> O, O: Ord>(&self, key: K) -> Card {
        *self.0.iter().min_by_key(|c| key(**c)).unwrap()
    }

    pub fn max_by_key<K: Fn(Card) -> O, O: Ord>(&self, key: K) -> Card {
        *self.0.iter().max_by_key(|c| key(**c)).unwrap()
    }

    pub fn random(&self) -> Card {
        *self.0.choose(&mut thread_rng()).unwrap()
    }

    pub fn try_filter(&self, predicate: impl Fn(Card) -> bool) -> Self {
        self.filter(predicate).unwrap_or_else(|| self.clone())
    }

    pub fn try_exclude(&self, predicate: impl Fn(Card) -> bool) -> Self {
        self.try_filter(|c| !predicate(c))
    }

    pub fn try_exclude_card(&self, card: Card) -> Self {
        self.try_exclude(|c| c == card)
    }
}
