use crate::ai::Opponent;
use crate::cards::Ruleset;
use glib::get_user_config_dir;
use serde::{Deserialize, Serialize};
use std::default::Default;
use std::error::Error;
use std::fs;
use std::path::PathBuf;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct GameOptions {
    pub ruleset: Ruleset,
    pub clubs_lead: bool,
    pub hearts_break: bool,
    pub no_blood: bool,
    pub queen_breaks_hearts: bool,
}

impl Default for GameOptions {
    fn default() -> Self {
        Self {
            ruleset: Ruleset::Standard,
            clubs_lead: true,
            hearts_break: true,
            no_blood: true,
            queen_breaks_hearts: true,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AIOptions {
    pub north_ai: Opponent,
    pub east_ai: Opponent,
    pub west_ai: Opponent,
}

impl Default for AIOptions {
    fn default() -> Self {
        Self {
            north_ai: Opponent::John,
            east_ai: Opponent::Pauline,
            west_ai: Opponent::Mike,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct StyleOptions {
    pub background_image: Option<PathBuf>,
    pub background_stretched: bool,
    pub card_style: Option<PathBuf>,
    pub show_scores: bool,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Config {
    pub game_options: GameOptions,
    pub ai_options: AIOptions,
    pub style_options: StyleOptions,
}

impl Config {
    fn from_file(filename: &PathBuf) -> Result<Self, Box<dyn Error>> {
        let buf = fs::read(filename)?;
        let config = toml::from_slice(&buf)?;
        Ok(config)
    }

    pub fn load() -> Self {
        config_path()
            .and_then(|filename| Self::from_file(&filename).ok())
            .unwrap_or_default()
    }

    pub fn save(&self) -> Result<(), Box<dyn Error>> {
        let filename = config_path().ok_or("Cannot find user's config directory")?;
        let dump = toml::to_vec(self)?;
        fs::write(&filename, &dump)?;
        Ok(())
    }
}

fn config_path() -> Option<PathBuf> {
    let mut path = get_user_config_dir()?;
    path.push("hearts.toml");
    Some(path)
}
