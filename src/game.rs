use crate::ai::Opponent;
use crate::card_list::*;
use crate::cards::*;
use crate::config::*;
use crate::i18n::*;
use crate::pass::*;
use crate::player::*;
use crate::score::*;
use crate::side::*;
use crate::trick::*;
use rand::{seq::SliceRandom, thread_rng};
use std::collections::HashMap;

#[derive(Debug, PartialEq, Eq)]
pub enum GameState {
    SelectCards,
    PassCards,
    RecieveCards,
    Play,
    RoundEnd,
    End { winner: Side },
}

pub struct Game {
    pub trick: Trick,
    pub state: GameState,
    pub pass: PassDirection,
    pub trick_winner: Side,
    pub hearts_broken: bool,
    pub players: [Player; 4],
    pub user: Side,
    pub game_options: GameOptions,
    pub score_history: Vec<ScoreValues>,
    pub on_round_end: Option<Box<dyn Fn() + 'static>>,
    pub on_state_changed: Option<Box<dyn Fn(&GameState) + 'static>>,
}

impl Game {
    pub fn new(config: &Config) -> Self {
        let mut game = Self {
            state: GameState::SelectCards,
            pass: PassDirection::NoPass,
            hearts_broken: false,
            trick: Trick::new(),
            players: [
                Player::new(
                    Side::NORTH,
                    Some(
                        config
                            .ai_options
                            .north_ai
                            .create_ai(Side::NORTH, &config.game_options),
                    ),
                    &config.game_options,
                ),
                Player::new(
                    Side::EAST,
                    Some(
                        config
                            .ai_options
                            .east_ai
                            .create_ai(Side::EAST, &config.game_options),
                    ),
                    &config.game_options,
                ),
                Player::new(Side::SOUTH, None, &config.game_options),
                Player::new(
                    Side::WEST,
                    Some(
                        config
                            .ai_options
                            .west_ai
                            .create_ai(Side::WEST, &config.game_options),
                    ),
                    &config.game_options,
                ),
            ],
            user: Side::SOUTH,
            game_options: config.game_options.clone(),
            trick_winner: Side::SOUTH, // valid only on second hand
            score_history: Vec::new(),
            on_round_end: None,
            on_state_changed: None,
        };

        game.new_round();

        game
    }

    pub fn connect_round_end(&mut self, on_round_end: impl Fn() + 'static) {
        self.on_round_end = Some(Box::new(on_round_end));
    }

    pub fn connect_state_changed(&mut self, on_state_changed: impl Fn(&GameState) + 'static) {
        self.on_state_changed = Some(Box::new(on_state_changed));
    }

    fn change_state(&mut self, new_state: GameState) {
        if self.state != new_state {
            self.state = new_state;
            if let Some(ref func) = self.on_state_changed {
                (func)(&self.state);
            }
        }
    }

    pub fn new_game(&mut self) {
        self.score_history.clear();
        self.pass = PassDirection::NoPass;
        self.new_round();
    }

    /* start a new round in this game */
    pub fn new_round(&mut self) {
        /* reset the game */
        self.trick = Trick::new();
        self.pass = self.pass.next();
        self.hearts_broken = false;

        /* new hands for everyone */
        {
            let mut cards = Card::DECK.to_vec();
            cards.shuffle(&mut thread_rng());

            self.players[0].draw(&cards[0..13]);
            self.players[1].draw(&cards[13..26]);
            self.players[2].draw(&cards[26..39]);
            self.players[3].draw(&cards[39..52]);
        }

        /* have the AI's select cards to pass on if we're passing cards this round */
        if self.pass != PassDirection::NoPass {
            let scores = self.get_scores();
            for i in &Side::ALL {
                if *i != self.user {
                    self.players[*i as usize].select_cards(&scores, *i + self.pass);
                }
            }
        }

        /* set the game state */
        if self.pass != PassDirection::NoPass {
            self.change_state(GameState::SelectCards);
        } else {
            self.open_round();
        }
    }

    /* restart the current round */
    pub fn restart_round(&mut self) {
        /* reset the game */
        self.trick = Trick::new();
        self.hearts_broken = false;

        /* reset the hands for everyone */
        for i in 0..4 {
            self.players[i].reset();
        }

        /* have the AI's select cards to pass on if we're passing cards this round */
        if self.pass != PassDirection::NoPass {
            let scores = self.get_scores();
            for i in &Side::ALL {
                if *i != self.user {
                    self.players[*i as usize].select_cards(&scores, *i + self.pass);
                }
            }
        }

        /* set the game state */
        if self.pass != PassDirection::NoPass {
            self.change_state(GameState::SelectCards);
        } else {
            self.open_round();
        }
    }

    /* start the round and play until the player is due */
    pub fn open_round(&mut self) {
        /* figure out which playes has to start */
        let starter = if self.game_options.clubs_lead {
            if self.players[0].has_two_of_clubs() {
                Side::NORTH
            } else if self.players[1].has_two_of_clubs() {
                Side::EAST
            } else if self.players[2].has_two_of_clubs() {
                Side::SOUTH
            } else {
                Side::WEST
            }
        } else {
            self.user + self.pass
        };

        let scores = self.get_scores();

        /* play */
        let mut i = starter;
        while i != self.user {
            let valid_cards = self.get_valid_cards(i);
            let player = &mut self.players[i as usize];
            let card = player.play(&valid_cards, &scores, &self.trick);
            player.play_card(card);
            self.trick.play(card, i);

            i = i.next_clockwise(1);
        }

        self.change_state(GameState::Play);
    }

    fn end_of_round(&mut self) {
        let scores = self.get_scores();

        /* Signal the players that the round is over */
        self.players[Side::NORTH as usize].round_end(&scores);
        self.players[Side::EAST as usize].round_end(&scores);
        self.players[Side::WEST as usize].round_end(&scores);

        self.players[0].update_score();
        self.players[1].update_score();
        self.players[2].update_score();
        self.players[3].update_score();

        self.score_history.push(ScoreValues {
            north: self.players[0].score_total,
            east: self.players[1].score_total,
            south: self.players[2].score_total,
            west: self.players[3].score_total,
        });

        /* end of game test */
        let score_value = i32::max(
            i32::max(self.players[0].score_total, self.players[1].score_total),
            i32::max(self.players[2].score_total, self.players[3].score_total),
        );

        if score_value >= self.game_options.ruleset.score_limit() {
            // If user is tied for win, make him win
            let winner = Side::ALL
                .iter()
                .cloned()
                .min_by_key(|side| {
                    (
                        self.players[(*side) as usize].score_total,
                        *side != self.user,
                    )
                })
                .unwrap();

            self.change_state(GameState::End { winner });
        } else {
            self.change_state(GameState::RoundEnd);
        }

        self.on_round_end.as_ref().map(|ref func| (func)());
    }

    /* pass cards to the next player */
    pub fn pass_cards(&mut self) {
        /* pass the cards and make a copy in the history for trick reset support */
        let mut temp = HashMap::<Side, Pass>::new();
        for side in &Side::ALL {
            let cards = self.players[*side as usize].draw_selected();
            temp.insert(*side + self.pass, cards);
        }
        for side in &Side::ALL {
            self.players[*side as usize].receive_cards(&temp[side]);
        }
        self.change_state(GameState::RecieveCards);
    }

    /* (de)select cards in the passing round */
    fn select_cards(&mut self, active_card: Option<Card>) {
        /* You cannot select the active card if there are already 3 selected cards */
        if let Some(active_card) = active_card {
            let user = &mut self.players[self.user as usize];
            user.toggle_select(active_card);
        }

        let num_selected = self.players[self.user as usize].num_selected();

        /* if there are 3 cards selected, cards can be passed */
        if num_selected == 3 {
            self.change_state(GameState::PassCards);
        } else {
            self.change_state(GameState::SelectCards);
        }
    }

    pub fn get_scores(&self) -> Score {
        Score {
            round: ScoreValues {
                north: self.players[0].score_round,
                east: self.players[1].score_round,
                south: self.players[2].score_round,
                west: self.players[3].score_round,
            },
            game: ScoreValues {
                north: self.players[0].score_total + self.players[0].score_round,
                east: self.players[1].score_total + self.players[1].score_round,
                south: self.players[2].score_total + self.players[2].score_round,
                west: self.players[3].score_total + self.players[3].score_round,
            },
        }
    }

    /* returns wether a card is valid to play from this hand on the stack */
    pub fn is_valid_card(&self, card: Card, hand: &[Card]) -> bool {
        /* special case: opening with a two of clubs (variant) */
        if self.game_options.clubs_lead && hand.len() == 13 && self.trick.num_played() == 0 {
            return card == Card::TWO_OF_CLUBS;
        }

        /* special case, no blood in the first round, unless we have to */
        if self.game_options.no_blood && is_point_card(card) && hand.len() == 13 {
            if hand.iter().any(|c| !is_point_card(*c)) {
                return false;
            }
        }

        /* special case: hearts must have been "broken" to be played as trump (variant).
        If a player has all hearts, then one must be played anyway */
        if self.game_options.hearts_break
            && self.trick.num_played() == 0
            && !self.hearts_broken
            && card.suit == Suit::HEARTS
            && hand.iter().any(|card| card.suit != Suit::HEARTS)
        {
            return false;
        }

        /* follow the trump if possible */
        if let Some(trump) = self.trick.trump() {
            if card.suit != trump && hand.iter().any(|card| card.suit == trump) {
                return false;
            }
        }

        /* no more reason to disallow a card */
        true
    }

    pub fn get_valid_cards(&self, side: Side) -> Cards {
        let player = &self.players[side as usize];
        let cards: Vec<_> = player
            .hand
            .iter()
            .filter(|card| self.is_valid_card(**card, &player.hand))
            .cloned()
            .collect();
        Cards::from_slice(&cards).unwrap()
    }

    /* get a hint string */
    pub fn get_hint(&self) -> String {
        let user = &self.players[self.user as usize];
        let mut ai = Opponent::John.create_ai(Side::NORTH, &self.game_options);

        match self.state {
            GameState::Play => {
                if self.trick.num_played() == 4 {
                    return tr("Click somewhere to continue the game.");
                }

                ai.update(&user.hand, &self.get_scores());
                let valid_cards = self.get_valid_cards(self.user);
                let card = ai.play_card(&valid_cards, &self.trick);
                if valid_cards.contains(card) {
                    return format!("Play {}.", card.get_name());
                }
            }
            GameState::PassCards => {
                let name = &self.players[(self.user + self.pass) as usize].name;
                return format!("Click on the hand of {} to pass the cards.", name);
            }
            GameState::SelectCards => {
                ai.update(&user.hand, &self.get_scores());
                let cards = ai.select_cards(self.user + self.pass);
                for card in cards.to_array() {
                    if !user.is_selected(card) {
                        return format!("Pass {}.", card.get_name());
                    }
                }
            }
            _ => {}
        }
        tr("Sorry, I can't help you.")
    }

    pub fn clicked(&mut self, side: Option<Side>, active_card: Option<Card>) -> bool {
        match self.state {
            GameState::SelectCards => {
                self.select_cards(active_card);
                true
            }
            GameState::PassCards => {
                let do_pass =
                    self.pass != PassDirection::NoPass && side == Some(self.user + self.pass);
                if do_pass {
                    self.pass_cards();
                } else {
                    self.select_cards(active_card);
                }
                true
            }
            GameState::RecieveCards => {
                /* start the round */
                self.players[0].deselect();
                self.players[1].deselect();
                self.players[2].deselect();
                self.players[3].deselect();
                self.open_round();
                true
            }
            GameState::Play => {
                /* see if we're done playing this trick */
                if self.trick.num_played() == 4 {
                    /* start the next trick in this game */
                    self.trick = Trick::new();

                    /* is this also the end of the round or game? */
                    if self.players[self.user as usize].hand.is_empty() {
                        self.end_of_round();
                        return true;
                    }

                    let scores = self.get_scores();

                    /* play cards until it's the user's turn */
                    let mut i = self.trick_winner;
                    while i != self.user {
                        let valid_cards = self.get_valid_cards(i);

                        let player = &mut self.players[i as usize];
                        let card = player.play(&valid_cards, &scores, &self.trick);
                        player.play_card(card);
                        self.trick.play(card, i);

                        i = i.next_clockwise(1);
                    }

                    return true;
                }

                /* loop through the hand to find the active card and make sure it's valid.
                 * Apparently it's sometimes possible to play an invalid card. See bug #30.
                 */
                match active_card {
                    None => false,
                    Some(card) => {
                        if !self.is_valid_card(card, &self.players[self.user as usize].hand) {
                            return false;
                        }

                        self.players[self.user as usize].play_card(card);
                        self.trick.play(card, self.user);

                        let scores = self.get_scores();

                        /* play cards until the trick is over */
                        let mut i = self.user;
                        while self.trick.num_played() < 4 {
                            i = i.next_clockwise(1);
                            let valid_cards = self.get_valid_cards(i);
                            let card =
                                self.players[i as usize].play(&valid_cards, &scores, &self.trick);
                            self.players[i as usize].play_card(card);
                            self.trick.play(card, i);
                        }

                        /* update the game status and score */
                        let hearts_broken = if self.game_options.queen_breaks_hearts {
                            self.trick.num_point_cards() > 0
                        } else {
                            self.trick.num_suit(Suit::HEARTS) > 0
                        };
                        if hearts_broken {
                            self.hearts_broken = true;
                        }

                        assert!(self.trick.num_played() == 4);

                        self.trick_winner = self.trick.get_winner_side().unwrap();
                        self.players[self.trick_winner as usize].take_trick(&self.trick);

                        /* show the tricks to the AI's */
                        let scores = self.get_scores();
                        self.players[0].trick_end(&scores, &self.trick);
                        self.players[1].trick_end(&scores, &self.trick);
                        self.players[3].trick_end(&scores, &self.trick);

                        true
                    }
                }
            }
            GameState::RoundEnd => {
                self.new_round();
                true
            }
            GameState::End { .. } => {
                self.new_game();
                true
            }
        }
    }
}
