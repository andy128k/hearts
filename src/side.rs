use num::FromPrimitive;
use num_derive::FromPrimitive;
use std::ops::Add;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, FromPrimitive)]
pub enum Side {
    NORTH = 0,
    EAST,
    SOUTH,
    WEST,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum PassDirection {
    NoPass = 0,
    Left,
    Across,
    Right,
}

impl Side {
    pub const ALL: [Side; 4] = [Side::NORTH, Side::EAST, Side::SOUTH, Side::WEST];

    pub fn next_clockwise(&self, count: usize) -> Self {
        Self::ALL[((*self as usize) + count) % 4]
    }
}

impl Add<PassDirection> for Side {
    type Output = Side;
    fn add(self, rhs: PassDirection) -> Side {
        Side::from_i8(((self as i8) + (rhs as i8)) % 4).unwrap()
    }
}

impl PassDirection {
    pub fn next(self) -> Self {
        [
            PassDirection::NoPass,
            PassDirection::Left,
            PassDirection::Across,
            PassDirection::Right,
        ][((self as usize) + 1) % 4]
    }
}
