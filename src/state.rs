use crate::config::Config;
use crate::game::Game;
use crate::playing_area::PlayingArea;
use gtk::{Application, ApplicationWindow, Statusbar, Toolbar};
use std::cell::RefCell;
use std::rc::Rc;

pub struct State {
    pub app: Application,
    pub window: ApplicationWindow,
    pub toolbar: Toolbar,
    pub playingarea: Rc<PlayingArea>,
    pub statusbar: Statusbar,
    pub config: Config,
    pub game: Rc<RefCell<Game>>,
}
