use crate::side::Side;

#[derive(Default, Clone)]
pub struct ScoreValues {
    pub north: i32,
    pub east: i32,
    pub south: i32,
    pub west: i32,
}

impl ScoreValues {
    pub fn get(&self, side: Side) -> i32 {
        match side {
            Side::NORTH => self.north,
            Side::EAST => self.east,
            Side::SOUTH => self.south,
            Side::WEST => self.west,
        }
    }

    pub fn min(&self) -> i32 {
        *[self.north, self.east, self.south, self.west]
            .iter()
            .min()
            .unwrap()
    }

    pub fn max(&self) -> i32 {
        *[self.north, self.east, self.south, self.west]
            .iter()
            .max()
            .unwrap()
    }
}

#[derive(Default, Clone)]
pub struct Score {
    pub round: ScoreValues,
    pub game: ScoreValues,
}
