use gdk::WindowTypeHint;
use gio::SimpleAction;
use glib::variant::Variant;
use gtk::prelude::*;
use gtk::{show_uri_on_window, AboutDialog, Dialog, DialogFlags, Label, ResponseType};

use crate::game::*;
use crate::i18n::*;
use crate::preferences::*;
use crate::score_dlg::*;
use crate::state::State;

/* New game menu item and button */
pub fn on_new_event(state: &State) {
    state.game.borrow_mut().new_game();
    state.playingarea.get_widget().queue_draw();
}

/* Restart menu item and button */
pub fn on_restart_event(state: &State) {
    state.game.borrow_mut().restart_round();
    state.playingarea.get_widget().queue_draw();
}

/* Show scores menu item */
pub fn on_scores_event(state: &State) {
    let game = state.game.borrow();
    let names = [
        game.players[0].name.as_str(),
        game.players[1].name.as_str(),
        game.players[2].name.as_str(),
        game.players[3].name.as_str(),
    ];
    let title = match game.state {
        GameState::End { winner } if winner == game.user => tr("Game over - You have won!"),
        GameState::End { winner } => format!("Game over - {} has won!", names[winner as usize]),
        _ => tr("Scores"),
    };
    show_scores(
        &state.window.clone().upcast(),
        &title,
        &names,
        &game.score_history,
    );
}

pub fn on_help(state: &State) {
    if let Err(error) = show_uri_on_window(Some(&state.window), "help:hearts", 0) {
        eprintln!("{}", error);
    }
}

pub fn on_about_event(state: &State) {
    let dlg = AboutDialog::new();
    dlg.set_transient_for(Some(&state.window));
    dlg.set_type_hint(WindowTypeHint::Normal);
    dlg.set_program_name("Gnome Hearts");
    dlg.set_version(Some(env!("CARGO_PKG_VERSION")));
    dlg.set_copyright(Some(
        "Copyright \
        2006-2008, Stichting Lone Wolves, \
        2018-2021 Andrey Kutejko",
    ));
    dlg.set_comments(Some("A hearts game for GNOME by Sander Marechal"));
    dlg.set_website(Some("https://gitlab.gnome.org/andy128k/hearts"));
    dlg.set_website_label(Some("Hearts for GNOME"));
    dlg.set_authors(&["Sander Marechal"]);
    dlg.set_icon_name(Some("gnome-hearts"));
    dlg.set_logo_icon_name(Some("gnome-hearts"));
    dlg.run();
    dlg.close();
}

pub fn on_hint_event(state: &State) {
    let hint = state.game.borrow().get_hint();

    let dlg = Dialog::with_buttons(
        Some("Hint"),
        Some(&state.window),
        DialogFlags::MODAL,
        &[("Close", ResponseType::Reject)],
    );
    dlg.set_transient_for(Some(&state.window));
    dlg.set_resizable(false);

    let label = Label::new(Some(&hint));
    label.set_margin_top(8);
    label.set_margin_bottom(8);
    label.set_margin_start(8);
    label.set_margin_end(8);

    dlg.get_content_area().add(&label);

    dlg.show_all();
    dlg.run();
    dlg.close();
}

pub fn on_preferences_event(state: &mut State) {
    let new_config = preferences(&state.window.clone().upcast(), &state.config);

    println!("{:#?}", new_config);

    // TODO: game_new(); POSSIBLE if rules or players are changed otherwise redraw playingarea
}

/* Fullscreen */
pub fn on_fullscreen_event(action: &SimpleAction, value: Option<&Variant>, state: &State) {
    if let Some(value) = value {
        if value.get::<bool>().unwrap_or_default() {
            state.window.fullscreen();
        } else {
            state.window.unfullscreen();
        }
        action.set_state(value);
    }
}

/* Toolbar toggle menu item */
pub fn on_toolbar_toggled(action: &SimpleAction, value: Option<&Variant>, state: &State) {
    if let Some(value) = value {
        if value.get::<bool>().unwrap_or_default() {
            state.toolbar.show_all();
        } else {
            state.toolbar.hide();
        }
        action.set_state(value);
    }
}

/* Statusbar toggle menu item */
pub fn on_statusbar_toggled(action: &SimpleAction, value: Option<&Variant>, state: &State) {
    if let Some(value) = value {
        if value.get::<bool>().unwrap_or_default() {
            state.statusbar.show_all();
        } else {
            state.statusbar.hide();
        }
        action.set_state(value);
    }
}
