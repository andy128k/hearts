use crate::cards::*;
use std::collections::HashSet;
use std::iter::Iterator;

#[derive(Clone)]
pub struct Pass(HashSet<Card>);

impl Pass {
    pub fn new() -> Self {
        Pass(HashSet::new())
    }

    pub fn add(&mut self, card: Card) -> bool {
        if !self.is_full() {
            self.0.insert(card);
            true
        } else {
            false
        }
    }

    pub fn add_all<'any>(&mut self, cards: impl Iterator<Item = &'any Card>) {
        for card in cards {
            self.add(*card);
        }
    }

    pub fn toggle(&mut self, card: Card) {
        if self.0.contains(&card) {
            self.0.remove(&card);
        } else if !self.is_full() {
            self.0.insert(card);
        }
    }

    pub fn remove(&mut self, card: Card) {
        self.0.remove(&card);
    }

    pub fn clear(&mut self) {
        self.0.clear();
    }

    pub fn is_full(&self) -> bool {
        self.0.len() == 3
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn contains(&self, card: Card) -> bool {
        self.0.contains(&card)
    }

    pub fn to_array(&self) -> Vec<Card> {
        self.0.iter().cloned().collect()
    }
}
