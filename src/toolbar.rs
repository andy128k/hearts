use gtk::prelude::*;
use gtk::{IconSize, Image, SeparatorToolItem, ToggleToolButton, ToolButton, Toolbar};

fn button(label: &str, icon: &str, action: &str) -> ToolButton {
    let image = Image::from_icon_name(Some(icon), IconSize::SmallToolbar.into());
    let item = ToolButton::new(Some(&image), Some(label));
    item.set_action_name(Some(action));
    item
}

pub fn create_tool_bar() -> Toolbar {
    let toolbar = Toolbar::new();

    toolbar.add(&button("New game", "document-new", "app.new"));
    toolbar.add(&button("Restart game", "view-refresh", "app.restart"));

    toolbar.add(&SeparatorToolItem::new());

    toolbar.add(&button("Hint", "dialog-information", "app.hint"));

    toolbar.add(&SeparatorToolItem::new());

    toolbar.add(&{
        let item = ToggleToolButton::new();
        item.set_label(Some("Fullscreen"));
        item.set_icon_name(Some("view-fullscreen"));
        item.set_action_name(Some("app.fullscreen"));
        item
    });

    toolbar
}
