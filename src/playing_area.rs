use crate::card_style::CardsImage;
use crate::cards::*;
use crate::config::StyleOptions;
use crate::game::*;
use crate::player::Player;
use crate::side::*;
use cairo::{Context, Extend, Format, ImageSurface, SurfacePattern};
use gdk::prelude::*;
use gdk::{EventButton, EventMask, EventMotion};
use gdk_pixbuf::Pixbuf;
use gio::prelude::*;
use gio::{Cancellable, File, MemoryInputStream};
use glib::{clone, Bytes};
use gtk::prelude::*;
use gtk::{DrawingArea, Widget};
use itertools::Itertools;
use pangocairo::functions::show_layout;
use std::cell::{Cell, RefCell};
use std::cmp::Ord;
use std::path::PathBuf;
use std::rc::Rc;

const REVEAL_CARDS: bool = false;

struct Rect {
    x: i32,
    y: i32,
    width: i32,
    height: i32,
}

impl Rect {
    fn contains(&self, x: f64, y: f64) -> bool {
        x >= self.x.into()
            && x < (self.x + self.width).into()
            && y >= self.y.into()
            && y < (self.y + self.height).into()
    }
}

fn create_pattern_from_pixbuf(pixbuf: &Pixbuf) -> SurfacePattern {
    let image =
        ImageSurface::create(Format::Rgb24, pixbuf.get_width(), pixbuf.get_height()).unwrap();

    {
        let cr = Context::new(&image);
        cr.set_source_pixbuf(pixbuf, 0.0, 0.0);
        cr.paint();
    }

    let pattern = SurfacePattern::create(&image);
    pattern.set_extend(Extend::Repeat);

    pattern
}

pub struct PlayingArea {
    area: DrawingArea,
    cards_image: RefCell<CardsImage>,
    game: Rc<RefCell<Game>>,
    active_card_index: Cell<Option<usize>>,
    options: StyleOptions,

    background: Pixbuf,
    background_pattern: SurfacePattern,
}

const DEFAULT_BACKGROUND: &[u8] = include_bytes!("../pixmaps/baize.png");

fn load_background(path: &Option<PathBuf>) -> (Pixbuf, SurfacePattern) {
    let background = match path {
        Some(path) => Pixbuf::from_file(path).unwrap(),
        None => {
            let stream = MemoryInputStream::from_bytes(&Bytes::from(&DEFAULT_BACKGROUND[..]));
            Pixbuf::from_stream(&stream, None::<&Cancellable>).unwrap()
        }
    };
    let background_pattern = create_pattern_from_pixbuf(&background);
    (background, background_pattern)
}

const DEFAULT_CARDS: &[u8] = include_bytes!("../pixmaps/dondorf.svg");

fn load_cards(path: &Option<PathBuf>) -> CardsImage {
    match path {
        Some(path) => {
            let stream = File::new_for_path(path).read(None::<&Cancellable>).unwrap();
            CardsImage::from_stream(stream).unwrap()
        }
        None => {
            let stream = MemoryInputStream::from_bytes(&Bytes::from(&DEFAULT_CARDS[..]));
            CardsImage::from_stream(stream).unwrap()
        }
    }
}

impl PlayingArea {
    pub fn new(options: &StyleOptions, game: &Rc<RefCell<Game>>) -> Rc<Self> {
        let (background, background_pattern) = load_background(&options.background_image);
        let cards_image = load_cards(&options.card_style);

        let area = DrawingArea::new();
        area.set_size_request(600, 400);
        area.set_hexpand(true);
        area.set_vexpand(true);
        area.set_events(EventMask::POINTER_MOTION_MASK | EventMask::BUTTON_PRESS_MASK);

        let pa = Rc::new(Self {
            area,
            cards_image: RefCell::new(cards_image),
            game: game.clone(),
            active_card_index: Cell::new(None),
            options: options.clone(),
            background,
            background_pattern,
        });

        pa.area.connect_configure_event(
            clone!(@weak pa => @default-return false, move |_area, _event| {
                pa.on_configure_event();
                false
            }),
        );
        pa.area.connect_draw(
            clone!(@weak pa => @default-return gtk::Inhibit(false), move |_area, context| {
                pa.draw(context);
                gtk::Inhibit(false)
            }),
        );
        pa.area.connect_button_press_event(
            clone!(@weak pa => @default-return gtk::Inhibit(false), move |_area, event| {
                pa.button_press(event);
                gtk::Inhibit(true)
            }),
        );
        pa.area.connect_motion_notify_event(
            clone!(@weak pa => @default-return gtk::Inhibit(false), move |_area, event| {
                pa.mouse_motion(event);
                gtk::Inhibit(true)
            }),
        );

        pa
    }

    pub fn get_widget(&self) -> Widget {
        self.area.clone().upcast()
    }

    fn render_background(&self, cr: &Context, width: i32, height: i32) {
        cr.save();
        if self.options.background_stretched {
            cr.scale(
                (width as f64) / (self.background.get_width() as f64),
                (height as f64) / (self.background.get_height() as f64),
            );
        }
        cr.set_source(&self.background_pattern);
        cr.fill();
        cr.paint();
        cr.restore();
    }

    fn on_configure_event(&self) {
        let allocation = self.area.get_allocation();

        let width = allocation.width / 8;
        let height = allocation.height / 5;

        /* resize the background and cards */
        self.cards_image.borrow_mut().set_card_size(width, height);
    }

    fn draw(&self, cr: &Context) {
        let allocation = self.area.get_allocation();

        /* Fill the buffer with the background image */
        self.render_background(cr, allocation.width, allocation.height);

        /* render the hands */
        for player in &self.game.borrow().players {
            let area = self.get_hand_area(
                player.direction,
                player.hand.len(),
                allocation.width,
                allocation.height,
                &self.cards_image.borrow(),
            );

            self.render_hand(player, &self.cards_image.borrow(), cr, area.x, area.y);

            let name = if self.options.show_scores {
                format!(
                    "<span foreground=\"white\"><b>{} ({}/{})</b></span>",
                    player.name,
                    player.score_round,
                    player.score_total + player.score_round
                )
            } else {
                format!("<span foreground=\"white\"><b>{}</b></span>", player.name)
            };

            let layout = self.area.create_pango_layout(None);
            layout.set_markup(&name);
            let (n_width, n_height) = layout.get_pixel_size();
            let (n_x, n_y) = match player.direction {
                Side::NORTH => ((allocation.width - n_width) / 2, area.y + area.height + 5),
                Side::EAST => (area.x - n_width - 15, (allocation.height - n_height) / 2),
                Side::SOUTH => ((allocation.width - n_width) / 2, area.y - n_height - 5),
                Side::WEST => (area.x + area.width + 15, (allocation.height - n_height) / 2),
            };

            cr.save();
            cr.translate(n_x.into(), n_y.into());
            show_layout(cr, &layout);
            cr.restore();
        }

        {
            /* render the trick */
            cr.translate(
                (allocation.width / 2).into(),
                (allocation.height / 2).into(),
            );
            self.render_trick(cr);
        }
    }

    fn button_press(&self, event: &EventButton) {
        if event.get_button() != 1 {
            return;
        }

        let allocation = self.area.get_allocation();

        let (cx, cy) = event.get_position();
        let mut click_side = None;

        for side in &Side::ALL {
            let area = self.get_hand_area(
                *side,
                self.game.borrow().players[*side as usize].hand.len(),
                allocation.width,
                allocation.height,
                &self.cards_image.borrow(),
            );
            if area.contains(cx, cy) {
                click_side = Some(*side);
            }
        }

        let active_card = self
            .active_card_index
            .get()
            .and_then(|index| self.get_user_hand().get(index).cloned());
        let redraw = self.game.borrow_mut().clicked(click_side, active_card);

        if redraw {
            self.area.queue_draw();
        }
    }

    /** returns the x,y,width,height for a hand */
    fn get_hand_area(
        &self,
        direction: Side,
        length: usize,
        allocation_width: i32,
        allocation_height: i32,
        image: &CardsImage,
    ) -> Rect {
        let (card_width, card_height) = image.card_size();
        let offset_x = card_width / 4;
        let offset_y = card_height / 4;

        if direction == Side::NORTH || direction == Side::SOUTH {
            let width = (length as i32 * offset_x) + (card_width - offset_x);
            let height = card_height + offset_y;
            let x = (allocation_width - width) / 2;
            let y = if direction == Side::NORTH {
                10
            } else {
                allocation_height - height - 10
            };
            Rect {
                x,
                y,
                width,
                height,
            }
        } else {
            let height = (length as i32 * offset_y) + (card_height - offset_y);
            let width = card_width + offset_x;
            let y = (allocation_height - height) / 2;
            let x = if direction == Side::WEST {
                10
            } else {
                allocation_width - width - 10
            };
            Rect {
                x,
                y,
                width,
                height,
            }
        }
    }

    fn get_user_hand(&self) -> Vec<Card> {
        let user = &self.game.borrow().players[self.game.borrow().user as usize];
        user.hand
            .to_vec()
            .into_iter()
            .sorted_by_key(card_order_key)
            .collect::<Vec<_>>()
    }

    fn get_hand(&self, player: &Player) -> Vec<Card> {
        if self.game.borrow().user == player.direction {
            self.get_user_hand()
        } else {
            player.hand.to_vec()
        }
    }

    fn render_hand(
        &self,
        player: &Player,
        cards: &CardsImage,
        cr: &Context,
        mut x: i32,
        mut y: i32,
    ) {
        let (card_width, card_height) = cards.card_size();

        let mut offset_x = 0;
        let mut offset_y = 0;
        match player.direction {
            Side::NORTH => {
                offset_y = card_height / 4;
            }
            Side::EAST => {
                offset_x = card_width / -4;
                x -= offset_x;
            }
            Side::SOUTH => {
                offset_y = card_height / -4;
                y -= offset_y;
            }
            Side::WEST => {
                offset_x = card_width / 4;
            }
        }

        for (i, card) in self.get_hand(player).into_iter().enumerate() {
            /* get the image ID */
            let id = if self.game.borrow().user == player.direction || REVEAL_CARDS {
                card.id()
            } else {
                CardsImage::CARD_BACK
            };

            cr.save();
            /* selected cards are pushed up */
            if player.is_selected(card) {
                cr.translate(f64::from(x + offset_x), f64::from(y + offset_y));
            } else {
                cr.translate(f64::from(x), f64::from(y));
            }
            cards.render(
                id,
                self.game.borrow().user == player.direction
                    && self.active_card_index.get() == Some(i),
                cr,
            );
            cr.restore();

            /* position of the next card */
            if player.direction == Side::NORTH || player.direction == Side::SOUTH {
                x += card_width / 4;
            } else {
                y += card_height / 4;
            }
        }
    }

    fn render_trick(&self, cr: &Context) {
        let (card_width, card_height) = self.cards_image.borrow().card_size();
        let card_width = card_width as f64;
        let card_height = card_height as f64;
        let trick = &self.game.borrow().trick;
        for (side, card) in trick.iter() {
            let (cx, cy) = match side {
                Side::NORTH => (-card_width / 2.0, -0.75 * card_height),
                Side::EAST => (0.0, -0.50 * card_height),
                Side::SOUTH => (-card_width / 2.0, -0.25 * card_height),
                Side::WEST => (-card_width, -0.50 * card_height),
            };
            cr.save();
            cr.translate(cx, cy);
            self.cards_image.borrow().render(card.id(), false, cr);
            cr.restore();
        }
    }

    fn mouse_motion(&self, event: &EventMotion) {
        let mut update = false;

        let allocation = self.area.get_allocation();
        let (cx, cy) = event.get_position();

        let (card_width, card_height) = self.cards_image.borrow().card_size();

        let offset_x = card_width / 4;
        let offset_y = card_height / 4;

        let user = &self.game.borrow().players[self.game.borrow().user as usize];

        let area = self.get_hand_area(
            self.game.borrow().user,
            user.hand.len(),
            allocation.width,
            allocation.height,
            &self.cards_image.borrow(),
        );

        if !area.contains(cx, cy) {
            if self.active_card_index.get().is_some() {
                self.active_card_index.set(None);
                update = true;
            }
        } else {
            let mut new_active = None;

            /* loop through the hand and mark the new active card */
            for (i, card) in self.get_user_hand().into_iter().enumerate() {
                /* calculate the card's onscreen position */
                let card_rect = Rect {
                    x: area.x + offset_x * (i as i32),
                    y: if user.is_selected(card) {
                        area.y
                    } else {
                        area.y + offset_y
                    },
                    width: card_width,
                    height: card_height,
                };

                /* is the cursor on the card? */
                if card_rect.contains(cx, cy) {
                    /* update the card */
                    if self.game.borrow().state != GameState::Play
                        || self.game.borrow().is_valid_card(card, &user.hand)
                    {
                        new_active = Some(i);
                    }
                }
            }

            if self.active_card_index.get() != new_active {
                self.active_card_index.set(new_active);
                update = true;
            }
        }

        if update {
            self.area.queue_draw();
        }
    }
}

fn card_order_key(card: &Card) -> impl Ord {
    (
        match card.suit {
            Suit::CLUBS => 1,
            Suit::DIAMONDS => 2,
            Suit::SPADES => 3,
            Suit::HEARTS => 4,
        },
        card.rank,
    )
}
