use crate::cards::*;
use crate::side::*;
use std::iter::Iterator;

pub struct Trick(Vec<(Side, Card)>);

impl Trick {
    pub fn new() -> Self {
        Trick(Vec::with_capacity(4))
    }

    /// number of cards played
    pub fn num_played(&self) -> usize {
        self.0.len()
    }

    /// first card to be played
    pub fn first_played_card(&self) -> Option<&Card> {
        self.0.first().map(|(_side, card)| card)
    }

    pub fn get_card(&self, player: Side) -> Option<Card> {
        self.0
            .iter()
            .filter(|(side, _card)| *side == player)
            .map(|(_side, card)| *card)
            .next()
    }

    /// Returns true if player has already played to the current trick.
    pub fn has_played(&self, player: Side) -> bool {
        self.0
            .iter()
            .filter(|(side, _card)| *side == player)
            .count()
            > 0
    }

    /// suit of the first card that was played
    pub fn trump(&self) -> Option<Suit> {
        self.0.first().map(|(_side, card)| card.suit)
    }

    /// return the winning side and card in this trick
    pub fn winner(&self) -> Option<&(Side, Card)> {
        let trump = self.trump()?;
        self.0
            .iter()
            .filter(|(_side, card)| card.suit == trump)
            .max_by_key(|(_side, card)| card.rank)
    }

    /// return the winning side in this trick
    pub fn get_winner_side(&self) -> Option<Side> {
        self.winner().map(|(side, _card)| *side)
    }

    /// return the winning card in this trick
    pub fn get_winner_card(&self) -> Option<&Card> {
        self.winner().map(|(_side, card)| card)
    }

    pub fn get_cost(&self, ruleset: Ruleset) -> i32 {
        self.iter_cards().map(|c| ruleset.card_cost(c)).sum()
    }

    /// get the score of a trick
    pub fn get_score(&self, ruleset: Ruleset) -> i32 {
        let cards: Vec<_> = self.iter_cards().map(|c| *c).collect();
        cards_get_points(&cards, ruleset)
    }

    /** return the number of suit cards in the trick */
    pub fn num_suit(&self, suit: Suit) -> usize {
        self.0
            .iter()
            .filter(|(_side, card)| card.suit == suit)
            .count()
    }

    /** return the number of point cards in the trick (sans bonus cards) */
    pub fn num_point_cards(&self) -> usize {
        self.0
            .iter()
            .filter(|(_side, card)| is_point_card(*card))
            .count()
    }

    pub fn iter(&self) -> impl Iterator<Item = &(Side, Card)> {
        self.0.iter()
    }

    pub fn iter_cards(&self) -> impl Iterator<Item = &Card> {
        self.0.iter().map(|(_suit, card)| card)
    }

    /// Return a list of players who have not played to the trick
    pub fn remaining_players(&self) -> Vec<Side> {
        Side::ALL
            .iter()
            .filter(|side| !self.iter().any(|(s, _)| s == *side))
            .map(|side| *side)
            .collect()
    }

    pub fn play(&mut self, card: Card, side: Side) {
        self.0.push((side, card));
    }

    pub fn contains(&self, card: &Card) -> bool {
        self.iter_cards().any(|c| c == card)
    }
}
