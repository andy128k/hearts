use gdk::prelude::*;
use gdk_pixbuf::Pixbuf;

fn active_cards(pixbuf: &Pixbuf, red: u16, green: u16, blue: u16) -> Pixbuf {
    fn highlight(c: u8, factor: u16) -> u8 {
        c.saturating_add(((0xFF - u16::from(c)) * factor / 0xFF) as u8)
    }

    let active = pixbuf.copy().unwrap();

    active.saturate_and_pixelate(&active, 0.0, false);

    {
        let pixels = unsafe { active.get_pixels() };
        let width = active.get_width();
        let height = active.get_height();
        let rowstride = active.get_rowstride();

        for py in 0..height {
            for px in 0..width {
                let offset = (py * rowstride + px * 4) as usize;
                let p = &mut pixels[offset..offset + 4];
                p[0] = highlight(p[0], red);
                p[1] = highlight(p[1], green);
                p[2] = highlight(p[1], blue);
            }
        }
    }

    active
}

pub struct CardsImage {
    image: Pixbuf,
    active: Pixbuf,
    /* width and height of a single card */
    width: i32,
    height: i32,
}

impl CardsImage {
    pub fn from_stream(stream: impl IsA<gio::InputStream>) -> Result<Self, glib::Error> {
        let image = Pixbuf::from_stream(&stream, None::<&gio::Cancellable>)?;
        let active = active_cards(&image, 0, 0, 170);
        let width = image.get_width() / 13;
        let height = image.get_height() / 5;
        Ok(Self {
            image,
            active,
            width,
            height,
        })
    }

    pub fn card_size(&self) -> (i32, i32) {
        (self.width, self.height)
    }

    fn original_width(&self) -> i32 {
        self.image.get_width() / 13
    }

    fn original_height(&self) -> i32 {
        self.image.get_height() / 5
    }

    pub fn set_card_size(&mut self, mut width: i32, mut height: i32) {
        let original_width = self.original_width();
        let original_height = self.original_height();

        /* recalculate the width and height with respect to the height/width ratio */
        if width * original_height < height * original_width {
            height = width * original_height / original_width;
        } else {
            width = height * original_width / original_height;
        }

        /* resize the cards */
        self.width = width;
        self.height = height;
    }

    pub const CARD_BACK: i32 = 54;

    pub fn render(&self, card_id: i32, active: bool, cr: &cairo::Context) {
        let pixbuf = if active { &self.active } else { &self.image };

        let card_width = self.original_width();
        let card_height = self.original_height();

        /* calculate card position in the image */
        let sx = (card_id % 13) * card_width;
        let sy = (card_id / 13) * card_height;

        cr.save();

        let scale = self.width as f64 / card_width as f64;
        cr.scale(scale, scale);

        cr.rectangle(0.0, 0.0, card_width as f64, card_height as f64);
        cr.clip();

        cr.set_source_pixbuf(pixbuf, (-sx).into(), (-sy).into());
        cr.paint();

        cr.restore();
    }
}
