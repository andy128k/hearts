use crate::ai::player::PlayerAI;
use crate::card_list::*;
use crate::cards::*;
use crate::config::*;
use crate::pass::*;
use crate::score::*;
use crate::side::*;
use crate::trick::*;
use glib::get_real_name;
use std::mem::swap;

pub struct Player {
    pub direction: Side,
    pub score_round: i32,
    pub score_total: i32,
    pub name: String,
    pub hand: Vec<Card>,
    pub history: Vec<Card>, /* copy of the original list of 13 cards (for reset purposes) */
    selected: Pass,
    cards_taken: Vec<Card>,
    ai: Option<Box<dyn PlayerAI>>,
    game_options: GameOptions,
}

fn get_user_name() -> Option<String> {
    let real_name = get_real_name()?;
    let string = real_name.to_str()?;
    Some(string.to_owned())
}

impl Player {
    pub fn new(direction: Side, ai: Option<Box<dyn PlayerAI>>, game_options: &GameOptions) -> Self {
        let name = match ai {
            Some(ref ai) => ai.get_name().to_owned(),
            None => get_user_name().unwrap_or_else(|| "You".to_owned()),
        };

        Self {
            direction,
            score_round: 0,
            score_total: 0,
            name,
            hand: Vec::new(),
            history: Vec::new(),
            selected: Pass::new(),
            cards_taken: Vec::new(),
            ai,
            game_options: game_options.clone(),
        }
    }

    pub fn draw_selected(&mut self) -> Pass {
        self.hand = self
            .hand
            .iter()
            .filter(|card| !self.selected.contains(**card))
            .cloned()
            .collect();
        let mut selected = Pass::new();
        swap(&mut selected, &mut self.selected);
        selected
    }

    pub fn deselect(&mut self) {
        self.selected.clear();
    }

    pub fn toggle_select(&mut self, card: Card) {
        self.selected.toggle(card);
    }

    pub fn is_selected(&self, card: Card) -> bool {
        self.selected.contains(card)
    }

    /** return the number of selected cards in this hand */
    pub fn num_selected(&self) -> usize {
        self.selected.len()
    }

    pub fn play(&mut self, valid_cards: &Cards, score: &Score, trick: &Trick) -> Card {
        if let Some(ref mut ai) = self.ai {
            ai.update(&self.hand, score);
            let card = ai.play_card(valid_cards, trick);

            /* Find the card in the hand */
            if !valid_cards.contains(card) {
                panic!("trying to play invalid card {:?}\n", card);
            }
            card
        } else {
            panic!("trying to play without brain"); // ???
        }
    }

    pub fn select_cards(&mut self, score: &Score, pass_side: Side) {
        if let Some(ref mut ai) = self.ai {
            ai.update(&self.hand, score);
            self.selected = ai.select_cards(pass_side);
        }
    }

    pub fn receive_cards(&mut self, pass: &Pass) {
        for card in pass.to_array() {
            self.hand.push(card);
        }
        if let Some(ref mut ai) = self.ai {
            ai.receive_cards(pass);
        }
    }

    /* show the trick to the AI */
    pub fn trick_end(&mut self, score: &Score, trick: &Trick) {
        if let Some(ref mut ai) = self.ai {
            ai.update(&self.hand, score);
            ai.trick_end(trick);
        }
    }

    /* Call an AI's round_end function */
    pub fn round_end(&mut self, score: &Score) {
        if let Some(ref mut ai) = self.ai {
            ai.round_end();
        }
    }

    /* play player's card on trick */
    pub fn play_card(&mut self, card: Card) {
        self.hand = self.hand.iter().filter(|c| **c != card).cloned().collect();
    }

    /* Add the cards from the trick to the player's list of taken card */
    pub fn take_trick(&mut self, trick: &Trick) {
        for card in trick.iter_cards() {
            self.cards_taken.push(*card);
        }
        /* Update the score for this round */
        self.score_round = cards_get_points(&self.cards_taken, self.game_options.ruleset);
    }

    pub fn draw(&mut self, cards: &[Card]) {
        self.hand = cards.to_vec();
        self.history = self.hand.clone();
        self.cards_taken = Vec::new();
        self.score_round = 0;
    }

    pub fn reset(&mut self) {
        self.hand = self.history.clone();
        self.cards_taken = Vec::new();
        self.score_round = 0;
    }

    pub fn has_two_of_clubs(&self) -> bool {
        self.hand.contains(&Card::TWO_OF_CLUBS)
    }

    pub fn update_score(&mut self) {
        self.score_total += self.score_round;
        self.score_round = 0;
    }
}
