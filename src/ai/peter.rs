use super::player::PlayerAI;
use crate::card_list::*;
use crate::cards::*;
use crate::config::GameOptions;
use crate::pass::*;
use crate::score::*;
use crate::side::*;
use crate::trick::*;
use itertools::Itertools;
use rand::{seq::SliceRandom, thread_rng};
use std::cmp::Ord;
use std::collections::HashSet;

/// Returns number of spades above queen in list
fn num_top_two_spades(list: &HashSet<Card>) -> usize {
    list.iter()
        .filter(|c| c.suit == Suit::SPADES && c.rank > Rank::QUEEN)
        .count()
}

/// Returns number of spades above jack in list
fn num_top_three_spades(list: &HashSet<Card>) -> usize {
    list.iter()
        .filter(|c| c.suit == Suit::SPADES && c.rank > Rank::JACK)
        .count()
}

/// Returns true if the hand contains
/// At least five spades
/// At least three spades below the queen
fn safe_spades(cards: &HashSet<Card>) -> bool {
    cards.iter().filter(|c| c.suit == Suit::SPADES).count() > 4
        || cards
            .iter()
            .filter(|c| c.suit == Suit::SPADES && c.rank < Rank::QUEEN)
            .count()
            > 2
}

#[derive(PartialEq, Eq)]
enum ReasonToShoot {
    TakenHalf,        // 2
    UnprotectedQueen, // 3
    ExitProblem,      // 7
    DuckableHearts,   // 4
    RunnableSuit,     // 5
    NonVoidMinorSuit, // 6
}

pub struct Peter {
    direction: Side,
    rules: GameOptions,
    hand: HashSet<Card>,
    hearts_played: bool,
    queen_played: bool,
    shooting: bool,
    hearts_split: bool,
    can_shoot: bool,
    pass_dir: Option<Side>,
    deck: HashSet<Card>,
    void_counts: HashSet<(Side, Suit)>,
    pass_cards: Pass,
    received_cards: HashSet<Card>,
    trick_num: usize,
    score: Score,
}

impl Peter {
    pub fn new(direction: Side, options: &GameOptions) -> Self {
        Self {
            direction,
            rules: options.clone(),
            hand: HashSet::new(),
            hearts_played: false,
            queen_played: false,
            shooting: false,
            hearts_split: false,
            can_shoot: true,
            pass_dir: None,
            deck: HashSet::new(),
            void_counts: HashSet::new(),
            pass_cards: Pass::new(),
            received_cards: HashSet::new(),
            trick_num: 1,
            score: Score::default(),
        }
    }

    /// is card a bonus card for Omnibus ruleset
    fn is_bonus_card(&self, card: Card) -> bool {
        match self.rules.ruleset {
            Ruleset::Omnibus => card == Card::new(Suit::DIAMONDS, Rank::JACK),
            Ruleset::OmnibusAlternative => card == Card::new(Suit::DIAMONDS, Rank::TEN),
            _ => false,
        }
    }

    fn select(&mut self, card: Card) {
        self.pass_cards.add(card);
        self.hand.remove(&card);
    }

    /// Returns true if suit_list contains
    /// Highest unplayed card in the suit
    /// or the second highest unplayed card
    /// and at least one lower card
    /// or the third highest unplayed card
    /// and at least two lower cards...
    fn is_entry_suit(&self, suit: Suit) -> bool {
        let suit_list: Vec<_> = self.hand.iter().filter(|c| c.suit == suit).collect();
        if let Some(highest) = suit_list.iter().max_by_key(|c| c.rank) {
            if self.num_above(**highest) < suit_list.len() {
                return true;
            }
        }
        false
    }

    /// Returns the number of cards in the suit which are still in the deck
    fn num_out(&self, suit: Suit) -> usize {
        self.deck.iter().filter(|c| c.suit == suit).count()
    }

    /// Returns number of outstanding cards in this suit which can duck this card
    fn num_below(&self, card: Card) -> usize {
        self.deck
            .iter()
            .filter(|c| c.suit == card.suit)
            .filter(|c| c.rank < card.rank)
            .count()
    }

    /// Returns number of outstanding cards in this suit which can beat this card
    fn num_above(&self, card: Card) -> usize {
        self.deck
            .iter()
            .filter(|c| c.suit == card.suit)
            .filter(|c| c.rank > card.rank)
            .count()
    }

    /// Returns true if, for each high card in decending sequence,
    /// and assuming that the highest outstanding card will fall on each trick,
    /// and that the outstanding cards are not all in the same hand, unless
    /// there is evidence to the contrary, every card in the suit will take a trick,
    /// and the suit is long enough to produce a reasonable number of discards
    fn is_runnable(&self, suit: Suit) -> bool {
        if self.num_discards(suit) == 0 {
            return false;
        }

        let suit_list = self
            .hand
            .iter()
            .filter(|c| c.suit == suit)
            .sorted_by_key(|c| -(c.rank as i32))
            .collect::<Vec<_>>();
        if suit_list.is_empty() {
            return false;
        }

        let deck_list = self
            .deck
            .iter()
            .filter(|c| c.suit == suit)
            .sorted_by_key(|c| -(c.rank as i32))
            .collect::<Vec<_>>();
        if deck_list.is_empty() {
            return true;
        }

        let mut deck_length = deck_list.len();
        for (index, card) in suit_list.iter().enumerate() {
            if index >= deck_length {
                return true;
            }
            if suit_list[index].rank < deck_list[index].rank {
                return false;
            }
            if self.num_players_have(suit) > 0 {
                deck_length = deck_length - 1;
            }
        }
        true
    }

    /// Returns true if, for each high card in decending sequence,
    /// and assuming that the other players will try to duck the trick,
    /// there is a reasonable chance that every card in the suit will take a trick.
    fn is_duckable(&self, suit: Suit) -> bool {
        let deck_list = self
            .deck
            .iter()
            .filter(|c| c.suit == suit)
            .sorted_by_key(|c| -(c.rank as i32))
            .collect::<Vec<_>>();
        let deck_length = deck_list.len();
        if deck_length == 0 {
            return true;
        }

        let suit_list = self
            .hand
            .iter()
            .filter(|c| c.suit == suit)
            .sorted_by_key(|c| -(c.rank as i32))
            .collect::<Vec<_>>();
        let suit_length = suit_list.len();
        if suit_length == 0 {
            return false;
        }

        let num_played = self.num_players_have(suit);
        let mut deck_index = 0;
        for card in suit_list {
            if deck_index >= deck_length {
                return false;
            }
            while card.rank < deck_list[deck_index].rank {
                deck_index += 1;
                if deck_index >= deck_length {
                    return false;
                }
            }
            if deck_length - deck_index < 5 {
                return false;
            }
            deck_index += num_played;
            if deck_index > deck_length {
                return false;
            }
        }
        true
    }

    /// Return a list of players who are void in the given suit
    fn players_void(&self, suit: Suit) -> HashSet<Side> {
        self.void_counts
            .iter()
            .filter(|(void_side, void_suit)| *void_suit == suit && *void_side != self.direction)
            .map(|(void_side, _void_suit)| *void_side)
            .collect()
    }

    /// Return the number of players who are not void in the given suit
    fn num_players_have(&self, suit: Suit) -> usize {
        3 - self.players_void(suit).len()
    }

    /// Returns the minimum number of discards the suit will draw if run.
    fn num_discards(&self, suit: Suit) -> usize {
        let suit_length = self.hand.iter().filter(|c| c.suit == suit).count();
        let deck_length = self.deck.iter().filter(|c| c.suit == suit).count();
        let follow = usize::min(self.num_players_have(suit) * suit_length, deck_length);
        suit_length * 3 - follow
    }

    /// Returns a list of all cards in the given suit which are lower then the corresponding card of that suit in the deck
    fn list_safe(&self, suit: Suit) -> Vec<Card> {
        let deck_list = self
            .deck
            .iter()
            .filter(|c| c.suit == suit)
            .sorted_by_key(|c| c.rank)
            .collect::<Vec<_>>();
        let hand_list = self
            .hand
            .iter()
            .filter(|c| c.suit == suit)
            .sorted_by_key(|c| c.rank)
            .collect::<Vec<_>>();
        hand_list
            .iter()
            .zip(deck_list.iter())
            .filter(|(hand_card, deck_card)| hand_card.rank < deck_card.rank)
            .map(|(hand_card, _deck_card)| *hand_card)
            .cloned()
            .collect()
    }

    // Returns list of cards which only one outstanding card can duck
    fn list_1below(&self, cards: &HashSet<Card>) -> Vec<Card> {
        cards
            .iter()
            .filter(|card| self.num_below(**card) == 1 && self.num_above(**card) > 0)
            .cloned()
            .collect()
    }

    /// Return true if more than half of the total points are still in the deck.
    fn deck_is_risky(&self) -> bool {
        let risk = if self.rules.ruleset == Ruleset::SpotHearts {
            65
        } else {
            13
        };
        self.deck
            .iter()
            .map(|c| self.rules.ruleset.card_cost(c))
            .sum::<i32>()
            >= risk
    }

    /// Returns true if card will win trick
    fn is_winner(&self, card: Card) -> bool {
        !self
            .deck
            .iter()
            .filter(|c| c.suit == card.suit)
            .any(|c| c.rank > card.rank)
    }

    /// Returns a list of all cards in the given suit which are lower then the lowest card of that suit in the deck
    fn list_exit(&self, suit: Suit) -> Vec<&Card> {
        if let Some(min_in_deck) = self
            .deck
            .iter()
            .filter(|c| c.suit == suit)
            .min_by_key(|c| c.rank)
        {
            self.hand
                .iter()
                .filter(|c| c.suit == suit && c.rank < min_in_deck.rank)
                .collect()
        } else {
            Vec::new()
        }
    }

    /// Return list of cards in the hand which are higher than two-thirds of the cards of the given suit in the deck
    fn list_high(&self, suit: Suit) -> Vec<&Card> {
        let hand_list: Vec<_> = self.hand.iter().filter(|c| c.suit == suit).collect();
        if hand_list.is_empty() {
            return vec![];
        }
        let deck_list = self
            .deck
            .iter()
            .filter(|c| c.suit == suit)
            .sorted_by_key(|c| -(c.rank as i32))
            .collect::<Vec<_>>();
        if deck_list.is_empty() {
            return hand_list;
        }
        let index = if deck_list.len() > 2 {
            deck_list.len() / 3
        } else {
            deck_list.len() - 1
        };
        let limit = deck_list[index].rank;
        hand_list.into_iter().filter(|c| c.rank > limit).collect()
    }

    // Return list of cards in the hand which are lower than two-thirds of the cards of the given suit in the deck
    fn list_low(&self, suit: Suit) -> Vec<&Card> {
        let hand_list: Vec<_> = self.hand.iter().filter(|c| c.suit == suit).collect();
        if hand_list.is_empty() {
            return vec![];
        }
        let deck_list = self
            .deck
            .iter()
            .filter(|c| c.suit == suit)
            .sorted_by_key(|c| c.rank)
            .collect::<Vec<_>>();
        if deck_list.is_empty() {
            return vec![];
        }
        let index = if deck_list.len() > 2 {
            deck_list.len() / 3
        } else {
            deck_list.len() - 1
        };
        let limit = deck_list[index].rank;
        hand_list.into_iter().filter(|c| c.rank < limit).collect()
    }

    /// If only one player has taken points,
    /// and that player is not ouself, return
    /// a one element list with the player
    /// number, and the number of points taken.
    fn sole_shooter(&self) -> Option<(Side, i32)> {
        let mut score_list = Vec::new();
        for index in &Side::ALL {
            let score = self.score.round.get(*index);
            if score != 0 {
                score_list.push((*index, score));
            }
        }
        if score_list.len() == 1 && score_list[0].0 != self.direction {
            Some(score_list[0])
        } else {
            None
        }
    }

    /// Return true if sole shooter is the trick leader,
    /// or has not played and and there are outstanding cards
    /// above the current highest card.
    fn can_shooter_win(&self, trick: &Trick) -> bool {
        if let Some((shooter_place, _shooter_score)) = self.sole_shooter() {
            if Some(shooter_place) == trick.get_winner_side() {
                return true;
            }
            if trick.has_played(shooter_place) {
                return false;
            }
            if self.num_above(*trick.get_winner_card().unwrap()) > 0 {
                return true;
            }
        }
        false
    }

    /// Returns true if in 4th position,
    /// or all remaining players have are known
    /// to be void in the suit led.
    fn last_to_play(&self, trick: &Trick) -> bool {
        if trick.num_played() == 3 {
            return true;
        }
        let yet_list: HashSet<_> = trick.remaining_players().into_iter().collect();
        let play_list = self.players_void(trick.trump().unwrap());
        yet_list.is_subset(&play_list)
    }

    /// Returns a score indicating the risk that
    /// the highest card in the given suit will
    /// be forced to take an unwanted trick.
    fn suit_score(&self, suit: Suit) -> i32 {
        if self.hand.iter().any(|c| c.suit == suit) && self.deck.iter().any(|c| c.suit == suit) {
            let suit_list = self
                .hand
                .iter()
                .filter(|c| c.suit == suit)
                .sorted_by_key(|c| -(c.rank as i32))
                .collect::<Vec<_>>();
            (self.num_below(*suit_list[0]) - 2 * suit_list.len() + 1) as i32
        } else {
            0
        }
    }

    /// Returns a non-zero indicating the reason why it is
    /// reasonable to try to shoot, else returns zero
    /// Have already taken half of the total points (2)
    /// Have the unprotected Queen and no short minor suits (3)
    /// If in the lead, and have an exit problem (7)
    /// Have a hand with all of the following: (4,5,6)
    ///     Duckable hearts, or none at all
    ///     At least one runnable suit
    ///     If not in the lead, entries in non-void minor suits
    fn try_to_shoot(&self, trick: &Trick) -> Option<ReasonToShoot> {
        let clubs = self.hand.iter().filter(|c| c.suit == Suit::CLUBS).count();
        let diamonds = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::DIAMONDS)
            .count();
        let spades = self.hand.iter().filter(|c| c.suit == Suit::SPADES).count();
        let hearts = self.hand.iter().filter(|c| c.suit == Suit::HEARTS).count();

        if !self.is_runnable(Suit::CLUBS)
            && !self.is_runnable(Suit::DIAMONDS)
            && !self.is_runnable(Suit::HEARTS)
            && !self.is_runnable(Suit::SPADES)
        {
            return None;
        }
        if self.rules.ruleset != Ruleset::SpotHearts && self.score.round.get(self.direction) >= 13 {
            return Some(ReasonToShoot::TakenHalf);
        }
        if self.rules.ruleset == Ruleset::SpotHearts && self.score.round.get(self.direction) >= 64 {
            return Some(ReasonToShoot::TakenHalf);
        }
        if self.hand.contains(&Card::QUEEN_OF_SPADES) && spades < 4 && clubs > 2 && diamonds > 2 {
            return Some(ReasonToShoot::UnprotectedQueen);
        }
        if trick.num_played() == 0 && self.exit_problem() {
            return Some(ReasonToShoot::ExitProblem);
        }
        if hearts > 0 {
            if !self.is_duckable(Suit::HEARTS) && !self.is_runnable(Suit::HEARTS) {
                return None;
            }
        }
        if trick.num_played() > 0 {
            if clubs > 0 && !self.is_entry_suit(Suit::CLUBS) {
                return None;
            }
            if diamonds > 0 && !self.is_entry_suit(Suit::DIAMONDS) {
                return None;
            }
        }
        return Some(ReasonToShoot::DuckableHearts);
    }

    /// Returns true if the hand contains
    /// Above average high card points
    /// Entries in all non-void suits
    fn pass_to_shoot(&self) -> bool {
        let points = self
            .hand
            .iter()
            .map(|card| match card.rank {
                Rank::ACE => 4,
                Rank::KING => 3,
                Rank::QUEEN => 2,
                Rank::JACK => 1,
                _ => 0,
            })
            .sum::<i32>();

        points >= 13
            && self.is_entry_suit(Suit::CLUBS)
            && self.is_entry_suit(Suit::DIAMONDS)
            && self.is_entry_suit(Suit::HEARTS)
            && self.is_entry_suit(Suit::SPADES)
    }

    fn open_trick(&self, valid_cards: &Cards) -> Card {
        // Special rule for breaking hearts by leading hearts
        // If I have only hearts, and it is still possible to shoot,
        // and I have the top heart, then play it. Otherwise, play
        // the lowest heart.
        if self.hand.iter().all(|c| c.suit == Suit::HEARTS) {
            let card = *self.hand.iter().max_by_key(|c| c.rank).unwrap();
            if self.is_winner(card) && self.can_shoot && self.is_runnable(Suit::HEARTS) {
                return card;
            }
            return *self.hand.iter().min_by_key(|c| c.rank).unwrap();
        }

        // Special rule for leading the queen of spades
        if self.hand.contains(&Card::QUEEN_OF_SPADES) {
            let spades = self.deck.iter().filter(|c| c.suit == Suit::SPADES).count();
            if spades > 0 && spades == self.num_above(Card::QUEEN_OF_SPADES) {
                return Card::QUEEN_OF_SPADES;
            }
        }

        // Special rule for leading the bonus card
        if let Some(bonus_card) = self.hand.iter().find(|c| self.is_bonus_card(**c)) {
            if self.num_above(*bonus_card) == 0 {
                return *bonus_card;
            }
        }

        // If queen of spades is still in the deck, try to flush it
        if self.hand.iter().any(|c| c.suit == Suit::SPADES)
            && self.deck.contains(&Card::QUEEN_OF_SPADES)
        {
            let flush_list: Vec<_> = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::SPADES && c.rank < Rank::QUEEN)
                .collect();
            if let Some(flush) = flush_list.iter().max_by_key(|c| c.rank) {
                let high_list: Vec<_> = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES && c.rank > Rank::QUEEN)
                    .collect();
                let player_num = self.num_players_have(Suit::SPADES);
                let out_num = self.deck.iter().filter(|c| c.suit == Suit::SPADES).count();
                if high_list.is_empty()
                    || out_num == 1
                    || (flush_list.len() >= (out_num / player_num) && flush_list.len() > 1)
                {
                    return **flush;
                }
            }
            // look for safe lead in a minor suit in which the holder of the
            // queen might be void.
        }

        // Establish safe spades
        if self.queen_played {
            if self.hand.iter().any(|c| c.suit == Suit::SPADES)
                && self.deck.iter().any(|c| c.suit == Suit::SPADES)
            {
                if let Some(high_spade) = self.list_high(Suit::SPADES).iter().max_by_key(|c| c.rank)
                {
                    return **high_spade;
                }
            }
        }

        // Establish safe minor suits
        {
            let club_score = self.suit_score(Suit::CLUBS);
            let diamond_score = self.suit_score(Suit::DIAMONDS);

            let risky_suit = if club_score > 0 && club_score > diamond_score {
                Some(Suit::CLUBS)
            } else if diamond_score > 0 {
                Some(Suit::DIAMONDS)
            } else {
                None
            };

            if let Some(risky_suit) = risky_suit {
                // If the deck is not risky, and I do not have an exit problem, lead most unprotected minor card
                if !self.deck_is_risky()
                    || (!self.spade_problem()
                        && self.num_out(risky_suit) > 4
                        && self.num_players_have(risky_suit) == 3)
                {
                    return *self.hand.iter().max_by_key(|c| c.rank).unwrap();
                }
            }
        }

        // Spread some hearts around
        if self.hearts_played {
            if let Some(hand_low_heart) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .min_by_key(|c| c.rank)
            {
                if let Some(deck_low_heart) = self
                    .deck
                    .iter()
                    .filter(|c| c.suit == Suit::HEARTS)
                    .min_by_key(|c| c.rank)
                {
                    if hand_low_heart.rank < deck_low_heart.rank {
                        return *hand_low_heart;
                    }
                }
            }
        }

        // Get out of lead without too much risk
        // See if there are any exit cards
        let mut exit_suits = vec![];
        for suit in &Suit::ALL {
            if self.list_exit(*suit).len() > 0 {
                if *suit == Suit::HEARTS && !self.hearts_played {
                    continue;
                }
                if *suit == Suit::SPADES && !self.safe_spade_lead() {
                    continue;
                }
                exit_suits.push(*suit);
            }
        }

        // See if there are any "one below" cards
        let mut below_suits = vec![];
        let below_list = self.list_1below(&self.hand);
        for suit in &Suit::ALL {
            if below_list.iter().any(|c| c.suit == *suit) {
                if *suit == Suit::HEARTS && !self.hearts_played {
                    continue;
                }
                if *suit == Suit::SPADES && !self.safe_spade_lead() {
                    continue;
                }
                below_suits.push(*suit);
            }
        }

        // See if there are any safe cards
        let mut safe_suits = vec![];
        for suit in &Suit::ALL {
            if self.list_safe(*suit).len() > 0 {
                if *suit == Suit::HEARTS && !self.hearts_played {
                    continue;
                }
                if *suit == Suit::SPADES && !self.safe_spade_lead() {
                    continue;
                }
                safe_suits.push(*suit);
            }
        }

        if self.deck.contains(&Card::QUEEN_OF_SPADES) {
            // Play an exit card
            if let Some(suit) = exit_suits
                .iter()
                .filter(|suit| **suit != Suit::SPADES || self.safe_spade_lead())
                .collect::<Vec<_>>()
                .choose(&mut thread_rng())
            {
                return *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == **suit)
                    .min_by_key(|c| c.rank)
                    .unwrap();
            }
            // Play a below card
            if let Some(suit) = below_suits
                .iter()
                .filter(|suit| **suit != Suit::SPADES || self.safe_spade_lead())
                .collect::<Vec<_>>()
                .choose(&mut thread_rng())
            {
                return *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == **suit)
                    .min_by_key(|c| c.rank)
                    .unwrap();
            }
            // Play a safe card
            if let Some(suit) = safe_suits
                .iter()
                .filter(|suit| **suit != Suit::SPADES || self.safe_spade_lead())
                .collect::<Vec<_>>()
                .choose(&mut thread_rng())
            {
                return *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == **suit)
                    .min_by_key(|c| c.rank)
                    .unwrap();
            }

            // Play the card which can be beat by the most cards in the deck
            if let Some(best_card) = self
                .hand
                .iter()
                .filter(|c| c.suit != Suit::HEARTS || self.hearts_played)
                .filter(|c| c.suit != Suit::SPADES || self.safe_spade_lead())
                .max_by_key(|c| self.num_above(**c))
            {
                return *best_card;
            }
        }

        // The queen is not in the deck, so lead a relatively high card
        // Play the highest card which can be beat by at least
        // four cards in the deck
        if let Some(best_card) = Cards::from_set(&self.hand)
            .map(|cards| cards.try_exclude(|c| self.is_bonus_card(c)))
            .and_then(|cards| cards.filter(|c| c.suit != Suit::HEARTS || self.hearts_played))
            .and_then(|cards| cards.filter(|c| !self.list_low(c.suit).contains(&&c)))
            .and_then(|cards| cards.filter(|c| self.num_above(c) > 3))
            .map(|cards| cards.highest())
        {
            return best_card;
        }

        // Play a card which can be beat
        if let Some(deck_high_diamond) = self
            .deck
            .iter()
            .filter(|c| c.suit == Suit::DIAMONDS)
            .max_by_key(|c| c.rank)
        {
            if let Some(low_diamond) = Cards::from_set(&self.hand)
                .and_then(|cards| cards.of_suit(Suit::DIAMONDS))
                .map(|cards| cards.try_exclude(|c| self.is_bonus_card(c)))
                .map(|cards| cards.lowest())
            {
                if low_diamond.rank < deck_high_diamond.rank {
                    return low_diamond;
                }
            }
        }
        if let Some(out_club) = self
            .deck
            .iter()
            .filter(|c| c.suit == Suit::CLUBS)
            .max_by_key(|c| c.rank)
        {
            if let Some(low_club) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::CLUBS)
                .min_by_key(|c| c.rank)
            {
                if low_club.rank < out_club.rank {
                    return *low_club;
                }
            }
        }
        if self.hearts_played {
            if let Some(out_heart) = self
                .deck
                .iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .max_by_key(|c| c.rank)
            {
                if let Some(low_heart) = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::HEARTS)
                    .min_by_key(|c| c.rank)
                {
                    if low_heart.rank < out_heart.rank {
                        return *low_heart;
                    }
                }
            }
        }
        if let Some(deck_high_spade) = self
            .deck
            .iter()
            .filter(|c| c.suit == Suit::SPADES)
            .max_by_key(|c| c.rank)
        {
            if let Some(low_spade) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::SPADES)
                .min_by_key(|c| c.rank)
            {
                if low_spade.rank < deck_high_spade.rank
                    && *low_spade != Card::QUEEN_OF_SPADES
                    && *low_spade != Card::KING_OF_SPADES
                {
                    return *low_spade;
                }
            }
        }
        if self.hand.iter().any(|c| c.suit == Suit::HEARTS) {
            if let Some(minor) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::CLUBS || c.suit == Suit::DIAMONDS)
                .min_by_key(|c| c.rank)
            {
                return *minor;
            }
            if self.hand.contains(&Card::QUEEN_OF_SPADES) {
                if let Some(spade) = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES && c.rank != Rank::QUEEN)
                    .min_by_key(|c| c.rank)
                {
                    return *spade;
                }
            }
        }

        // Abandon hope
        valid_cards.random()
    }

    /// Play a card following the trick's trump
    fn follow_suit(&self, valid_cards: &Cards, trick: &Trick) -> Card {
        // If we get this far, then hand will have at least two entries of trump suit

        // If I must win this trick, do so with highest card,
        // with the following exceptions:
        // Queen of Spades - play next lowest (all rulesets)
        // Take with bonus card (Omnibus rulesets)
        // Take with lowest heart (Spot ruleset)
        {
            let hand_low = self
                .hand
                .iter()
                .filter(|c| c.suit == trick.trump().unwrap())
                .min_by_key(|c| c.rank)
                .unwrap();
            let deck_high = self
                .deck
                .iter()
                .filter(|c| c.suit == trick.trump().unwrap())
                .max_by_key(|c| c.rank)
                .unwrap();
            let highest_card = trick.get_winner_card().unwrap();

            if hand_low.rank > deck_high.rank
                || (self.last_to_play(trick) && hand_low.rank > highest_card.rank)
            {
                let suit_list = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == trick.trump().unwrap() && **c != Card::QUEEN_OF_SPADES)
                    .sorted_by_key(|c| c.rank)
                    .collect::<Vec<_>>();
                if let Some(bonus_card) = suit_list.iter().find(|c| self.is_bonus_card(***c)) {
                    return **bonus_card;
                }
                if self.rules.ruleset == Ruleset::SpotHearts
                    && trick.trump().unwrap() == Suit::HEARTS
                {
                    return *suit_list[0];
                }
                return **suit_list.last().unwrap();
            }
        }

        // Play spades
        if trick.trump() == Some(Suit::SPADES) {
            // I have the queen
            if self.hand.contains(&Card::QUEEN_OF_SPADES) {
                // See if I can dump the queen
                if trick.contains(&Card::KING_OF_SPADES) || trick.contains(&Card::ACE_OF_SPADES) {
                    return Card::QUEEN_OF_SPADES;
                }
                // Try playing something else but the queen
                let spades_list = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES)
                    .sorted_by_key(|c| c.rank)
                    .collect::<Vec<_>>();
                if *spades_list[0] == Card::QUEEN_OF_SPADES {
                    // return the highest card.
                    // Could be the queen if that's all we have.
                    return **spades_list.last().unwrap();
                }
                return **spades_list
                    .iter()
                    .filter(|c| c.rank < Rank::QUEEN)
                    .max_by_key(|c| c.rank)
                    .unwrap();
            }
            // Queen has been played
            if !self.deck.contains(&Card::QUEEN_OF_SPADES) {
                return *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES)
                    .max_by_key(|c| c.rank)
                    .unwrap();
            }
            // I don't have the queen
            if self.hand.contains(&Card::KING_OF_SPADES) || self.hand.contains(&Card::ACE_OF_SPADES)
            {
                if trick.num_played() == 3 && !trick.contains(&Card::QUEEN_OF_SPADES) {
                    return *self
                        .hand
                        .iter()
                        .filter(|c| c.suit == Suit::SPADES)
                        .max_by_key(|c| c.rank)
                        .unwrap();
                }
                if self.already_played(trick) {
                    return *self
                        .hand
                        .iter()
                        .filter(|c| c.suit == Suit::SPADES)
                        .max_by_key(|c| c.rank)
                        .unwrap();
                }
                let spades_list = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES)
                    .sorted_by_key(|c| c.rank)
                    .collect::<Vec<_>>();
                if spades_list[0].rank >= Rank::KING {
                    // Forced to play high spades
                    return **spades_list.last().unwrap();
                }
                return *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES && c.rank < Rank::KING)
                    .min_by_key(|c| c.rank)
                    .unwrap();
            }
            return *self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::SPADES)
                .max_by_key(|c| c.rank)
                .unwrap();
        }

        // Play hearts
        if trick.trump() == Some(Suit::HEARTS) {
            let hearts_list = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .sorted_by_key(|c| c.rank)
                .collect::<Vec<_>>();
            let highest_card = trick.get_winner_card().unwrap();
            if hearts_list[0].rank > highest_card.rank {
                if trick.num_played() == 3 || self.num_above(*hearts_list[0]) == 0 {
                    // I'm going to get it anyway, so play the highest
                    return **hearts_list.last().unwrap();
                }
                return *hearts_list[0];
            }
            // If someone is trying to shoot, put a spoke in their wheels
            if let Some((shooter_side, shooter_points)) = self.sole_shooter() {
                if trick.get_winner_side().unwrap() == shooter_side {
                    if self.rules.ruleset == Ruleset::SpotHearts && shooter_points > 64
                        || self.rules.ruleset != Ruleset::SpotHearts && shooter_points > 13
                    {
                        if hearts_list.last().unwrap().rank > highest_card.rank {
                            return **hearts_list.last().unwrap();
                        }
                        return *hearts_list[hearts_list.len() - 2];
                    }
                }
            }
            // Play highest heart that doesn't win
            if let Some(card) = self.duck_high(*highest_card) {
                return card;
            }
            // Play lowest card
            return *hearts_list[0];
        }

        // Play clubs or diamonds

        // Special rule for 1st trick - always safe to take
        if self.trick_num == 1 && self.rules.no_blood {
            if let Some(card) = self
                .hand
                .iter()
                .filter(|c| c.suit == trick.trump().unwrap())
                .max_by_key(|c| c.rank)
            {
                return *card;
            }
        }

        // Special rule for Omnibus ruleset
        if let Some(bonus_card) = self
            .hand
            .iter()
            .find(|c| c.suit == trick.trump().unwrap() && self.is_bonus_card(**c))
        {
            if self.num_above(*bonus_card) == 0 {
                return *bonus_card;
            }
        }

        let cards_of_suit = {
            let suit_list: Vec<_> = self
                .hand
                .iter()
                .filter(|c| c.suit == trick.trump().unwrap())
                .cloned()
                .collect();
            Cards::from_slice(&suit_list).unwrap()
        };

        let suit_list = self
            .hand
            .iter()
            .filter(|c| c.suit == trick.trump().unwrap())
            .sorted_by_key(|c| c.rank)
            .collect::<Vec<_>>();
        let highest_card = trick.get_winner_card().unwrap();

        // If I can duck this suit forever, do so
        let trick_list = trick
            .iter_cards()
            .filter(|c| c.suit == trick.trump().unwrap())
            .count();
        let deck_list = self
            .deck
            .iter()
            .filter(|c| c.suit == trick.trump().unwrap())
            .count();
        if trick_list == deck_list
            || self.list_safe(trick.trump().unwrap()).len() == suit_list.len()
        {
            if let Some(low) = suit_list
                .iter()
                .filter(|c| c.rank < highest_card.rank)
                .max_by_key(|c| c.rank)
            {
                return **low;
            }
        }

        // If I'm the last in the trick, take with high card,
        // or with bonus card if playing Omnibus ruleset
        if trick.num_played() == 3 && trick.get_cost(self.rules.ruleset) <= 10 {
            if let Some(bonus_card) = suit_list.iter().find(|c| self.is_bonus_card(***c)) {
                if bonus_card.rank > highest_card.rank {
                    return **bonus_card;
                }
                return cards_of_suit
                    .try_exclude(|c| self.is_bonus_card(c))
                    .highest();
            } else {
                return cards_of_suit.highest();
            }
        }

        // Play high card if queen of spades not in deck
        // But don't play bonus card if Omnibus ruleset,
        // unless it will win the trick
        if self.num_out(trick.trump().unwrap()) > 4
            && (!self.deck.contains(&Card::QUEEN_OF_SPADES) || self.already_played(trick))
        {
            if let Some(bonus_card) = suit_list.iter().find(|c| self.is_bonus_card(***c)) {
                if self.num_above(**bonus_card) == 0 {
                    return **bonus_card;
                }
                return cards_of_suit
                    .try_exclude(|c| self.is_bonus_card(c))
                    .highest();
            } else {
                return cards_of_suit.highest();
            }
        }

        // Play the highest card that doesn't win the trick
        if let Some(card) = self.duck_high(*highest_card) {
            return card;
        }

        // Play lowest card
        cards_of_suit
            .try_exclude(|c| self.is_bonus_card(c))
            .lowest()
    }

    /// Play a card when I have no trump cards
    fn dont_follow_suit(&self, valid_cards: &Cards, trick: &Trick) -> Card {
        // If the queen is in my hand, and not well
        // protected, dump it now
        if valid_cards.contains(Card::QUEEN_OF_SPADES)
            && (self.spade_problem() || self.exit_problem())
        {
            return Card::QUEEN_OF_SPADES;
        }
        // If the queen is in the deck, give priority
        // to unprotected high cards
        // Else favor discarding hearts
        let mut best_score = 0;
        let mut best_card = None;
        for suit in &Suit::ALL {
            if let Some(suit_cards) = valid_cards.of_suit(*suit) {
                // Favor Queen over any other spades
                let card = if suit_cards.contains(Card::QUEEN_OF_SPADES) {
                    Card::QUEEN_OF_SPADES
                } else {
                    suit_cards.highest()
                };
                // If Omnibus ruleset, don't discard bonus_card or any of its protectors
                if self.is_bonus_card(card) {
                    continue;
                }
                let mut score = self.num_below(card) as i32;
                let above = self.num_above(card);
                // Prefer high spades if queen still in deck
                // unless well protected
                if *suit == Suit::SPADES
                    && self.deck.contains(&Card::QUEEN_OF_SPADES)
                    && card.rank > Rank::QUEEN
                    && suit_cards.len() - num_top_two_spades(&self.hand) < 4
                {
                    score += 5;
                }
                // Don't discard cards which can not take a trick
                if score == 0 {
                    continue;
                }
                // Favor entries and short suits
                if above == 0 {
                    score += 5;
                }
                if suit_cards.len() == 1 {
                    score += 5;
                }
                // Greatly prefer high hearts in Spot Hearts ruleset
                if self.rules.ruleset == Ruleset::SpotHearts && *suit == Suit::HEARTS {
                    score += card.rank as i32;
                }
                // Prefer hearts if Queen not in deck
                if *suit == Suit::HEARTS && !self.deck.contains(&Card::QUEEN_OF_SPADES) {
                    score += 1;
                    if self.heart_problem() || !self.hearts_played {
                        score += 4;
                    }
                    // Greatly prefer stopping a shoot
                    if self.sole_shooter().is_some() {
                        if !self.can_shooter_win(trick) {
                            score += 10;
                        } else {
                            score -= 5;
                        }
                    }
                }
                // Prefer cards received in the pass
                if self.received_cards.contains(&card) {
                    score += 1;
                }
                // Update best values
                if score > best_score {
                    best_score = score;
                    best_card = Some(card);
                }
            }
        }
        // Discard most dangerous card
        if let Some(best_card) = best_card {
            return self.list_peers(best_card).random();
        }

        // Even if the Queen is well protected, now is a good time to play it
        if valid_cards.contains(Card::QUEEN_OF_SPADES) {
            return Card::QUEEN_OF_SPADES;
        }

        // As a last resort, play the highest valid card, but not the bonus card
        valid_cards.try_exclude(|c| self.is_bonus_card(c)).highest()
    }

    /// Spades are not a problem if:
    /// The queen has been played
    /// I have no spades
    /// I have none of the top three spades
    /// I have more spades than there are low spades outstanding minus the number of other players who have not show out of spades
    fn spade_problem(&self) -> bool {
        if self.queen_played {
            return false;
        }
        let spades = self.hand.iter().filter(|c| c.suit == Suit::SPADES).count();
        if spades == 0 {
            return false;
        }
        if num_top_three_spades(&self.hand) == 0 {
            return false;
        }
        if spades > (self.num_below(Card::QUEEN_OF_SPADES) - self.num_players_have(Suit::SPADES)) {
            return false;
        }
        return true;
    }

    /// It is relatively safe to lead a low spade if:
    /// The queen is not in the deck and is not my lowest spade,
    /// or I have neither of the top two spades,
    /// or I have at least two low spades as protection.
    fn safe_spade_lead(&self) -> bool {
        let spades_list = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::SPADES)
            .sorted_by_key(|c| c.rank)
            .collect::<Vec<_>>();
        if spades_list.is_empty() {
            return false;
        }
        if spades_list[0].rank == Rank::QUEEN {
            return false;
        }
        if !self.deck.contains(&Card::QUEEN_OF_SPADES) {
            return true;
        }
        if spades_list.iter().any(|c| c.rank > Rank::QUEEN) {
            if self.num_out(Suit::SPADES) == 1 {
                return true;
            }
            let top3list = spades_list.iter().filter(|c| c.rank > Rank::JACK).count();
            spades_list.len() > top3list + 1
        } else {
            true
        }
    }

    /// Hearts are not a problem if
    /// I have no hearts
    /// I have more low hearts than high hearts and at least one lowest heart
    fn heart_problem(&self) -> bool {
        let hearts_list: Vec<_> = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::HEARTS)
            .collect();
        if hearts_list.is_empty() {
            return false;
        }
        let low_list = hearts_list
            .iter()
            .filter(|c| c.rank < Rank::SIX)
            .sorted_by_key(|c| c.rank)
            .collect::<Vec<_>>();
        let high_count = hearts_list.iter().filter(|c| c.rank > Rank::TEN).count();
        if high_count > low_list.len() {
            return true;
        }
        if let Some(out_heart) = self
            .deck
            .iter()
            .filter(|c| c.suit == Suit::HEARTS)
            .min_by_key(|c| c.rank)
        {
            low_list.len() > 0 && low_list[0].rank > out_heart.rank
        } else {
            false
        }
    }

    /// Returns true if there are no exits and no low cards
    fn exit_problem(&self) -> bool {
        if self.list_low(Suit::CLUBS).len() > 0
            || self.list_low(Suit::DIAMONDS).len() > 0
            || self.list_low(Suit::HEARTS).len() > 0
            || self.list_low(Suit::SPADES).len() > 0
        {
            return false;
        }

        for card in &self.hand {
            if self.num_above(*card) > 0 && self.num_below(*card) == 0 {
                return false;
            }
        }
        true
    }

    /// Return true if hand with Queen has already played
    fn already_played(&self, trick: &Trick) -> bool {
        if let Some(pass_player) = self.pass_dir {
            self.pass_cards.contains(Card::QUEEN_OF_SPADES)
                && trick.has_played(pass_player)
                && !trick.contains(&Card::QUEEN_OF_SPADES)
        } else {
            false
        }
    }

    // Play card routines for trying to shoot
    fn open_trick_shoot(&self, valid_cards: &Cards) -> Card {
        let mut runnable_suits = vec![];
        let mut win_suits = vec![];
        let mut entry_suits = vec![];
        let mut noentry_suits = vec![];
        let mut void_suits = vec![];
        let mut low_suit = None;
        let mut low_length = 0;
        let mut high_suit = None;
        let mut high_length = 0;
        let mut discards = 0;
        let mut high_out = 0;
        let mut points = self.deck.iter().filter(|c| is_point_card(**c)).count();

        // If hearts are ready to run, do so
        if let Some(hearts) = valid_cards.of_suit(Suit::HEARTS) {
            if self.is_runnable(Suit::HEARTS) {
                if let Some(return_card) = self.low_win(Suit::HEARTS) {
                    return return_card;
                }
            }
            if self.is_duckable(Suit::HEARTS) {
                return hearts.highest();
            }
        }
        // Categorize suits as runnable, win, noentry, or void
        for suit in &[Suit::CLUBS, Suit::DIAMONDS, Suit::SPADES] {
            if let Some(suit_cards) = valid_cards.of_suit(*suit) {
                let length = suit_cards.len();
                let highest = suit_cards.highest();

                if self.is_runnable(*suit) {
                    runnable_suits.push((suit, length));
                    discards += self.num_discards(*suit);
                } else if self.is_winner(highest) {
                    win_suits.push((suit, length));
                    high_out += self.num_above(highest);
                } else if self.is_entry_suit(*suit) {
                    entry_suits.push((suit, length));
                    high_out += self.num_above(highest);
                } else {
                    noentry_suits.push((suit, length));
                }
                if length > low_length {
                    low_suit = Some(*suit);
                    low_length = length;
                }
                if self.num_above(highest) == 0 {
                    if length > high_length {
                        high_suit = Some(suit);
                        high_length = length;
                    }
                } else {
                    points += self.num_above(highest);
                }
            } else {
                void_suits.push((suit, 0));
            }
        }

        // If we can generate enought discards, play high from runnable suit
        if let Some((runnable_suit, _length)) =
            runnable_suits.iter().max_by_key(|(_, length)| length)
        {
            if discards >= points {
                // Run longest suit first, and minors before majors
                if let Some(return_card) = self.low_win(**runnable_suit) {
                    return return_card;
                }
            }
        }

        // Play low card from suit with entry, but no winner
        // Try to set up our longest suit first
        for (suit, _) in entry_suits
            .iter()
            .sorted_by_key(|(_, length)| usize::max_value() - length)
        {
            if self.num_players_have(**suit) != 3 {
                continue;
            }
            if self.deck.iter().filter(|c| c.suit == **suit).count() <= 3 {
                continue;
            }
            let card = valid_cards.of_suit(**suit).unwrap().lowest();
            if card != Card::QUEEN_OF_SPADES {
                return card;
            }
        }

        // Play low card from suit with no entry
        // Try to void our shortest suit first
        for (suit, _) in noentry_suits.iter().sorted_by_key(|(_, length)| length) {
            if self.num_players_have(**suit) != 3 {
                continue;
            }
            if self.deck.iter().filter(|c| c.suit == **suit).count() <= 3 {
                continue;
            }
            let card = valid_cards.of_suit(**suit).unwrap().lowest();
            if card != Card::QUEEN_OF_SPADES {
                return card;
            }
        }

        // Play high from runnable suit regardless of number of discards
        // Run longest suit first
        for (suit, _) in runnable_suits
            .iter()
            .sorted_by_key(|(_, length)| usize::max_value() - length)
        {
            if **suit == Suit::SPADES && runnable_suits.len() > 1 {
                continue;
            } // ???
            if let Some(return_card) = self.low_win(**suit) {
                return return_card;
            }
        }

        // Play winner from longest valid suit which has one
        if let Some(high_suit) = high_suit {
            if let Some(high_suit_cards) = valid_cards.of_suit(*high_suit) {
                return self.list_peers(high_suit_cards.highest()).random();
            }
        }

        if self.hearts_played {
            // Play winning heart, if possible
            if let Some(card) = valid_cards
                .of_suit(Suit::HEARTS)
                .map(|cards| cards.highest())
            {
                if self.num_above(card) == 0 {
                    if let Some(win_card) = self.low_win(Suit::HEARTS) {
                        return win_card;
                    }
                }
            }
        } else {
            // Play low card from longest suit
            if let Some(low_suit) = low_suit {
                if self.num_players_have(low_suit) == 3 {
                    if let Some(low_suit_cards) = valid_cards.of_suit(low_suit) {
                        return low_suit_cards.lowest();
                    }
                }
            }
        }

        // Play lowest valid card
        valid_cards.lowest()
    }

    fn follow_suit_shoot(
        &self,
        valid_cards: &Cards,
        trick: &Trick,
        shoot_code: ReasonToShoot,
    ) -> Card {
        // See if I can dump the queen
        if shoot_code == ReasonToShoot::ExitProblem
            && self.hand.contains(&Card::QUEEN_OF_SPADES)
            && (trick.contains(&Card::KING_OF_SPADES) || trick.contains(&Card::ACE_OF_SPADES))
        {
            return Card::QUEEN_OF_SPADES;
        }
        // Special case for 1st trick, always safe to duck trick
        if self.trick_num == 1 && self.rules.no_blood {
            return valid_cards.lowest();
        }

        // Check to see if any point cards have already fallen
        let must_take = trick.num_point_cards() > 0;

        // Check to see if we are ready to run suits
        let try_take = self.hearts_played && self.is_runnable(Suit::HEARTS) || {
            let mut num_runnable = 0;
            let mut discards = 0;
            for suit in &[Suit::CLUBS, Suit::DIAMONDS, Suit::SPADES] {
                if valid_cards.have_suit(*suit) && self.is_runnable(*suit) {
                    num_runnable += 1;
                    discards += self.num_discards(*suit);
                }
            }

            let points = self.deck.iter().filter(|c| is_point_card(**c)).count();
            discards >= points && num_runnable > 0
        };

        // We want this trick
        if must_take || try_take {
            let high_card = trick.get_winner_card().unwrap();

            // If in favorable position, play lowest card which will take trick
            if self.last_to_play(trick) || (!must_take && trick.num_played() == 2) {
                let has_high_spades = valid_cards.contains(Card::KING_OF_SPADES)
                    || valid_cards.contains(Card::ACE_OF_SPADES);
                if let Some(cards) = valid_cards.filter(|c| {
                    c.rank < high_card.rank && (c != Card::QUEEN_OF_SPADES || !has_high_spades)
                }) {
                    return cards.lowest();
                }
            }
            // Try to win the trick from unfavorable position
            if let Some(play_card) = self.low_win(high_card.suit) {
                return play_card;
            }

            if !must_take {
                // Try to take trick with highest card
                if let Some(highest) = valid_cards.filter(|c| c.rank > high_card.rank) {
                    return highest.highest();
                }
            } else if valid_cards.contains(Card::QUEEN_OF_SPADES) {
                // Can't win, so abandon shoot.
                return Card::QUEEN_OF_SPADES;
            } else {
                // Play highest card
                return valid_cards.highest();
            }
        }

        // We don't want this trick
        valid_cards.try_exclude_card(Card::QUEEN_OF_SPADES).lowest()
    }

    fn dont_follow_suit_shoot(&self, valid_cards: &Cards, _trick: &Trick) -> Card {
        // Play lowest non-point card in shortest suit
        if let Some(low_suit) = [Suit::CLUBS, Suit::DIAMONDS, Suit::SPADES]
            .iter()
            .filter_map(|suit| valid_cards.of_suit(*suit))
            .min_by_key(|cards| cards.len())
        {
            low_suit.highest()
        } else {
            // Only hearts left
            valid_cards.highest()
        }
    }

    /// Returns the highest card from the current hand
    /// which is less than the specified card,
    /// and of the same suit. If Omnibus ruleset,
    /// don't return the bonus card.
    fn duck_high(&self, card: Card) -> Option<Card> {
        Some(
            Cards::from_set(&self.hand)?
                .of_suit(card.suit)?
                .filter(|c| c.rank < card.rank)?
                .try_exclude(|c| self.is_bonus_card(c))
                .highest(),
        )
    }

    /// Returns the lowest card of the specified suit
    /// from the current hand which is higher than
    /// the highest outstanding card of that suit,
    /// or None
    fn low_win(&self, suit: Suit) -> Option<Card> {
        let suit_list = Cards::from_set(&self.hand)?
            .of_suit(suit)?
            .try_exclude_card(Card::QUEEN_OF_SPADES);

        if let Some(highest_rank_in_deck) = self
            .deck
            .iter()
            .filter(|c| c.suit == suit)
            .max_by_key(|c| c.rank)
        {
            Some(
                suit_list
                    .filter(|c| c.rank > highest_rank_in_deck.rank)?
                    .lowest(),
            )
        } else {
            Some(suit_list.lowest())
        }
    }

    /// Returns a two element list containing the next highest,
    /// and next lowest, deck cards to the specified card.
    fn bracket(&self, card: Card) -> (Rank, Rank) {
        (
            self.deck
                .iter()
                .filter(|c| c.suit == card.suit && c.rank < card.rank)
                .max_by_key(|c| c.rank)
                .map_or(Rank::TWO, |c| c.rank.succ()),
            self.deck
                .iter()
                .filter(|c| c.suit == card.suit && c.rank > card.rank)
                .min_by_key(|c| c.rank)
                .map_or(Rank::ACE, |c| c.rank.pred()),
        )
    }

    /// Returns a list of cards from the current hand
    /// which are of the same effective rank as the
    /// the specified card. (The card itself is a member
    /// of the list, if it is in the current hand.)
    fn list_peers(&self, card: Card) -> Cards {
        if card == Card::QUEEN_OF_SPADES || self.is_bonus_card(card) {
            return Cards::new(card);
        }

        let (low, high) = self.bracket(card);

        Cards::from_set(&self.hand)
            .unwrap()
            .filter(|c| c.suit == card.suit)
            .unwrap()
            .filter(|c| c != Card::QUEEN_OF_SPADES && !self.is_bonus_card(c))
            .unwrap()
            .filter(|c| c.rank >= low && c.rank <= high)
            .unwrap()
    }
}

impl PlayerAI for Peter {
    fn get_name(&self) -> &str {
        "Peter"
    }

    fn update(&mut self, cards: &[Card], score: &Score) {
        self.hand = cards.iter().cloned().collect();
        self.score = score.clone();
    }

    fn select_cards(&mut self, direction: Side) -> Pass {
        // Remember pass direction
        self.pass_dir = Some(direction);

        // Create deck of unseen cards
        self.deck = Card::DECK.iter().cloned().collect();
        for card in &self.hand {
            self.deck.remove(&card);
        }

        self.pass_cards = Pass::new();

        if self.pass_to_shoot() {
            while !self.pass_cards.is_full() {
                // Pass unduckable hearts
                if !self.is_duckable(Suit::HEARTS) {
                    let low_heart = *self
                        .hand
                        .iter()
                        .filter(|c| c.suit == Suit::HEARTS)
                        .min_by_key(|c| c.rank)
                        .unwrap();
                    self.select(low_heart);
                }
                if let Some(low_spade) = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES && c.rank < Rank::QUEEN)
                    .cloned()
                    .min_by_key(|c| c.rank)
                {
                    // Pass spades below the Queen
                    self.select(low_spade);
                } else if self.hand.contains(&Card::TWO_OF_CLUBS)
                    && self.hand.iter().filter(|c| c.suit == Suit::CLUBS).count() < 5
                    && self.rules.clubs_lead
                {
                    // Pass deuce of clubs (if must be led)
                    self.select(Card::TWO_OF_CLUBS);
                } else {
                    // Pass lowest cards in hand
                    let lowest = *self.hand.iter().min_by_key(|c| c.rank).unwrap();
                    self.select(lowest);
                }
            }
            return self.pass_cards.clone();
        }

        // Only pass spades as a last resort
        if !safe_spades(&self.hand) {
            if self.hand.contains(&Card::QUEEN_OF_SPADES) {
                self.select(Card::QUEEN_OF_SPADES);
                if self.hand.contains(&Card::ACE_OF_SPADES) {
                    self.select(Card::ACE_OF_SPADES);
                }
                if self.hand.contains(&Card::KING_OF_SPADES) {
                    self.select(Card::KING_OF_SPADES);
                }
            } else {
                if self.hand.iter().filter(|c| c.suit == Suit::CLUBS).count() <= 3 {
                    for i in self
                        .hand
                        .clone()
                        .into_iter()
                        .filter(|c| c.suit == Suit::CLUBS)
                    {
                        self.select(i);
                    }
                } else if self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::DIAMONDS)
                    .count()
                    <= 3
                {
                    for i in self
                        .hand
                        .clone()
                        .into_iter()
                        .filter(|c| c.suit == Suit::DIAMONDS)
                    {
                        self.select(i);
                    }
                } else {
                    if self.hand.contains(&Card::ACE_OF_SPADES) {
                        self.select(Card::ACE_OF_SPADES);
                    }
                    if self.hand.contains(&Card::KING_OF_SPADES) {
                        self.select(Card::KING_OF_SPADES);
                    }
                }
            }
        }

        // Pass off the other cards
        while !self.pass_cards.is_full() {
            let clubs_list = self
                .hand
                .clone()
                .into_iter()
                .filter(|c| c.suit == Suit::CLUBS)
                .sorted_by_key(|c| -(c.rank as i32))
                .collect::<Vec<_>>();
            let diamonds_list = self
                .hand
                .clone()
                .into_iter()
                .filter(|c| c.suit == Suit::DIAMONDS)
                .sorted_by_key(|c| -(c.rank as i32))
                .collect::<Vec<_>>();
            let hearts_list = self
                .hand
                .clone()
                .into_iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .sorted_by_key(|c| -(c.rank as i32))
                .collect::<Vec<_>>();
            let safe_hearts = self.list_safe(Suit::HEARTS);
            let some_high_heart = self
                .hand
                .clone()
                .into_iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .max_by_key(|c| c.rank);

            // If we can clear a minor suit do so,
            // unless Omnibus ruleset and we have the bonus card
            // else pick an unsafe heart or the highest minor
            // If spots ruleset, pass unsafe hearts first
            if self.rules.ruleset == Ruleset::SpotHearts {
                if let Some(high_heart) = some_high_heart {
                    if !safe_hearts.contains(&high_heart) && high_heart.rank > Rank::SIX {
                        self.select(high_heart);
                        continue;
                    }
                }
            }
            if clubs_list.len() > 0 && clubs_list.len() <= 3 - self.pass_cards.len() {
                let high_club = *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::CLUBS)
                    .max_by_key(|c| c.rank)
                    .unwrap();
                self.select(high_club);
            } else if diamonds_list.len() > 0
                && diamonds_list.len() <= 3 - self.pass_cards.len()
                && !self.hand.iter().any(|c| self.is_bonus_card(*c))
            {
                let high_diamond = *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::DIAMONDS)
                    .max_by_key(|c| c.rank)
                    .unwrap();
                self.select(high_diamond);
            } else if hearts_list.len() > 0
                && !safe_hearts.contains(&hearts_list[0])
                && hearts_list[0].rank > Rank::SIX
            {
                self.select(hearts_list[0]);
            } else {
                if let Some(card) = self
                    .hand
                    .clone()
                    .into_iter()
                    .filter(|c| c.suit == Suit::CLUBS || c.suit == Suit::DIAMONDS)
                    .max_by_key(|c| c.rank)
                {
                    self.select(card);
                } else {
                    let high = *self.hand.iter().max_by_key(|c| c.rank).unwrap();
                    self.select(high);
                }
            }
        }

        return self.pass_cards.clone();
    }

    fn receive_cards(&mut self, cards: &Pass) {
        self.received_cards = cards.to_array().into_iter().collect();
    }

    fn play_card(&mut self, valid_cards: &Cards, trick: &Trick) -> Card {
        // Play Card Entry
        self.trick_num = 14 - self.hand.len();
        // If first trick of round
        if self.trick_num == 1 {
            // Reset round variables
            self.hearts_played = false;
            self.queen_played = false;
            self.shooting = false;
            self.hearts_split = false;
            self.can_shoot = true;
            self.void_counts.clear();
            // Remove my hand from deck
            self.deck = Card::DECK.iter().cloned().collect();
            for card in &self.hand {
                self.deck.remove(&card);
            }
        }

        // Check for shooting
        let shoot_code = self.try_to_shoot(trick);
        self.shooting = shoot_code.is_some();

        let mut score_list = vec![];
        for index in &Side::ALL {
            let score = self.score.round.get(*index);
            if score != 0 && *index != self.direction {
                score_list.push(score);
            }
        }
        if score_list.len() > 0 {
            self.can_shoot = false;
            self.shooting = false;
        }
        if score_list.len() > 1 || self.score.round.get(self.direction) != 0 {
            self.hearts_split = true;
        }
        // invoke action routine
        if let Some(only_card) = valid_cards.only() {
            return only_card;
        }

        if self.shooting {
            // Routines for shooting
            if trick.num_played() == 0 {
                self.open_trick_shoot(valid_cards)
            } else if self.hand.iter().any(|c| c.suit == trick.trump().unwrap()) {
                self.follow_suit_shoot(valid_cards, trick, shoot_code.unwrap())
            } else {
                self.dont_follow_suit_shoot(valid_cards, trick)
            }
        } else {
            // Routines for not shooting
            if trick.num_played() == 0 {
                self.open_trick(valid_cards)
            } else if self.hand.iter().any(|c| c.suit == trick.trump().unwrap()) {
                self.follow_suit(valid_cards, trick)
            } else {
                self.dont_follow_suit(valid_cards, trick)
            }
        }
    }

    fn trick_end(&mut self, trick: &Trick) {
        // remove played cards from global lists
        for card in trick.iter_cards() {
            self.deck.remove(card);
            self.pass_cards.remove(*card);
            self.received_cards.remove(card);
        }
        // Update global variables
        for (side, card) in trick.iter() {
            if Some(card.suit) != trick.trump() {
                self.void_counts.insert((*side, trick.trump().unwrap()));
            }
        }
        if trick.num_suit(Suit::HEARTS) > 0 {
            self.hearts_played = true;
        }
        if trick.contains(&Card::QUEEN_OF_SPADES) {
            self.queen_played = true;
        }
    }

    fn round_end(&mut self) {
        self.pass_dir = None;
        self.received_cards.clear();
    }
}
