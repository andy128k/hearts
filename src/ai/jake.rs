use super::player::PlayerAI;
use crate::card_list::*;
use crate::cards::*;
use crate::pass::*;
use crate::score::*;
use crate::side::*;
use crate::trick::*;
use itertools::Itertools;
use std::collections::HashSet;

pub struct Jake {
    hand: Vec<Card>,
    // Keeps track of bad suits: a suit that some other player doesn't have. These suits can be dangerous to take.
    player_out_suits: HashSet<(Side, Suit)>,
    seen_cards: HashSet<Card>,
}

impl Jake {
    pub fn new() -> Self {
        Self {
            hand: Vec::new(),
            player_out_suits: HashSet::new(),
            seen_cards: HashSet::new(),
        }
    }

    /// Add all the cards played thus far to the list seen_cards
    fn record_played_cards(&mut self, trick: &Trick) {
        for card in &self.hand {
            self.seen_cards.insert(*card);
        }
        for card in trick.iter_cards() {
            self.seen_cards.insert(*card);
        }
    }

    /// returns true if the only unseen cards of same suit beat this card
    fn is_giveaway_card(&self, card: Card) -> bool {
        Rank::ALL
            .iter()
            .filter(|rank| **rank < card.rank)
            .map(|rank| Card::new(card.suit, *rank))
            .all(|card| self.seen_cards.contains(&card))
    }

    /// returns true if the suit still has some scoring cards unseen,
    /// i.e., if the suit is either hearts or spades with an unplayed Queen.
    fn is_scoring_suit(&self, suit: Suit) -> bool {
        suit == Suit::HEARTS
            || suit == Suit::SPADES
                && (!self.seen_cards.contains(&Card::QUEEN_OF_SPADES)
                    || self.hand.contains(&Card::QUEEN_OF_SPADES))
    }

    /// returns number of cards not accounted for
    fn num_unseen(&self, suit: Suit) -> usize {
        13 - self.seen_cards.iter().filter(|c| c.suit == suit).count()
    }

    /// returns number of players out of a suit (some player doesn't have it)
    /// Note: includes the player himself in this count, but that should be
    /// harmless.
    fn num_players_out(&self, suit: Suit) -> usize {
        self.player_out_suits
            .iter()
            .filter(|(_, s)| *s == suit)
            .count()
    }

    /// Return the number of unseen cards that beat this card
    fn unseen_above(&self, card: Card) -> usize {
        Rank::ALL
            .iter()
            .filter(|rank| **rank > card.rank)
            .filter(|rank| !self.seen_cards.contains(&Card::new(card.suit, **rank)))
            .count()
    }

    /// Spades are iffy if I have the queen or a card higher than the queen
    fn spades_is_iffy(&self) -> bool {
        self.hand
            .iter()
            .any(|card| card.suit == Suit::SPADES && card.rank >= Rank::QUEEN)
    }

    /// call a card a giveaway card if it is lower than any card in the
    /// suit not yaet seen and someone has a card in the suit not yet
    /// seen.  If we lead with a giveaway card, someone else will take
    /// the trick.
    ///
    /// If I have the ace or king of a non-dangerous suit, then play it.
    ///
    /// If I do not have anything queen or above of spades and if spades
    /// are not bad, then play the highest spade below the queen.
    ///
    /// Play a giveaway heart.
    ///
    /// Even if spades are bad, play a low spade provided I have nothing
    /// above Jack in my hand.
    ///
    /// Play a giveaway card in any suit.  Choose the suit with the most
    /// folks out of it.
    ///
    /// Pick the card that has the most trump cards above it unseen.
    /// (We may as well restrict ourselves to the lowest card of each
    /// suit.)
    fn choose_lead_card(&self, valid_cards: &Cards) -> Card {
        // If I have an ace or king of a non-dangerous suit and there are
        // at least 8 unplayed cards of that suit, play it.
        let suits_by_length = Suit::ALL.iter().sorted_by_key(|suit| {
            usize::max_value() - valid_cards.of_suit(**suit).map_or(0, |cards| cards.len())
        });
        for suit in suits_by_length {
            if let Some(card) = valid_cards.of_suit(*suit).map(|cards| cards.highest()) {
                if card.rank > Rank::QUEEN
                    && !self.is_scoring_suit(card.suit)
                    && self.num_players_out(card.suit) == 0
                    && self.num_unseen(card.suit) > 5
                {
                    return card;
                }
            }
        }

        // If I do not have anything queen or above of spades and if no one
        // is out of spades, then play the highest spade below the queen.
        if self.num_players_out(Suit::SPADES) == 0
            && !self.seen_cards.contains(&Card::QUEEN_OF_SPADES)
        {
            if let Some(high_spade) = valid_cards
                .of_suit(Suit::SPADES)
                .map(|cards| cards.highest())
            {
                if high_spade.rank < Rank::QUEEN {
                    return high_spade;
                }
            }
        }

        // Play a giveaway heart
        if let Some(low_heart) = valid_cards
            .of_suit(Suit::HEARTS)
            .map(|cards| cards.lowest())
        {
            if self.is_giveaway_card(low_heart) {
                return low_heart;
            }
        }

        // Even if spades are bad, play a low spade provided I
        // have nothing above Jack in my hand and the Queen is
        // still out.
        if !self.seen_cards.contains(&Card::QUEEN_OF_SPADES) {
            if let Some(spades) = valid_cards.of_suit(Suit::SPADES) {
                if spades.highest().rank < Rank::QUEEN {
                    return spades.lowest();
                }
            }
        }

        // Play a giveaway card in any suit. Choose the suit with the most folks out of it.
        for cards_of_suit in Suit::ALL
            .iter()
            .filter_map(|suit| valid_cards.of_suit(*suit))
            .sorted_by_key(|cards| usize::max_value() - cards.len())
        {
            let cmin = cards_of_suit.lowest();
            let cmax = cards_of_suit.highest();
            if self.is_giveaway_card(cmin) && (cmin.suit != Suit::SPADES || cmax.rank < Rank::QUEEN)
            {
                return cmin;
            }
        }

        // Pick the card that has the most trump cards above it unseen.
        // (We may as well restrict ourselves to the lowest card of each suit.)
        let best_suit = Suit::ALL
            .iter()
            .filter(|suit| **suit != Suit::SPADES || !self.spades_is_iffy())
            .max_by_key(|suit| {
                valid_cards
                    .of_suit(**suit)
                    .map(|cards| self.unseen_above(cards.lowest()))
            });

        if let Some(best_suit) = best_suit {
            let best_card = valid_cards.of_suit(*best_suit).unwrap().lowest();
            // try a non-points spade
            if self.unseen_above(best_card) == 0 {
                if let Some(non_point_spades) = valid_cards
                    .try_exclude_card(Card::QUEEN_OF_SPADES)
                    .of_suit(Suit::SPADES)
                {
                    return non_point_spades.lowest();
                }
            }
            return best_card;
        } else {
            valid_cards.lowest()
        }
    }

    fn avoid_trick(&self, valid_cards: &Cards, high_card: &Card, trick: &Trick) -> Card {
        // play the highest card that doesn't take the trick
        if let Some(dodge_cards) =
            valid_cards.filter(|card| card.suit != high_card.suit || card.rank < high_card.rank)
        {
            return dodge_cards.highest();
        }

        // we can't dodge, so take it with the fewest points.
        // If we're the last player, use a high card.
        // Otherwise use a low card and hope that someone else gets it.
        if trick.num_played() == 3 {
            valid_cards
                .try_exclude_card(Card::QUEEN_OF_SPADES)
                .highest()
        } else {
            valid_cards.try_exclude_card(Card::QUEEN_OF_SPADES).lowest()
        }
    }

    fn take_trick(&self, valid_cards: &Cards, high_card: &Card) -> Card {
        // if someone else will take the Queen, play it!
        // Getting rid of the Queen is important.
        if valid_cards.contains(Card::QUEEN_OF_SPADES) && Rank::QUEEN < high_card.rank {
            return Card::QUEEN_OF_SPADES;
        }

        // Play the highest card that's no a point
        if let Some(pointless_cards) = valid_cards.filter(|c| !is_point_card(c)).and_then(|cards| {
            cards.filter(|c| {
                // Try not to trump the queen of spades
                if high_card.suit == Suit::SPADES
                    && !self.seen_cards.contains(&Card::QUEEN_OF_SPADES)
                {
                    c.rank < Rank::QUEEN
                } else {
                    true
                }
            })
        }) {
            return pointless_cards.highest();
        }
        // Have to play a point. Go low
        valid_cards.lowest()
    }

    fn dump_cards(&self, valid_cards: &Cards, _high_card: &Card) -> Card {
        // We don't have the suit at all.  First, try to dump high spades.
        if valid_cards.contains(Card::QUEEN_OF_SPADES) {
            return Card::QUEEN_OF_SPADES;
        }
        if valid_cards.contains(Card::ACE_OF_SPADES) && self.is_scoring_suit(Suit::SPADES) {
            return Card::ACE_OF_SPADES;
        }
        if valid_cards.contains(Card::KING_OF_SPADES) && self.is_scoring_suit(Suit::SPADES) {
            return Card::KING_OF_SPADES;
        }

        // Dump high cards (above queen), any suit.
        if let Some(high_card) = valid_cards
            .filter(|c| c.rank > Rank::JACK)
            .map(|cards| cards.highest())
        {
            return high_card;
        }

        // Dump the highest point card we have (or just highest).
        valid_cards.try_filter(is_point_card).highest()
    }

    /// Called when we are the fourth player on the trick.
    ///
    /// Have the suit:
    /// - If there are no points in the trick, take it with the highest card we have.
    /// - If there are points in the trick, play the highest card that won't take it.  
    /// - If we are forced to take it, take it with the highest card we have.
    /// Not have the suit:
    /// - Play the Queen of Spades.
    /// - Play the King or Ace of Spades
    /// - Play the highest Queen or above card we have.
    /// - Play the highest heart.
    /// - Play the highest card we have.
    fn choose_final_card(&self, valid_cards: &Cards, trick: &Trick) -> Card {
        let high_card = trick.get_winner_card().unwrap();
        if valid_cards.have_suit(high_card.suit) {
            if trick.num_point_cards() > 0 {
                self.avoid_trick(valid_cards, high_card, trick)
            } else {
                self.take_trick(valid_cards, high_card)
            }
        } else {
            self.dump_cards(valid_cards, high_card)
        }
    }

    /// return true if one of the remaining players doesn't have the suit
    /// of the trick and either (has hearts or queen of spades is not played)
    fn is_dangerous_trick(&self, high_card: &Card, trick: &Trick) -> bool {
        if self.num_unseen(high_card.suit) + trick.num_played() < 6 {
            // print "Not enough cards! Dangerous!"
            return true;
        }

        let remaining_players: Vec<&Side> = Side::ALL
            .iter()
            .filter(|side| !trick.iter().any(|(s, _)| s == *side))
            .collect();

        for side in remaining_players {
            if self.player_out_suits.contains(&(*side, high_card.suit))
                && (!self.player_out_suits.contains(&(*side, Suit::HEARTS))
                    || (!self.seen_cards.contains(&Card::QUEEN_OF_SPADES)
                        && !self.player_out_suits.contains(&(*side, Suit::SPADES))))
            {
                // print "Upcoming player is dangerous!"
                return true;
            }
        }
        false
    }

    /// Have the suit:
    /// - If the suit is not dangerous and has at least 7 cards in other folks hands,
    ///     play the highest card we have.
    /// - Else, play the highest card that won't take the trick.
    /// Not have the suit:
    /// - Play the Queen of Spades.
    /// - Play the highest Queen or above card we have.
    /// - Play the highest heart.
    ///
    /// - If the suit is not dangerous and has at least 7 cards in other folks hands,
    ///     play the highest card we have.
    fn choose_mid_card(&self, valid_cards: &Cards, trick: &Trick) -> Card {
        let high_card = trick.get_winner_card().unwrap();
        if valid_cards.have_suit(high_card.suit) {
            if trick.num_point_cards() > 0 || self.is_dangerous_trick(high_card, trick) {
                self.avoid_trick(valid_cards, high_card, trick)
            } else {
                self.take_trick(valid_cards, high_card)
            }
        } else {
            self.dump_cards(valid_cards, high_card)
        }
    }
}

impl PlayerAI for Jake {
    fn get_name(&self) -> &str {
        "Jake" // Alternative names: "Jesse" and "Quincy"
    }

    fn update(&mut self, cards: &[Card], _score: &Score) {
        self.hand = cards.to_vec();
    }

    /// This function is called when you have to select three cards to pass to the
    /// next player. Return a list of three cards.
    ///
    /// - If I have ace or king of spades, get rid of it.
    /// - If I have fewer than four spades, get rid of the queen.
    /// - If my lowest heart is greater than 6, get rid of every heart over 10.
    /// - If I have fewer than three diamonds, get rid of as many as possible.
    /// - If I have fewer than four clubs, get rid of as many as possible.
    /// - Get rid of the highest diamonds/clubs.
    fn select_cards(&mut self, _direction: Side) -> Pass {
        let mut result = Pass::new();

        // first pass on the high spades
        if let Some(card) = select_card(&mut self.hand, &Card::ACE_OF_SPADES) {
            result.add(card);
        }
        if let Some(card) = select_card(&mut self.hand, &Card::KING_OF_SPADES) {
            result.add(card);
        }
        if self.hand.iter().filter(|c| c.suit == Suit::SPADES).count() < 4 {
            if let Some(card) = select_card(&mut self.hand, &Card::QUEEN_OF_SPADES) {
                result.add(card);
            }
        }

        // if our lowest heart is above six, get rid of all heart face cards.
        if let Some(lowest_heart) = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::HEARTS)
            .min_by_key(|c| c.rank)
        {
            if lowest_heart.rank > Rank::SIX {
                let faces = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::HEARTS && c.rank >= Rank::JACK)
                    .sorted_by_key(|c| c.rank);
                result.add_all(faces.into_iter().rev());
            }
        }

        let clubs_list = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::CLUBS)
            .sorted_by_key(|c| c.rank);
        let diamonds_list = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::DIAMONDS)
            .sorted_by_key(|c| c.rank);

        // Try to clear diamonds and clubs
        if clubs_list.len() < diamonds_list.len() {
            if clubs_list.len() > 0 && clubs_list.len() < 4 {
                result.add_all(clubs_list.into_iter().rev());
            }
            if diamonds_list.len() > 0 && diamonds_list.len() < 4 {
                result.add_all(diamonds_list.into_iter().rev());
            }
        } else {
            if diamonds_list.len() > 0 && diamonds_list.len() < 4 {
                result.add_all(diamonds_list.into_iter().rev());
            }
            if clubs_list.len() > 0 && clubs_list.len() < 4 {
                result.add_all(clubs_list.into_iter().rev());
            }
        }

        // dump dull cards
        result.add_all(
            self.hand
                .iter()
                .filter(|c| c.suit == Suit::CLUBS || c.suit == Suit::DIAMONDS)
                .sorted_by_key(|c| c.rank)
                .rev(),
        );

        // I shouldn't need to dump any more hearts or spades, but
        // it's always possible that I was dealt only these two suits!
        result.add_all(
            self.hand
                .iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .sorted_by_key(|c| c.rank)
                .rev(),
        );
        result.add_all(
            self.hand
                .iter()
                .filter(|c| c.suit == Suit::SPADES)
                .sorted_by_key(|c| c.rank)
                .rev(),
        );

        result
    }

    /// LEADING:
    ///
    /// Call a suit bad if I know that one player is out of that suit.
    ///
    /// Call a suit dangerous if either it contains an unseen scoring
    /// card (i.e., spades before the queen has been seen or hearts anytime) or
    /// it is bad.
    ///
    /// If I have the ace or king of a non-dangerous suit, then play it.
    ///    
    /// If I do not have anything queen or above of spades and if spades
    /// are not bad, then play the highest spade below the queen.
    ///
    /// If I have a heart that I know will not take the trick, play it
    /// (if possible).  (Play the highest heart that won't take the trick).
    ///
    /// Even if spades are bad, play a low spade provided I have nothing
    /// above Jack in my hand.
    ///
    /// Play the highest diamond/club that will not take the trick.
    ///
    /// FOLLOWING:
    ///    
    /// Not fourth:
    ///     Have the suit:
    ///         If the suit is not dangerous and has at least 7 cards in other folks hands,
    ///         play the highest card we have.
    ///         Else, play the highest card that won't take the trick.
    ///     Not have the suit:
    ///         Play the Queen of Spades.
    ///         Play the highest Queen or above card we have.
    ///         Play the highest heart.
    /// Fourth:
    ///     Have the suit:
    ///         If there are no points in the trick, take it with the highest card we have.
    ///         If there are points in the trick, play the highest card that won't take it.  
    ///         If we are forced to take it, take it with the highest card we have.
    ///     Not have the suit:
    ///         As above.
    fn play_card(&mut self, valid_cards: &Cards, trick: &Trick) -> Card {
        self.record_played_cards(trick);
        match trick.num_played() {
            0 => self.choose_lead_card(valid_cards),
            3 => self.choose_final_card(valid_cards, trick),
            _ => self.choose_mid_card(valid_cards, trick),
        }
    }

    fn trick_end(&mut self, trick: &Trick) {
        let trump = trick.trump().unwrap();
        for (side, card) in trick.iter() {
            if card.suit != trump {
                self.player_out_suits.insert((*side, trump));
            }
        }
        self.record_played_cards(trick);
    }

    fn round_end(&mut self) {
        self.player_out_suits.clear();
        self.seen_cards.clear();
    }
}
