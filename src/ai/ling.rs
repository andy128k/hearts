use super::player::PlayerAI;
use crate::card_list::*;
use crate::cards::*;
use crate::pass::*;
use crate::score::*;
use crate::side::*;
use crate::trick::*;
use itertools::Itertools;
use std::cmp::Ord;
use std::collections::HashSet;

fn count_suit(cards: &Cards, suit: Suit) -> usize {
    cards.of_suit(suit).map_or(0, |cards| cards.len())
}

pub struct Ling {
    direction: Side,
    hand: Vec<Card>,

    // Keeps track of bad suits: a suit that some other player doesn't have. These suits can be dangerous to take.
    player_out_suits: HashSet<(Side, Suit)>,

    seen_cards: HashSet<Card>,
    score: Score,
}

impl Ling {
    pub fn new(direction: Side) -> Self {
        Self {
            direction,
            hand: Vec::new(),
            player_out_suits: HashSet::new(),
            seen_cards: HashSet::new(),
            score: Score::default(),
        }
    }

    /// Add all the cards played thus far to the list seen_cards
    fn record_played_cards(&mut self, trick: &Trick) {
        for card in &self.hand {
            self.seen_cards.insert(*card);
        }
        for card in trick.iter_cards() {
            self.seen_cards.insert(*card);
        }
    }

    /// returns true if the only unseen cards of same suit beat this card
    fn is_giveaway_card(&self, card: Card) -> bool {
        self.unseen_below(card) == 0 && self.unseen_above(card) > 0
    }

    fn is_played(&self, card: Card) -> bool {
        !self.hand.contains(&card) && self.seen_cards.contains(&card)
    }

    /// returns true if the number of unseen cards of same
    /// suit below card is fewer than the number of players
    /// that presumably have the suit.  Of course, we require
    /// at least one player above this card!
    fn is_probable_giveaway_card(&self, card: Card) -> bool {
        3 - self.num_players_out(card.suit) > self.unseen_below(card) && self.unseen_above(card) > 0
    }

    /// returns true if the suit still has some scoring cards unseen,
    /// i.e., if the suit is either hearts or spades with an unplayed Queen.
    fn is_scoring_suit(&self, suit: Suit) -> bool {
        suit == Suit::HEARTS || suit == Suit::SPADES && !self.is_played(Card::QUEEN_OF_SPADES)
    }

    /// returns number of cards in suit not accounted for
    fn num_unseen(&self, suit: Suit) -> usize {
        13 - self.seen_cards.iter().filter(|c| c.suit == suit).count()
    }

    /// returns number of players out of a suit (some player
    /// doesn't have it) Note: includes the player himself in
    /// this count, but that should be harmless.
    fn num_players_out(&self, suit: Suit) -> usize {
        self.player_out_suits
            .iter()
            .filter(|(_, s)| *s == suit)
            .count()
    }

    /// returns true if the guy who is currently winning the
    /// trick has scored more than 14 pts. this hand and no
    /// one else has scored a single pt.  If so, then we need
    /// to change strategy so that he can't shoot the moon.
    fn moon_shooter(&self, valid_cards: &Cards, trick: &Trick) -> bool {
        if let Some(first_played) = trick.first_played_card() {
            // A list of all the non-zero scorers. If there's only one, then he might be shooting the moon.
            let scorers: Vec<_> = Side::ALL
                .iter()
                .filter(|side| self.score.round.get(**side) > 0)
                .collect();

            if scorers.len() > 1 {
                return false;
            }

            // If he is dropping a high spade as the only scorer, then he's pretty suspicious!
            if first_played.suit == Suit::SPADES
                && first_played.rank >= Rank::QUEEN
                && (!self.is_played(Card::QUEEN_OF_SPADES)
                    || *first_played == Card::QUEEN_OF_SPADES)
            {
                return true;
            }

            if first_played.suit == Suit::HEARTS && first_played.rank > Rank::TEN {
                return true;
            }

            if scorers.len() == 1 {
                let scorer = scorers[0];
                let his_card = trick.get_card(*scorer).unwrap();

                // If the only guy with points isn't currently
                // taking the trick, then we won't worry.
                // NOTE: He may not have played yet, but if
                // not, then he is not in control, so we won't
                // worry.

                if Some(&his_card) == trick.get_winner_card() {
                    // Let's assume moon_shooter if he has more
                    // than 14 points so far or if he has more than
                    // 6 points and the queen hasn't shown up yet.

                    // He's taken the queen and at least two other hearts.
                    if self.score.round.get(*scorer) > 14 {
                        return true;
                    }

                    // He's taken more than six hearts

                    if self.score.round.get(*scorer) > 6
                        && (!self.is_played(Card::QUEEN_OF_SPADES)
                            || trick.contains(&Card::QUEEN_OF_SPADES))
                    {
                        return true;
                    }

                    // If he's led with the highest unseen
                    // card of a suit that someone is out
                    // of.  This leads to some false
                    // positives (Luis is stoopid!), so
                    // let's restrict it to the case in
                    // which he's already taken at least
                    // two points.

                    if self.unseen_above(his_card) == 0
                        && self.num_players_out(his_card.suit) > 0
                        && self.score.round.get(*scorer) > 1
                    {
                        // If we can take the trick,
                        // then he wouldn't know that,
                        // so let's not count it as a
                        // moon shooter in that case.
                        if self.num_players_out(his_card.suit) == 1
                            && valid_cards.have_suit(his_card.suit)
                        {
                            return false;
                        }
                        return true;
                    }
                }
            }
        }
        false
    }

    /// Someone is threatening to shoot the moon.  We'll take
    /// it if we can and if we aren't currently the high
    /// score.  Otherwise, we will play the card with the
    /// fewest unseen cards less than it (any suit) in hopes
    /// that we may take a later trick.
    fn moon_defense(&self, valid_cards: &Cards, trick: &Trick) -> Option<Card> {
        let high_card = trick.get_winner_card()?;

        // Check to see if we are the highest score.  If so, to
        // heck with it.  Let someone else make the sacrifice.

        let my_game_score = self.score.game.get(self.direction);

        if my_game_score != self.score.game.max()
            || (my_game_score < self.score.game.min() + 10 && my_game_score < 85)
            || my_game_score < 26
        {
            if let Some(winning) =
                valid_cards.filter(|c| c.suit == high_card.suit && c.rank > high_card.rank)
            {
                // winning contains a list of cards that could take the trick.

                // We can take the trick.  If it is
                // dangerous, we should take it with
                // the lowest card that will do.
                // Otherwise, might as well take it
                // with the highest card that will do.
                if !self.is_dangerous_trick(trick) {
                    return Some(self.choose_equivalent_to_highest(&winning, trick));
                } else {
                    // Play the lowest card that will take the trick.
                    return Some(self.choose_equivalent_to_lowest(&winning, trick));
                }
            }

            // We can't take the trick.  Let's look at the lowest
            // card in each suit.  We'd like to get rid of the
            // card with the fewest unseen cards *below* it, so
            // that we keep our high cards in our hand.  We might
            // still need to take a trick.

            // Exception: If I have a single card from this suit
            // which has no unseen cards above it, then I can
            // throw out cards below that card.  After all, if he
            // ever plays that suit, I'll take it.

            for suit in &Suit::ALL {
                if let Some(this_suit) = valid_cards.of_suit(*suit) {
                    // If one of our cards from
                    // this suit has no unseen
                    // cards above it, then get
                    // rid of something equivalent
                    // to the second highest card
                    // of this suit.
                    let highest = this_suit.highest();
                    if self.unseen_above(highest) == 0 {
                        if let Some(this_suit_but_highest) = this_suit.filter(|c| c != highest) {
                            return Some(
                                self.choose_equivalent_to_highest(&this_suit_but_highest, trick),
                            );
                        }
                    }
                }
            }

            let best = Suit::ALL
                .iter()
                .filter_map(|suit| valid_cards.of_suit(*suit))
                .map(|cards| cards.lowest())
                .max_by_key(|card| self.unseen_below(*card))
                .unwrap();

            return Some(self.choose_equiv(best, valid_cards, trick));
        }
        None
    }

    /// We prefer to dump a if there are fewer cards beneath a such that unseen_above is > 0.
    fn dump_rank(&self, suit: Suit, cards: &Cards) -> usize {
        cards
            .of_suit(suit)
            .and_then(|cards| cards.filter(|c| self.unseen_above(c) > 0))
            .map_or(0, |cards| cards.len())
    }

    /// Suits with only one card are lowest.
    /// If two suits have more than one card, then we rank the suits as follows:
    /// check the number of unseen cards above the *second* member of the suit, the more the better.
    ///
    /// If spades are iffy, rank them higher than anything else.
    fn key_prob_give_pref(&self, cards: &Cards) -> impl Ord {
        let highest = cards.highest();
        (
            self.spades_is_iffy() && highest.suit != Suit::SPADES,
            cards.len() == 1,
            cards
                .filter(|c| c != highest)
                .map(|cards| self.unseen_above(cards.highest())),
        )
    }

    /// return the number of unseen cards that are below card.
    fn unseen_below(&self, card: Card) -> usize {
        Rank::ALL
            .iter()
            .filter(|rank| **rank < card.rank)
            .filter(|rank| !self.seen_cards.contains(&Card::new(card.suit, **rank)))
            .count()
    }

    /// Return the number of unseen cards that beat this card
    fn unseen_above(&self, card: Card) -> usize {
        Rank::ALL
            .iter()
            .filter(|rank| **rank > card.rank)
            .filter(|rank| !self.seen_cards.contains(&Card::new(card.suit, **rank)))
            .count()
    }

    /// Spades are iffy if I have the queen or a card higher than the queen
    fn spades_is_iffy(&self) -> bool {
        !self.is_played(Card::QUEEN_OF_SPADES)
            && self
                .hand
                .iter()
                .any(|card| card.suit == Suit::SPADES && card.rank >= Rank::QUEEN)
    }

    fn will_take_trick(&self, card: Card, trick: &Trick) -> bool {
        trick
            .get_winner_card()
            .map(|winner| card.suit == winner.suit && card.rank > winner.rank)
            .unwrap_or(false)
    }

    /// Two cards are equivalent if they have the same suit,
    /// the same unseen_above (and hence the same unseen_below)
    /// and either both of them or neither of them take the current trick.
    fn is_equiv(&self, a: Card, b: Card, trick: &Trick) -> bool {
        a.suit == b.suit
            && self.unseen_above(a) == self.unseen_above(b)
            && self.will_take_trick(a, trick) == self.will_take_trick(b, trick)
    }

    /// Randomly select a card from list which has the same unseen_above score as card.
    fn choose_equiv(&self, card: Card, cards: &Cards, trick: &Trick) -> Card {
        cards
            .filter(|c| self.is_equiv(c, card, trick))
            .unwrap()
            .random()
    }

    fn choose_equivalent_to_lowest(&self, cards: &Cards, trick: &Trick) -> Card {
        self.choose_equiv(cards.lowest(), cards, trick)
    }

    fn choose_equivalent_to_highest(&self, cards: &Cards, trick: &Trick) -> Card {
        self.choose_equiv(cards.highest(), cards, trick)
    }

    /// call a card a giveaway card if it is lower than any
    /// card in the suit not yet seen and someone has a card
    /// in the suit not yet seen.  If we lead with a giveaway
    /// card, someone else will take the trick.
    ///
    /// If I have the ace or king of a non-dangerous suit,
    /// then play it.
    ///
    /// If I do not have anything queen or above of spades and
    /// if spades are not bad, then play the highest spade
    /// below the queen.
    ///
    /// Play a giveaway heart.
    ///
    /// Even if spades are bad, play a low spade provided I
    /// have nothing above Jack in my hand.
    ///
    /// Play a giveaway card in any suit.  Choose the suit
    /// with the most folks out of it.
    ///
    /// Pick the card that has the most trump cards above it
    /// unseen.  (We may as well restrict ourselves to the
    /// lowest card of each suit.)
    fn choose_lead_card(&self, valid_cards: &Cards, trick: &Trick) -> Card {
        // If I have any cards with unseen_above of 0 and a
        // non-dangerous suit and there are at least 8 unplayed
        // cards of that suit, play it.

        // Example: If I have the J and K and the Q and A have
        // both been played, then both J and K qualify.  Play
        // one of them.

        // If I have the Queen and there are no unseen spades
        // beneath it and at least one unseen spade above it,
        // play it!  What a giveaway!

        if valid_cards.contains(Card::QUEEN_OF_SPADES)
            && self.is_giveaway_card(Card::QUEEN_OF_SPADES)
        {
            return Card::QUEEN_OF_SPADES;
        }

        let suits_by_length = Suit::ALL
            .iter()
            .cloned()
            .sorted_by_key(|suit| {
                usize::max_value() - valid_cards.of_suit(*suit).map_or(0, |cards| cards.len())
            })
            .collect::<Vec<_>>();

        for suit in &suits_by_length {
            if !self.is_scoring_suit(*suit)
                && self.num_players_out(*suit) == 0
                && self.num_unseen(*suit) > 5
            {
                // Form a list of all those cards with 0 cards
                // unseen above.  Ace is always in this list
                // (if you have it).  King is in the list if
                // you've seen the Ace, and so on.
                // Randomly pick a card. Don't always choose the highest, since that gives too much information.
                if let Some(card) = valid_cards
                    .of_suit(*suit)
                    .and_then(|cards| cards.filter(|c| self.unseen_above(c) == 0))
                    .map(|cards| cards.random())
                {
                    return card;
                }
            }
        }

        // If I do not have anything queen or above of spades
        // and if no one is out of spades, then play the
        // highest spade below the queen.

        if valid_cards.have_suit(Suit::SPADES)
            && self.num_players_out(Suit::SPADES) == 0
            && !self.is_played(Card::QUEEN_OF_SPADES)
            && !self.spades_is_iffy()
        {
            let spades = valid_cards.of_suit(Suit::SPADES).unwrap();
            return self.choose_equivalent_to_highest(&spades, trick);
        }

        // Play a giveaway heart
        if let Some(giveaway_heart) = valid_cards
            .of_suit(Suit::HEARTS)
            .and_then(|cards| cards.filter(|c| self.is_giveaway_card(c)))
            .map(|cards| cards.random())
        {
            return giveaway_heart;
        }

        // Even if spades are bad, play a low spade provided I have nothing above Jack in my hand and the Queen is still out.
        if !self.is_played(Card::QUEEN_OF_SPADES) && !self.spades_is_iffy() {
            if let Some(spades) = valid_cards.of_suit(Suit::SPADES) {
                return self.choose_equivalent_to_lowest(&spades, trick);
            }
        }

        // Play a giveaway card in any suit. Choose the suit with the most folks out of it.
        for suit in suits_by_length {
            if suit == Suit::SPADES && self.spades_is_iffy() {
                continue;
            }

            if let Some(card) = valid_cards
                .of_suit(suit)
                .and_then(|cards| cards.filter(|c| self.is_giveaway_card(c)))
                .map(|cards| cards.random())
            {
                return card;
            }
        }

        // Play a "probable giveaway".  This is a card where
        // the number of cards unseen below it is fewer than
        // the 4 - number of players out of the suit.  Of
        // course, we still require that there is at least one
        // unseen card above this card!

        // If we have a probable giveaway that is the only card
        // in that suit, play it first.
        //
        // Otherwise, rank preference according to the number
        // of cards unseen above the *second* card of the suit.

        if let Some(candidate_suit) = Suit::ALL
            .iter()
            .filter(|suit| **suit != Suit::SPADES || !self.spades_is_iffy())
            .filter_map(|suit| valid_cards.of_suit(*suit))
            .filter(|cards| self.is_probable_giveaway_card(cards.highest()))
            .min_by_key(|cards| self.key_prob_give_pref(cards))
        {
            return self.choose_equivalent_to_lowest(&candidate_suit, trick);
        }

        // Pick the card that has the most trump cards above it unseen.
        // (We may as well restrict ourselves to the lowest card of each suit.)
        let best_card = Suit::ALL
            .iter()
            .filter(|suit| **suit != Suit::SPADES || !self.spades_is_iffy())
            .filter_map(|suit| valid_cards.of_suit(*suit))
            .map(|cards| cards.lowest())
            .max_by_key(|card| self.unseen_above(*card));

        if let Some(best_card) = best_card {
            // try a non-points spade
            if self.unseen_above(best_card) == 0 {
                if let Some(pointless_spades) =
                    valid_cards.filter(|c| c.suit == Suit::SPADES && c.rank != Rank::QUEEN)
                {
                    let pointless_spade = pointless_spades.lowest();
                    if self.unseen_above(pointless_spade) > 0 {
                        return self.choose_equiv(pointless_spade, &pointless_spades, trick);
                    }
                }
            }
            return self.choose_equiv(
                best_card,
                &valid_cards.of_suit(best_card.suit).unwrap(),
                trick,
            );
        } else {
            let suit_cards = Suit::ALL
                .iter()
                .filter_map(|suit| valid_cards.of_suit(*suit))
                .max_by_key(|cards| self.key_prob_give_pref(cards))
                .unwrap();

            return self.choose_equivalent_to_highest(&suit_cards, trick);
        }
    }

    fn avoid_trick(&self, valid_cards: &Cards, high_card: Card, trick: &Trick) -> Card {
        // If we can avoid taking the trick, do so with the highest card possible (up to equivalence)
        if let Some(below) = valid_cards.filter(|c| c.rank < high_card.rank) {
            return self.choose_equivalent_to_highest(&below, trick);
        }

        // we can't dodge, so take it with the fewest points.
        // If we're the last player, use a high card.
        // Otherwise use a low card and hope that someone else
        // gets it.

        // remove the queen of spades, since we don't want to
        // play her unless we have to.
        let cards = valid_cards.try_exclude_card(Card::QUEEN_OF_SPADES);

        if trick.num_played() == 3 {
            self.choose_equivalent_to_highest(&cards, trick)
        } else {
            self.choose_equivalent_to_lowest(&cards, trick)
        }
    }

    fn take_trick(&self, valid_cards: &Cards, high_card: Card, trick: &Trick) -> Card {
        // if someone else will take the Queen, play it!
        // Getting rid of the Queen is important.
        if valid_cards.contains(Card::QUEEN_OF_SPADES) && Rank::QUEEN < high_card.rank {
            return Card::QUEEN_OF_SPADES;
        }

        // Play the highest card that's not a point
        if let Some(pointless_cards) = valid_cards.filter(|c| !is_point_card(c)) {
            // Try not to trump the queen of spades
            if high_card.suit == Suit::SPADES && !self.is_played(Card::QUEEN_OF_SPADES) {
                if let Some(safe_cards) = pointless_cards.filter(|c| c.rank < Rank::QUEEN) {
                    return self.choose_equivalent_to_highest(&safe_cards, trick);
                }
            }

            // Can't avoid playing a dangerous spade.
            return self.choose_equivalent_to_highest(&pointless_cards, trick);
        }
        // Have to play a point. Go low
        self.choose_equivalent_to_lowest(valid_cards, trick)
    }

    fn dump_cards(&self, valid_cards: &Cards, _high_card: Card, trick: &Trick) -> Card {
        // We don't have the suit at all.  First, try to dump high spades.

        if valid_cards.contains(Card::QUEEN_OF_SPADES) {
            return Card::QUEEN_OF_SPADES;
        }
        if valid_cards.contains(Card::ACE_OF_SPADES) && self.is_scoring_suit(Suit::SPADES) {
            return Card::ACE_OF_SPADES;
        }
        if valid_cards.contains(Card::KING_OF_SPADES) && self.is_scoring_suit(Suit::SPADES) {
            return Card::KING_OF_SPADES;
        }

        // Dump any card which is the last card of the suit, has some unseen card below it and has no unseen cards above it.
        if let Some(lone_cards) = valid_cards.filter(|c| {
            count_suit(valid_cards, c.suit) == 1
                && self.unseen_below(c) > 0
                && self.unseen_above(c) == 0
        }) {
            return lone_cards.random();
        }

        // Dump any card which is the last card of the suit and has some unseen card below it.
        if let Some(lone_cards) =
            valid_cards.filter(|c| count_suit(valid_cards, c.suit) == 1 && self.unseen_below(c) > 0)
        {
            return lone_cards.random();
        }

        // Dump any card with no cards unseen above it.

        // If there are candidates from several suits, then
        // choose the suit with the fewest safety cards (cards with unseen_above > 0).
        if let Some(high_cards) =
            valid_cards.filter(|c| self.unseen_above(c) == 0 && self.unseen_below(c) > 0)
        {
            let c = high_cards.min_by_key(|c| self.dump_rank(c.suit, valid_cards));
            return self.choose_equiv(c, &high_cards, trick);
        }

        // Dump the highest point card we have.
        if let Some(point_cards) = valid_cards.filter(is_point_card) {
            return self.choose_equivalent_to_highest(&point_cards, trick);
        }

        // Try to dump cards that have some unseen cards below
        // them and few unseen cards above them first.
        if let Some(candidates) = valid_cards.filter(|c| self.unseen_below(c) > 0) {
            let c = candidates.min_by_key(|c| self.unseen_above(c));
            return self.choose_equiv(c, &candidates, trick);
        }

        // None of my cards have any unseen cards
        // below them.  I'm not sure, but it seems to
        // me that this is a safe situation.  Just pick one.
        valid_cards.random()
    }

    /// Called when we are the fourth player on the trick.
    ///
    /// Have the suit:
    /// - If there are no points in the trick, take it with the highest card we have.
    /// - If there are points in the trick, play the highest card that won't take it.  
    /// - If we are forced to take it, take it with the highest card we have.
    /// Not have the suit:
    /// - Play the Queen of Spades.
    /// - Play the King or Ace of Spades
    /// - Play the highest Queen or above card we have.
    /// - Play the highest heart.
    /// - Play the highest card we have.
    fn choose_final_card(&self, valid_cards: &Cards, trick: &Trick) -> Card {
        let high_card = *trick.get_winner_card().unwrap();
        if valid_cards.have_suit(high_card.suit) {
            if trick.num_point_cards() > 0 {
                self.avoid_trick(valid_cards, high_card, trick)
            } else {
                self.take_trick(valid_cards, high_card, trick)
            }
        } else {
            self.dump_cards(valid_cards, high_card, trick)
        }
    }

    /// return true if one of the remaining players doesn't
    /// have the suit of the trick and either (has hearts or
    /// queen of spades is not played)
    fn is_dangerous_trick(&self, trick: &Trick) -> bool {
        let high_card = *trick.get_winner_card().unwrap();

        // First trick: no points possible, so not dangerous!
        if self.hand.len() == 13 {
            return false;
        }

        if trick.num_point_cards() > 0 {
            return true;
        }

        if self.num_unseen(high_card.suit) + trick.num_played() < 6 {
            return true;
        }

        let remaining_players: Vec<&Side> = Side::ALL
            .iter()
            .filter(|side| !trick.iter().any(|(s, _)| s == *side))
            .collect();

        for side in remaining_players {
            if self.player_out_suits.contains(&(*side, high_card.suit))
                && (!self.player_out_suits.contains(&(*side, Suit::HEARTS))
                    || (!self.seen_cards.contains(&Card::QUEEN_OF_SPADES)
                        && !self.player_out_suits.contains(&(*side, Suit::SPADES))))
            {
                return true;
            }
        }

        false
    }

    /// Have the suit:
    /// - If the suit is not dangerous and has at least 7 cards in other folks hands, play the highest card we have.
    /// - Else, play the highest card that won't take the trick.
    /// Not have the suit:
    /// - Play the Queen of Spades.
    /// - Play the highest Queen or above card we have.
    /// - Play the highest heart.
    ///
    /// - If the suit is not dangerous and has at least 7 cards in other folks hands, play the highest card we have.
    fn choose_mid_card(&self, valid_cards: &Cards, trick: &Trick) -> Card {
        let high_card = *trick.get_winner_card().unwrap();
        if valid_cards.have_suit(high_card.suit) {
            if trick.num_point_cards() > 0 || self.is_dangerous_trick(trick) {
                self.avoid_trick(valid_cards, high_card, trick)
            } else {
                self.take_trick(valid_cards, high_card, trick)
            }
        } else {
            self.dump_cards(valid_cards, high_card, trick)
        }
    }
}

impl PlayerAI for Ling {
    fn get_name(&self) -> &str {
        "Ling" // Alternative names: "Mikhi" and "Bruno"
    }

    fn update(&mut self, cards: &[Card], score: &Score) {
        self.hand = cards.to_vec();
        self.score = score.clone();
    }

    /// This function is called when you have to select three
    /// cards to pass to the next player. Return a list of three cards.
    ///
    /// - If I have ace or king of spades, get rid of it.
    /// - If I have fewer than four spades, get rid of the queen.
    /// - If my lowest heart is greater than 6, get rid of every heart over 10.
    /// - If I have fewer than three diamonds, get rid of as many as possible.
    /// - If I have fewer than four clubs, get rid of as many as possible.
    /// - Get rid of the highest diamonds/clubs.
    fn select_cards(&mut self, _pass_direction: Side) -> Pass {
        let mut result = Pass::new();

        // first pass on the high spades
        if let Some(card) = select_card(&mut self.hand, &Card::ACE_OF_SPADES) {
            result.add(card);
        }
        if let Some(card) = select_card(&mut self.hand, &Card::KING_OF_SPADES) {
            result.add(card);
        }
        if self.hand.iter().filter(|c| c.suit == Suit::SPADES).count() < 4 {
            if let Some(card) = select_card(&mut self.hand, &Card::QUEEN_OF_SPADES) {
                result.add(card);
            }
        }

        // if our lowest heart is above six, get rid of all heart face cards.
        if let Some(lowest_heart) = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::HEARTS)
            .min_by_key(|c| c.rank)
        {
            if lowest_heart.rank > Rank::SIX {
                let faces = self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::HEARTS && c.rank >= Rank::JACK)
                    .sorted_by_key(|c| c.rank);
                result.add_all(faces.into_iter().rev());
            }
        }

        let clubs_list = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::CLUBS)
            .sorted_by_key(|c| c.rank);
        let diamonds_list = self
            .hand
            .iter()
            .filter(|c| c.suit == Suit::DIAMONDS)
            .sorted_by_key(|c| c.rank);

        // Try to clear diamonds and clubs
        if clubs_list.len() < diamonds_list.len() {
            if clubs_list.len() > 0 && clubs_list.len() < 4 {
                result.add_all(clubs_list.into_iter().rev());
            }
            if diamonds_list.len() > 0 && diamonds_list.len() < 4 {
                result.add_all(diamonds_list.into_iter().rev());
            }
        } else {
            if diamonds_list.len() > 0 && diamonds_list.len() < 4 {
                result.add_all(diamonds_list.into_iter().rev());
            }
            if clubs_list.len() > 0 && clubs_list.len() < 4 {
                result.add_all(clubs_list.into_iter().rev());
            }
        }

        // dump dull cards
        result.add_all(
            self.hand
                .iter()
                .filter(|c| c.suit == Suit::CLUBS || c.suit == Suit::DIAMONDS)
                .sorted_by_key(|c| c.rank)
                .into_iter()
                .rev(),
        );

        // I shouldn't need to dump any more hearts or spades, but
        // it's always possible that I was dealt only these two suits!
        result.add_all(
            self.hand
                .iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .sorted_by_key(|c| c.rank)
                .rev(),
        );
        result.add_all(
            self.hand
                .iter()
                .filter(|c| c.suit == Suit::SPADES)
                .sorted_by_key(|c| c.rank)
                .rev(),
        );

        result
    }

    /// LEADING:
    ///
    /// Call a suit bad if I know that one player is out of that suit.
    ///
    /// Call a suit dangerous if either it contains an unseen scoring
    /// card (i.e., spades before the queen has been seen or hearts anytime) or
    /// it is bad.
    ///
    /// If I have the ace or king of a non-dangerous suit, then play it.
    ///
    /// If I do not have anything queen or above of spades and if spades
    /// are not bad, then play the highest spade below the queen.
    ///
    /// If I have a heart that I know will not take the trick, play it
    /// (if possible).  (Play the highest heart that won't take the trick).
    ///
    /// Even if spades are bad, play a low spade provided I have nothing
    /// above Jack in my hand.
    ///
    /// Play the highest diamond/club that will not take the trick.
    ///
    /// FOLLOWING:
    ///
    /// Not fourth:
    ///     Have the suit:
    ///         If the suit is not dangerous and has at least 7 cards in other folks hands,
    ///         play the highest card we have.
    ///         Else, play the highest card that won't take the trick.
    ///     Not have the suit:
    ///         Play the Queen of Spades.
    ///         Play the highest Queen or above card we have.
    ///         Play the highest heart.
    /// Fourth:
    ///     Have the suit:
    ///         If there are no points in the trick, take it with the highest card we have.
    ///         If there are points in the trick, play the highest card that won't take it.  
    ///         If we are forced to take it, take it with the highest card we have.
    ///     Not have the suit:
    ///         As above.
    fn play_card(&mut self, valid_cards: &Cards, trick: &Trick) -> Card {
        self.record_played_cards(trick);

        if self.moon_shooter(valid_cards, trick) {
            if let Some(ret) = self.moon_defense(valid_cards, trick) {
                return ret;
            }
        }
        match trick.num_played() {
            0 => self.choose_lead_card(valid_cards, trick),
            3 => self.choose_final_card(valid_cards, trick),
            _ => self.choose_mid_card(valid_cards, trick),
        }
    }

    fn trick_end(&mut self, trick: &Trick) {
        let trump = trick.trump().unwrap();
        for (side, card) in trick.iter() {
            if card.suit != trump {
                self.player_out_suits.insert((*side, trump));
            }
        }
        self.record_played_cards(trick);
    }

    fn round_end(&mut self) {
        self.player_out_suits.clear();
        self.seen_cards.clear();
    }
}
