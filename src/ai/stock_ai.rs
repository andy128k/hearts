use super::player::PlayerAI;
use crate::card_list::*;
use crate::cards::*;
use crate::pass::*;
use crate::score::*;
use crate::side::*;
use crate::trick::*;
use std::cmp::Ord;

fn s_points_and_high_spades(card: Card) -> impl Ord {
    (
        card.suit == Suit::SPADES && card.rank == Rank::QUEEN,
        card.suit == Suit::SPADES && card.rank == Rank::ACE,
        card.suit == Suit::SPADES && card.rank == Rank::KING,
        card.suit == Suit::HEARTS,
        card.rank,
    )
}

// spade priority = J,10,9,8,7,6,5,4,3,2,A,K,Q followed by the other suits
fn s_spades_priority(card: Card) -> impl Ord {
    (
        card.suit == Suit::SPADES,
        card.rank >= Rank::QUEEN,
        -(card.rank as i32),
    )
}

pub struct StockAI {
    name: String,
    hand: Vec<Card>,
    ruleset: Ruleset,
}

impl StockAI {
    pub fn new(name: &str, ruleset: Ruleset) -> Self {
        Self {
            name: name.to_owned(),
            hand: Vec::new(),
            ruleset,
        }
    }
}

impl PlayerAI for StockAI {
    fn get_name(&self) -> &str {
        &self.name
    }

    fn update(&mut self, cards: &[Card], _score: &Score) {
        self.hand = cards.to_vec();
    }

    fn select_cards(&mut self, _pass_direction: Side) -> Pass {
        let mut pass = Pass::new();

        // First pass on the high spades
        if let Some(card) = select_card(&mut self.hand, &Card::QUEEN_OF_SPADES) {
            pass.add(card);
        }
        if let Some(card) = select_card(&mut self.hand, &Card::KING_OF_SPADES) {
            pass.add(card);
        }
        if let Some(card) = select_card(&mut self.hand, &Card::ACE_OF_SPADES) {
            pass.add(card);
        }

        // Pass off the other cards
        while !pass.is_full() {
            // If we can clear a suit with one card then do so, else pick a hearts or a high card
            if self.hand.iter().filter(|c| c.suit == Suit::CLUBS).count() == 1 {
                pass.add(select_card_best_by_key(&mut self.hand, |card| {
                    card.suit == Suit::CLUBS
                }));
            } else if self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::DIAMONDS)
                .count()
                == 1
            {
                pass.add(select_card_best_by_key(&mut self.hand, |card| {
                    card.suit == Suit::DIAMONDS
                }));
            } else if self.hand.iter().any(|c| c.suit == Suit::HEARTS) {
                pass.add(select_card_best_by_key(&mut self.hand, |card| {
                    (card.suit == Suit::HEARTS, card.rank)
                }));
            } else {
                pass.add(select_card_best_by_key(&mut self.hand, |card| card.rank));
            }
        }

        pass
    }

    fn play_card(&mut self, cards: &Cards, trick: &Trick) -> Card {
        if trick.num_played() == 0 {
            // Open with the lowest valid card
            return cards.try_exclude_card(Card::QUEEN_OF_SPADES).lowest();
        }

        // Someone has already played. If the score is zero, it's not a point card
        if trick.get_score(self.ruleset) == 0 {
            // If we have a trump suit, take it with the highest card
            if cards.have_suit(trick.trump().unwrap()) {
                // Spades are special because of the Queen
                if trick.trump() == Some(Suit::SPADES) {
                    // Try dumping the Queen on someone else
                    if trick.iter_cards().any(|c| {
                        c.suit == Suit::SPADES && (c.rank == Rank::KING || c.rank == Rank::ACE)
                    }) && cards.contains(Card::QUEEN_OF_SPADES)
                    {
                        return Card::QUEEN_OF_SPADES;
                    }

                    if trick.num_played() == 3 {
                        // We're the last one playing and the queen isn't on the trick.
                        // Take it with the Ace, King or anything below the Queen
                        return cards.try_exclude_card(Card::QUEEN_OF_SPADES).highest();
                    }

                    // Other people still have to play. Try not to play the Queen or anything above because we may end up taking the points
                    return cards.min_by_key(s_spades_priority);
                } else {
                    // Trump is diamonds or clubs. Play highest cards
                    return cards.highest();
                }
            }

            // We don't have a trump. Dump a point card
            return cards.max_by_key(s_points_and_high_spades);
        }

        // There are point cards on the trick
        if cards.have_suit(trick.trump().unwrap()) {
            // Play the highest card that doesn't take the trick
            let high_card = trick.get_winner_card().unwrap();
            if let Some(highest) = cards
                .filter(|card| card.suit != high_card.suit || card.rank < high_card.rank)
                .map(|cards| cards.highest())
            {
                return highest;
            }

            // We are forced to take the trick. Play highest card if we're the last player
            if trick.num_played() == 3 {
                return cards.try_exclude_card(Card::QUEEN_OF_SPADES).highest();
            }

            // Play low. Someone may beat us yet
            return cards.lowest();
        } else {
            // We don't have a trump. Dump a point card
            return cards.max_by_key(s_points_and_high_spades);
        }
    }
}
