use super::player::PlayerAI;
use crate::card_list::*;
use crate::cards::*;
use crate::pass::*;
use crate::score::*;
use crate::side::*;
use crate::trick::*;
use itertools::Itertools;
use std::collections::HashMap;

pub struct Luis {
    hand: Vec<Card>,

    hearts_played: bool,
    queen_played: bool,
    trick_starts: HashMap<Suit, u32>,
}

impl Luis {
    pub fn new() -> Self {
        Self {
            hand: Vec::new(),
            hearts_played: false,
            queen_played: false,
            trick_starts: HashMap::new(),
        }
    }

    fn open_trick(&self, valid_cards: &Cards) -> Card {
        // If I have only hearts, play the lowest
        if self.hand.iter().all(|c| c.suit == Suit::HEARTS) {
            return *self.hand.iter().min_by_key(|c| c.rank).unwrap();
        }

        // If I have the two of clubs, play it
        // FIXME: This should honor the clubs_lead = false rule
        if self.hand.contains(&Card::TWO_OF_CLUBS) {
            return Card::TWO_OF_CLUBS;
        }

        // If I don't have the queen of spades or higher and the queen hasn't been played, open with high spades
        if !self.queen_played
            && !self.hand.contains(&Card::QUEEN_OF_SPADES)
            && !self.hand.contains(&Card::KING_OF_SPADES)
            && !self.hand.contains(&Card::ACE_OF_SPADES)
        {
            if let Some(high_spade) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::SPADES)
                .max_by_key(|c| c.rank)
            {
                return *high_spade;
            }
        }

        // Try playing a suit that hasn't been played much
        if self.trick_starts[&Suit::DIAMONDS] < 2 {
            if let Some(card) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::DIAMONDS)
                .max_by_key(|c| c.rank)
            {
                return *card;
            }
        }

        if self.trick_starts[&Suit::CLUBS] < 2 {
            if let Some(card) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::CLUBS)
                .max_by_key(|c| c.rank)
            {
                return *card;
            }
        }

        // A low hearts to give them trouble
        // FIXME: Should honor hearts_broken = false
        if have_suit(&self.hand, Suit::HEARTS) && self.hearts_played {
            let max_hearts = 2 + ((self.trick_starts[&Suit::HEARTS] + 1) * 3);
            if let Some(low_heart) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::HEARTS && (c.rank as u32) < max_hearts)
                .min_by_key(|c| c.rank)
            {
                return *low_heart;
            }
        }

        // Find a good card based on scores
        let best_card = Suit::ALL
            .iter()
            .filter(|suit| self.trick_starts[suit] < 4)
            .filter_map(|suit| valid_cards.of_suit(*suit).map(|cards| cards.lowest()))
            .max_by_key(|card| 2 + ((self.trick_starts[&card.suit] + 1) * 3) - (card.rank as u32));

        if let Some(best_card) = best_card {
            return best_card;
        }

        // Give up. Play random
        valid_cards.random()
    }

    /// Play a card following the trick's trump
    fn follow_suit(&self, trick: &Trick) -> Card {
        // Play spades
        if trick.trump() == Some(Suit::SPADES) {
            // Uh oh, I have the queen
            if self.hand.contains(&Card::QUEEN_OF_SPADES) {
                // See if I can dump the queen
                if trick
                    .iter_cards()
                    .any(|c| *c == Card::KING_OF_SPADES || *c == Card::ACE_OF_SPADES)
                {
                    return Card::QUEEN_OF_SPADES;
                }
                // Try playing something else but the queen
                if self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES)
                    .min_by_key(|c| c.rank)
                    == Some(&Card::QUEEN_OF_SPADES)
                {
                    // return the highest card. Could be the queen if that's all we have.
                    return *self
                        .hand
                        .iter()
                        .filter(|c| c.suit == Suit::SPADES)
                        .max_by_key(|c| c.rank)
                        .unwrap();
                }
                return *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES && c.rank < Rank::QUEEN)
                    .max_by_key(|c| c.rank)
                    .unwrap();
            }
            // I don't have the queen
            if self
                .hand
                .iter()
                .any(|c| *c == Card::KING_OF_SPADES || *c == Card::ACE_OF_SPADES)
            {
                if trick.num_played() == 3
                    && !trick.iter_cards().any(|c| *c == Card::QUEEN_OF_SPADES)
                {
                    return *self
                        .hand
                        .iter()
                        .filter(|c| c.suit == Suit::SPADES)
                        .max_by_key(|c| c.rank)
                        .unwrap();
                }

                if self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES)
                    .min_by_key(|c| c.rank)
                    .unwrap()
                    .rank
                    >= Rank::KING
                {
                    // Forced to play high spades
                    return *self
                        .hand
                        .iter()
                        .filter(|c| c.suit == Suit::SPADES)
                        .max_by_key(|c| c.rank)
                        .unwrap();
                }
                return *self
                    .hand
                    .iter()
                    .filter(|c| c.suit == Suit::SPADES && c.rank < Rank::KING)
                    .max_by_key(|c| c.rank)
                    .unwrap();
            }

            return *self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::SPADES)
                .max_by_key(|c| c.rank)
                .unwrap();
        }

        // Play hearts
        if trick.trump() == Some(Suit::HEARTS) {
            let highest_card = trick.get_winner_card().unwrap();

            let hearts_list = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .sorted_by_key(|c| c.rank)
                .collect::<Vec<_>>();
            if hearts_list[0].rank > highest_card.rank {
                if trick.num_played() == 3
                    || (trick.num_played() == 2 && hearts_list[0].rank - highest_card.rank > 3)
                {
                    // I'm going to get it anyway, so play the highest
                    return **hearts_list.last().unwrap();
                }
                return *hearts_list[0];
            }
            // Play highest hearts that doesn't win
            if let Some(low) = self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::HEARTS && c.rank < highest_card.rank)
                .max_by_key(|c| c.rank)
            {
                return *low;
            }
            // Play lowest card
            return *self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::HEARTS)
                .min_by_key(|c| c.rank)
                .unwrap();
        }

        // Play clubs or diamonds
        let suit = if trick.trump() == Some(Suit::CLUBS) {
            Suit::CLUBS
        } else {
            Suit::DIAMONDS
        };
        // Play high card if I don't take the queen of hearts
        if self.trick_starts[&trick.trump().unwrap()] < 2
            && !trick.iter_cards().any(|c| *c == Card::QUEEN_OF_SPADES)
        {
            return *self
                .hand
                .iter()
                .filter(|c| c.suit == suit)
                .max_by_key(|c| c.rank)
                .unwrap();
        }
        // If I'm the last in the trick, take with high card
        if trick.num_played() == 3 && !trick.iter_cards().any(|c| *c == Card::QUEEN_OF_SPADES) {
            return *self
                .hand
                .iter()
                .filter(|c| c.suit == suit)
                .max_by_key(|c| c.rank)
                .unwrap();
        }
        // Play the highest card that doesn't win the trick
        let highest_card = trick.get_winner_card().unwrap();
        if let Some(low) = self
            .hand
            .iter()
            .filter(|c| c.suit == suit && c.rank < highest_card.rank)
            .max_by_key(|c| c.rank)
        {
            return *low;
        }
        // Play lowest card
        *self
            .hand
            .iter()
            .filter(|c| c.suit == suit)
            .min_by_key(|c| c.rank)
            .unwrap()
    }

    /// Play a card when I have no trump cards
    fn dont_follow_suit(&self, valid_cards: &Cards) -> Card {
        if valid_cards.contains(Card::QUEEN_OF_SPADES) {
            return Card::QUEEN_OF_SPADES;
        }
        if valid_cards.contains(Card::KING_OF_SPADES) {
            return Card::KING_OF_SPADES;
        }
        if valid_cards.contains(Card::ACE_OF_SPADES) {
            return Card::ACE_OF_SPADES;
        }

        if let Some(highest_cards) = valid_cards.filter(|c| c.rank > Rank::EIGHT) {
            return highest_cards.highest();
        }

        if let Some(hearts) = valid_cards.of_suit(Suit::HEARTS) {
            return hearts.highest();
        }

        valid_cards.highest()
    }
}

impl PlayerAI for Luis {
    fn get_name(&self) -> &str {
        "Luis"
    }

    fn update(&mut self, cards: &[Card], _score: &Score) {
        self.hand = cards.to_vec();
    }

    fn select_cards(&mut self, _direction: Side) -> Pass {
        let mut result = Pass::new();

        // First pass on the high spades
        if let Some(card) = select_card(&mut self.hand, &Card::QUEEN_OF_SPADES) {
            result.add(card);
        }
        if let Some(card) = select_card(&mut self.hand, &Card::KING_OF_SPADES) {
            result.add(card);
        }
        if let Some(card) = select_card(&mut self.hand, &Card::ACE_OF_SPADES) {
            result.add(card);
        }

        // Pass off the other cards
        while !result.is_full() {
            // If we can clear a suit with one card then do so, else pick a hearts or a high card
            if self.hand.iter().filter(|c| c.suit == Suit::CLUBS).count() == 1 {
                result.add(select_card_best_by_key(&mut self.hand, |card| {
                    card.suit == Suit::CLUBS
                }));
            } else if self
                .hand
                .iter()
                .filter(|c| c.suit == Suit::DIAMONDS)
                .count()
                == 1
            {
                result.add(select_card_best_by_key(&mut self.hand, |card| {
                    card.suit == Suit::DIAMONDS
                }));
            } else if self.hand.iter().any(|c| c.suit == Suit::HEARTS) {
                result.add(select_card_best_by_key(&mut self.hand, |card| {
                    (card.suit == Suit::HEARTS, card.rank)
                }));
            } else {
                result.add(select_card_best_by_key(&mut self.hand, |card| card.rank));
            }
        }

        result
    }

    fn play_card(&mut self, valid_cards: &Cards, trick: &Trick) -> Card {
        if let Some(trump) = trick.trump() {
            if have_suit(&self.hand, trump) {
                self.follow_suit(trick)
            } else {
                self.dont_follow_suit(valid_cards)
            }
        } else {
            self.open_trick(valid_cards)
        }
    }

    fn trick_end(&mut self, trick: &Trick) {
        *self.trick_starts.entry(trick.trump().unwrap()).or_insert(0) += 1;
        if trick.iter_cards().any(|c| c.suit == Suit::HEARTS) {
            self.hearts_played = true;
        }
        if trick.iter_cards().any(|c| *c == Card::QUEEN_OF_SPADES) {
            self.queen_played = true;
        }
    }

    fn round_end(&mut self) {
        self.hearts_played = false;
        self.trick_starts.clear();
    }
}
