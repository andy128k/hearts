pub mod jake;
pub mod ling;
pub mod luis;
pub mod peter;
pub mod player;
pub mod stock_ai;

use crate::config::GameOptions;
use crate::side::Side;
use serde::{Deserialize, Serialize};

#[derive(Clone, Copy, Debug, Serialize, Deserialize)]
pub enum Opponent {
    Jake,
    John,
    Pauline,
    Mike,
    Luis,
    Ling,
    Peter,
}

impl Opponent {
    pub const ALL: [Opponent; 7] = [
        Opponent::Jake,
        Opponent::John,
        Opponent::Pauline,
        Opponent::Mike,
        Opponent::Luis,
        Opponent::Ling,
        Opponent::Peter,
    ];

    pub fn create_ai(&self, side: Side, options: &GameOptions) -> Box<dyn player::PlayerAI> {
        match self {
            Opponent::Jake => Box::new(jake::Jake::new()),
            Opponent::John => Box::new(stock_ai::StockAI::new("John", options.ruleset)),
            Opponent::Pauline => Box::new(stock_ai::StockAI::new("Pauline", options.ruleset)),
            Opponent::Mike => Box::new(stock_ai::StockAI::new("Mike", options.ruleset)),
            Opponent::Luis => Box::new(luis::Luis::new()),
            Opponent::Ling => Box::new(ling::Ling::new(side)),
            Opponent::Peter => Box::new(peter::Peter::new(side, options)),
        }
    }

    pub fn to_str(&self) -> &str {
        match self {
            Opponent::Jake => "Jake",
            Opponent::John => "John",
            Opponent::Pauline => "Pauline",
            Opponent::Mike => "Mike",
            Opponent::Luis => "Luis",
            Opponent::Ling => "Ling",
            Opponent::Peter => "Peter",
        }
    }

    pub fn from_str(s: &str) -> Option<Self> {
        match s {
            "Jake" => Some(Opponent::Jake),
            "John" => Some(Opponent::John),
            "Pauline" => Some(Opponent::Pauline),
            "Mike" => Some(Opponent::Mike),
            "Luis" => Some(Opponent::Luis),
            "Ling" => Some(Opponent::Ling),
            "Peter" => Some(Opponent::Peter),
            _ => None,
        }
    }
}
