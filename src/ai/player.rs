use crate::card_list::*;
use crate::cards::*;
use crate::pass::*;
use crate::score::*;
use crate::side::*;
use crate::trick::*;

pub trait PlayerAI {
    fn get_name(&self) -> &str;

    fn update(&mut self, cards: &[Card], score: &Score);

    fn select_cards(&mut self, direction: Side) -> Pass;

    fn receive_cards(&mut self, _cards: &Pass) {}

    fn play_card(&mut self, valid_cards: &Cards, trick: &Trick) -> Card;

    fn trick_end(&mut self, _trick: &Trick) {}

    fn round_end(&mut self) {}
}
