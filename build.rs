use std::env;
use std::process::Command;

fn main() {
    let out_dir = env::var("OUT_DIR").unwrap();

    Command::new("glib-compile-resources")
        .arg(&format!("--target={}/pixmaps.gresource", out_dir))
        .arg("--sourcedir=pixmaps")
        .arg("pixmaps/pixmaps.gresource.xml")
        .status()
        .unwrap();
}
